function [M, tau] = pdpSamples(Nc, T_Q, spread, profile)
%%PDPSAMPLES
% Return equally spaced samples of requested PDP
%
% INPUTS:
% * Nc: number of samples starting from 0
% * T_Q: sampling period
% * spread: root mean square delay spread
% * ( profile: either 'uniGauss','uniExp' or '2ray'. Default: 'uniExp' )
%
% OUTPUTS:
% * M: samples of the requested PDP as column vector normalized so that
% sum(M)=1
% * tau: time vector corresponding to M

% arg check
narginchk(3, 4);
if ( nargin<4 )
    profile = 'uniExp';
end

if ( ~isscalar(Nc) || ~isscalar(T_Q) || ~isscalar(spread) )
    error('Nc, T_Q, spread must be scalars')
end
if ( Nc<1 )
    error('Nc<1')
end
if ( T_Q<0 )
    error('T_Q<0')
end
if ( spread<0 )
    error('spread<0')
end
if ( ~any( strcmp(profile, {'uniExp', 'uniGauss', '2ray'}) ))
    error('profile must be either uniExp or uniGauss')
end

% init
tau = (0:Nc-1)'*T_Q;
% compute
switch profile
    case 'uniGauss'
        M = sqrt(2/pi)/spread*exp( -tau.^2/(2*spread.^2) );
    case 'uniExp'
        M = 1/spread*exp( -tau/spread );
    case '2ray'
        M = zeros( size(tau) );
        M(1) = 0.5; % first ray
        n = ( tau>(2*spread-T_Q/2) & tau<=(2*spread+T_Q/2) ); % if in the middle -> exceeding one
        M(n) = 0.5; % second ray
end

% normalization
M = M/sum(M);
end

