clear variables
close all
clc

% parameters
Tc = 1; % unit sampling
fd = 2.5e-4/Tc;
relDelay = 3; % relative delay of second component as multiple of Tc
Kdb = 2; % rice factor
N = 12e3 + 1; % #samples (from 0 to 12k => 12k+1 samples)
fs = 0; % doppler shift
pdpProfile = '2ray';
chMethod = 'filter';
rays = 50; % only important if chMethod=='exp'

exportPlots = false; % export plots

% pdp creation
M = pdpSamples(relDelay+1, Tc, relDelay*Tc/2, pdpProfile);
K = 10^(Kdb/10);
C = sqrt( K/(K+1) );

% channel creation
fprintf('Creating channel (N=%d, chMethod=%s)...\n',...
    N, chMethod);
[h, PDPtot] = channelSimulation(Tc, fd, N, M, C, fs, chMethod, rays);

%% PDP+C plot
disp 'Plotting PDP...'
n = (0:length(PDPtot)-1)'*Tc;

figure
stem(n, 10*log10(PDPtot),'k'); grid on
xlabel('nT_c')
ylabel('PDP_{dB}')
title('Power Delay Profile')
xlim([-0.2 3.2]*Tc)
ax = gca;
ax.XTick = 0:3;
if (exportPlots)
    matlab2tikz('../Latex/img/PDP.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%% plot |h0|
disp 'Plotting |h0|...'
nTc = (0:N-1)'*Tc;

figure
plot(nTc, 10*log10(abs( h(:,1) ) ), 'k'); grid on
xlabel('nT_c')
ylabel('|h_0(nT_c)|_{dB}')
title('|h_0(nT_c)|')
if (exportPlots)
    cleanfigure();
    matlab2tikz('../Latex/img/channel_plot.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

disp 'Plotting trace...'
figure % just because it is cool
noise = h(:,1);
hist3([real(noise), imag(noise)], [1,1]*50)
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
xlabel('Re', 'interpreter', 'latex', 'FontSize', 12)
ylabel('Im', 'interpreter', 'latex', 'FontSize', 12)
zlabel('Count', 'interpreter', 'latex', 'FontSize', 12)
title('Channel complex histogram', 'interpreter', 'latex', 'FontSize', 16)
if (exportPlots)
    c = colormap(hot);
    colormap( flipud(c) ); % nice colormap for b/w
    print(['../Latex/img/trace_', chMethod], '-deps');
end

%% pdf of |h0|
disp 'Plotting pdfs...'
n = 1:1e3; % first 1000 samples
h0_trunc = h(n,1);
h0_trunc = abs(h0_trunc)/sqrt( PDPtot(1) );

figure
histogram(h0_trunc, 50, 'normalization', 'pdf',...
    'FaceAlpha', 0.1, 'FaceColor', 'black',...
    'EdgeAlpha', 0.2); hold on; grid on
xlabel('a')
ylabel('p(a)')
title('PDF of  |h_0|/\surdE[|h_0|^2]')

% overlapping with rician
K0 = C^2/PDPtot(4); % K factor of ray 0 only
[ric_pdf, a] = getRicianPdf(K0, 0, 3, 1000);

plot(a, ric_pdf, 'k', 'Linewidth', 2)
xlabel('a')
ylabel('p(a)')
title('|h_0|/\surdE[|h_0|^2] vs. Rician')
legend('Empirical', sprintf('Rician (K_{0,dB}=%.1fdB)', 10*log10(K0)))
if (exportPlots)
    matlab2tikz('../Latex/img/pdf_h.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

% pdf of the whole simulation
figure
histogram(abs(h(:,1))./sqrt( mean(abs(h(:,1)).^2) ), 50,...
    'normalization', 'pdf','FaceAlpha', 0.1, 'FaceColor', 'black'); hold on; grid on

plot(a, ric_pdf, 'k', 'Linewidth', 2)
xlabel('a')
ylabel('p(a)')
title('|h_0|/\surdE[|h_0|^2] vs. Rician')
legend('Empirical', sprintf('Rician (K_{0,dB}=%.1fdB)', 10*log10(K0)))

%% 1000 realizations
n = 1e3; % #realizations
Ntrans = 10e3; % to avoid transient
N = 500; % we are interested in h0(500), it is useless to create more samples
h0_500 = zeros(n,1); % to store values

% compouting samples needed
fprintf('Creating %d channels  (N=%d, chMethod=%s)...\n',...
    n, N, chMethod);
for i = 1:n
    % log
    if ( mod(i,100)==0 )
        fprintf('Channels: %4d/%4d\n', i, n);
    end
    
    h = channelSimulation(Tc, fd, N, M, C, fs, chMethod, rays);
    
    h0_500(i) = h(N,1); % store N-th sample of h0 (index starts from 1)
end

% normalize
h0_500 = abs(h0_500)/sqrt( PDPtot(1) );

figure
histogram(h0_500, 'normalization', 'pdf',...
    'FaceAlpha', 0.1, 'FaceColor', 'black'); hold on; grid on
plot(a, ric_pdf, 'k', 'Linewidth', 2)
xlabel('a')
ylabel('p(a)')
title('|h_0(500)|/\surdE[|h_0|^2] vs. Rician')
legend('Empirical', sprintf('Rician (K_{0,dB}=%.1fdB)', 10*log10(K0)))
if (exportPlots)
    matlab2tikz('../Latex/img/pdf_h500.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end