clear variables
close all
clc

N = 1e5;
C = 0.9; pow = 1-C^2;
K = C^2/pow;
noise = crandn(N,1,C,pow);


figure
histogram(abs(noise), 'normalization', 'pdf',...
    'FaceAlpha', 0.1, 'FaceColor', 'black'); hold on

% rician distrib (converting to matlab's parameters)
s = sqrt( K/(1+K) );
sigma = sqrt( 1/( 2*(1+K) ));
ric = makedist('Rician', s, sigma); % creating rician ditribution object
a = (0:0.01:3)'; % sampling
ric_pdf = pdf(ric,a); % creating pdf

plot(a, ric_pdf, 'k', 'Linewidth', 2)

figure
hist3([real(noise), imag(noise)], [1,1]*50)
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');