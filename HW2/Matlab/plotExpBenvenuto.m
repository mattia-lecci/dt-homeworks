clear variables
close all
clc

% parameters
Tc = 1; % unit sampling
fd = 2.5e-4/Tc;
relDelay = 3; % relative delay of second component as multiple of Tc
Kdb = 2; % rice factor
N = 1e7; % #samples (from 0 to 12k => 12k+1 samples)
fs = 0; % doppler shift
pdpProfile = '2ray';
rays = 100; % only important if chMethod=='exp'

exportPlots = false; % export plots

% pdp creation
M = pdpSamples(relDelay+1, Tc, relDelay*Tc/2, pdpProfile);
K = 10^(Kdb/10);
C = sqrt( K/(K+1) );

nTc = (0:N-1)'*Tc;

%% oldExp
chMethod = 'oldExp';
% channel creation
fprintf('Creating channel (N=%d, chMethod=%s)...\n',...
    N, chMethod);
[h, ~] = channelSimulation(Tc, fd, N, M, C, fs, chMethod, rays);

oldAbs = abs( h(:,1) );
oldPhase = angle( h(:,1) );

figure
subplot(2,1,1)
plot(nTc, 10*log10(oldAbs), 'k'); grid on
title('|h_0(nT_c)| - OldExp')
ylabel('|h_0|_{dB}')

subplot(2,1,2)
plot(nTc, oldPhase, 'k'); grid on
ylabel('phase(h_0)')

xlabel('nT_c')

figure % just because it is cool
noise = h(:,1);
hist3([real(noise), imag(noise)], [1,1]*50)
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
xlabel('Re', 'interpreter', 'latex', 'FontSize', 12)
ylabel('Im', 'interpreter', 'latex', 'FontSize', 12)
zlabel('Count', 'interpreter', 'latex', 'FontSize', 12)
title('Channel complex histogram', 'interpreter', 'latex', 'FontSize', 16)

%% newExp
chMethod = 'exp';
% channel creation
fprintf('Creating channel (N=%d, chMethod=%s)...\n',...
    N, chMethod);
[h, ~] = channelSimulation(Tc, fd, N, M, C, fs, chMethod, rays);

newAbs = abs(h(:,1));
newPhase = angle( h(:,1) );

figure
subplot(2,1,1)
plot(nTc, 10*log10(newAbs), 'k'); grid on
title('|h_0(nT_c)| - NewExp')
ylabel('|h_0|_{dB}')

subplot(2,1,2)
plot(nTc, newPhase, 'k'); grid on
ylabel('phase(h_0)')

xlabel('nT_c')

figure % just because it is cool
noise = h(:,1);
hist3([real(noise), imag(noise)], [1,1]*50)
set(get(gca,'child'),'FaceColor','interp','CDataMode','auto');
xlabel('Re', 'interpreter', 'latex', 'FontSize', 12)
ylabel('Im', 'interpreter', 'latex', 'FontSize', 12)
zlabel('Count', 'interpreter', 'latex', 'FontSize', 12)
title('Channel complex histogram', 'interpreter', 'latex', 'FontSize', 16)

%% Correlation
maxLag = floor( length(nTc)/5 );

[oldCorr,oldLag] = xcorr(oldAbs-mean(oldAbs),oldPhase,'unbiased',maxLag);
[newCorr,newLag] = xcorr(newAbs-mean(newAbs),newPhase,'unbiased',maxLag);

figure
subplot(2,1,1)
plot(oldLag,oldCorr,'k');grid on
subplot(2,1,2)
plot(newLag,newCorr,'k');grid on