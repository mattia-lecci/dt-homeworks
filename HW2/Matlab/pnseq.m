function seq = pnseq(init, outType)
%%PNSEQ
% Generates PN Maximum-Length sequence. The length of the init vector is
% intended to be r and seq(end)=init(end).
%
% INPUTS:
% * init: init 01 vector. r=length(init) in [1,30]
% * ( outType: either '01' or '+-1'. Default: '01' )
%
% OUTPUTS:
% * seq: the sequence as column vector. length(seq)=L=2^r-1. Initial
%       conditions are actually at the end of the vector (considered in
%       -1,-2,...)


% arg check
narginchk(1,2);
if ( nargin<2 )
    outType='01';
end

if ( ~isvector(init))
    error('Input init must be a vector')
end
if ( ~all( init==0 | init==1 ) )
    error('init must be a 0-1 vector')
end

r = length(init);

if ( r<1 || r>30 )
    error('r=length(init) must be in [1,30]')
end
if ( ~any( strcmp(outType, {'01','+-1'}) ))
    error('outType not recognized. Must be either 01 or +-1');
end

% init vars
% Consider initial conditions in p(0),p(1),... for easier computation, then
% move them to the end of the vector
L = 2^r-1;
seq = [init(:);...
    zeros(L-r, 1)];

switch r % choose based on length
    case 1
        % do nothing
    case 2
        for l = r+1:L
            tmp = seq(l-1)+seq(l-2);
            seq(l) = mod(tmp,2);
        end
    case 3
        for l = r+1:L
            tmp = seq(l-2)+seq(l-3);
            seq(l) = mod(tmp,2);
        end
    case 4
        for l = r+1:L
            tmp = seq(l-3)+seq(l-4);
            seq(l) = mod(tmp,2);
        end
    case 5
        for l = r+1:L
            tmp = seq(l-3)+seq(l-5);
            seq(l) = mod(tmp,2);
        end
    case 6
        for l = r+1:L
            tmp = seq(l-5)+seq(l-6);
            seq(l) = mod(tmp,2);
        end
    case 7
        for l = r+1:L
            tmp = seq(l-6)+seq(l-7);
            seq(l) = mod(tmp,2);
        end
    case 8
        for l = r+1:L
            tmp = seq(l-2)+seq(l-3)+seq(l-4)+seq(l-8);
            seq(l) = mod(tmp,2);
        end
    case 9
        for l = r+1:L
            tmp = seq(l-5)+seq(l-9);
            seq(l) = mod(tmp,2);
        end
    case 10
        for l = r+1:L
            tmp = seq(l-7)+seq(l-10);
            seq(l) = mod(tmp,2);
        end
    case 11
        for l = r+1:L
            tmp = seq(l-9)+seq(l-11);
            seq(l) = mod(tmp,2);
        end
    case 12
        for l = r+1:L
            tmp = seq(l-2)+seq(l-10)+seq(l-11)+seq(l-12);
            seq(l) = mod(tmp,2);
        end
    case 13
        for l = r+1:L
            tmp = seq(l-1)+seq(l-11)+seq(l-12)+seq(l-13);
            seq(l) = mod(tmp,2);
        end
    case 14
        for l = r+1:L
            tmp = seq(l-2)+seq(l-12)+seq(l-13)+seq(l-14);
            seq(l) = mod(tmp,2);
        end
    case 15
        for l = r+1:L
            tmp = seq(l-14)+seq(l-15);
            seq(l) = mod(tmp,2);
        end
    case 16
        for l = r+1:L
            tmp = seq(l-11)+seq(l-13)+seq(l-14)+seq(l-16);
            seq(l) = mod(tmp,2);
        end
    case 17
        for l = r+1:L
            tmp = seq(l-14)+seq(l-17);
            seq(l) = mod(tmp,2);
        end
    case 18
        for l = r+1:L
            tmp = seq(l-11)+seq(l-18);
            seq(l) = mod(tmp,2);
        end
    case 19
        for l = r+1:L
            tmp = seq(l-14)+seq(l-17)+seq(l-18)+seq(l-19);
            seq(l) = mod(tmp,2);
        end
    case 20
        for l = r+1:L
            tmp = seq(l-17)+seq(l-20);
            seq(l) = mod(tmp,2);
        end
    case 21
        for l = r+1:L
            tmp = seq(l-19)+seq(l-21);
            seq(l) = mod(tmp,2);
        end
    case 22
        for l = r+1:L
            tmp = seq(l-21)+seq(l-22);
            seq(l) = mod(tmp,2);
        end
    case 23
        for l = r+1:L
            tmp = seq(l-18)+seq(l-23);
            seq(l) = mod(tmp,2);
        end
    case 24
        for l = r+1:L
            tmp = seq(l-17)+seq(l-22)+seq(l-23)+seq(l-24);
            seq(l) = mod(tmp,2);
        end
    case 25
        for l = r+1:L
            tmp = seq(l-22)+seq(l-25);
            seq(l) = mod(tmp,2);
        end
    case 26
        for l = r+1:L
            tmp = seq(l-20)+seq(l-24)+seq(l-25)+seq(l-26);
            seq(l) = mod(tmp,2);
        end
    case 27
        for l = r+1:L
            tmp = seq(l-22)+seq(l-25)+seq(l-26)+seq(l-27);
            seq(l) = mod(tmp,2);
        end
    case 28
        for l = r+1:L
            tmp = seq(l-25)+seq(l-28);
            seq(l) = mod(tmp,2);
        end
    case 29
        for l = r+1:L
            tmp = seq(l-27)+seq(l-29);
            seq(l) = mod(tmp,2);
        end
    case 30
        for l = r+1:L
            tmp = seq(l-7)+seq(l-28)+seq(l-29)+seq(l-30);
            seq(l) = mod(tmp,2);
        end
end

% initial conditions are considered p(-1),p(-2),...
% => rotate the vector to put them at the end
seq = circshift(seq, -r);

% check if output need as 01 or +1-1 vector
if ( strcmp('+-1', outType) )
    seq( seq==0 ) = -1;
end

end