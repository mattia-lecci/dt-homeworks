function [p_a, a] = getRicianPdf(K, xmin, xmax, npoints)

% calculating equivalent variables
s = sqrt( K/(1+K) );
sigma = sqrt( 1/( 2*(1+K) ));

% creating rician ditribution object
ric = makedist('Rician', s, sigma);
% sampling
a = linspace(xmin, xmax, npoints)';
% creating pdf
p_a = pdf(ric,a);

end