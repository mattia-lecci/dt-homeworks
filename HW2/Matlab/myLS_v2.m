function [c, epsilon] = myLS_v2(x, N, d)

% build necessary tools
L = length(d);
I = buildI(x,N,L);
pinvI = pinv(I);

c = pinvI*d;
epsilon = d'*d - d'*I*pinvI*d;
epsilon = abs(epsilon); % to avoid numerical problems

end

function [I] = buildI(x, N, L)

I = zeros(L,N);

index = N:-1:1;
for i = 1:L
    I(i,:) = x(index);
    index = index+1;
end

end