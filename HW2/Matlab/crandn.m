function a = crandn(n, m, meanval, variance)
%%CRAND
% Generates nxm complex gaussia r.v. with given mean and variance (power)
%
% INPUTS:
% * n: number of columns
% * ( m: number of rows. Defult: m=n )
% * ( meanval: complex. Default: 0 )
% * ( variance: variance. Default: 1 )
%
% OUTPUTS:
% * a: nxm matrix

% arg check
narginchk(1,4);
if ( nargin<2 )
    m = n;
end
if ( nargin<3 )
    meanval = 0;
end
if ( nargin<4 )
    variance = 1;
end

if ( ~isscalar(n) || ~isscalar(m) || ~isscalar(meanval) || ~isscalar(variance))
    error('Inputs must be scalars')
end
if ( n<0 || m<0 )
    error('m,n must be >=0')
end
if ( variance<0 )
    error('var<0')
end

% computation
if ( variance==0 ) % avoid computation
    a = meanval*ones(n,m);
else
    std = sqrt(variance/2);
    a = meanval + std*randn(n,m) + 1j*std*randn(n,m);
end

end
