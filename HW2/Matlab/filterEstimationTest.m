clear variables
close all
clc

% use functions from HW1!
addpath('../../HW1/Matlab')

% create reference x
x = zeros(86,1);
pn = pnseq( ones(5,1), '+-1');
L = length(pn);   % length pn seq
x( 1:(2*L) ) = repmat(pn,2,1);

% Useful parameter
N = 31;   % bound on filter length
noiseStddev = 0.75;
confidence = 0.95;

b = [0 0 0 1 0 0 0 0 1 -0.8 0.5 0 0 0 1];
output = filter(b, 1, x);
output = output + noiseStddev*randn( size(output) );

index = N:N+L-1; % index vector for correlation
d = output(index);

% correlation
shiftpn = circshift(pn, -(N-1)); % align pn sequence
[corr, lags] = mycorrelation(d, shiftpn, N-1, 'circ',2);
h_hat=corr(find(lags==0):end);

figure
subplot(2,1,1)
stem(abs(h_hat), 'k'); hold on
stem(abs(b), 'rx')
xlabel('m');
title('$|\widehat{h_m}|$ - CORRELATOR', 'Interpreter', 'latex');
legend('h_{hat}','h')
subplot(2,1,2)
stem(angle(h_hat), 'k'); hold on
stem(angle(b), 'rx')
title('\angle{h_m}');

% LS
[h_hat, epsilon, CI] = myDirectLSPredictor(x(1:index(end)), N, d, confidence);

n = length(d);
p = length(h_hat);
sigma2_w = epsilon/(n-p);

fprintf(['Test 1: simple filter\n',...
    'noise std = %.3f\n',...
    'estimated = %.3f\n'],...
    noiseStddev, sqrt(sigma2_w));

figure
subplot(2,1,1)
stem(abs(h_hat), 'k'); hold on
stem(abs(b), 'rx')
plot(CI, 'k--')
xlabel('m');
title('$|\widehat{h_m}|$ - LS', 'Interpreter', 'latex');
legend('h_{hat}','h')
subplot(2,1,2)
stem(angle(h_hat), 'k'); hold on
stem(angle(b), 'rx')
title('\angle{h_m}');

%% POLYPHASE TEST

upx = zeros( length(x)*2,1 );
upx(1:2:end) = x;

r = filter(b,1,upx);
r = r + noiseStddev*randn( size(r) );

r0 = r(1:2:end);
r1 = r(2:2:end);

% correlation

% H_0 - First filter

index = N:N+L-1; % index vector for correlation
shiftpn = circshift(pn, -(N-1)); % align pn sequence

% Correlation between polyphase component taken after the initial transient  
% and one period of pn sequence
[corr, lags] = mycorrelation(r0(index), shiftpn, N-1, 'circ',2);
h_hat_0=corr(find(lags==0):end);

% figure
% subplot(2,1,1)
% stem(abs(h_hat_0), 'k')
% title('$|\widehat{h_m^0}|$', 'Interpreter', 'latex');
% subplot(2,1,2)
% stem(angle(h_hat_0))
% title('angle{h_m^0}');

% H_1 - Second filter

% Correlation between polyphase component taken after the initial transient  
% and one period of pn sequence
[corr, lags] = mycorrelation(r1(index), shiftpn, N-1, 'circ',2);
h_hat_1=corr(find(lags==0):end);

% figure
% subplot(2,1,1)
% stem(abs(h_hat_1), 'k')
% title('$|\widehat{h_m^1}|$', 'Interpreter', 'latex');
% subplot(2,1,2)
% stem(angle(h_hat_1))
% title('\angle{h_m^1}');

% Union of two polyphase component
h_hat = zeros(length(h_hat_0)+length(h_hat_1), 1);
h_hat(1:2:end) = h_hat_0;
h_hat(2:2:end) = h_hat_1;

figure
subplot(2,1,1)
stem(abs(h_hat), 'k'); hold on
stem(abs(b), 'rx')
xlabel('m');
title('$|\widehat{h_m}|$ - CORRELATOR', 'Interpreter', 'latex');
legend('h_{hat}', 'h')
subplot(2,1,2)
stem(angle(h_hat), 'k'); hold on
stem(angle(b), 'rx')
title('\angle{h_m}');

% LS

% utility vectors (possibly different from before)
index = N:length(x); % index vector for correlation
d0 = r0(index);
d1 = r1(index);

[h_hat_0, epsilon_0, CI0] = myDirectLSPredictor(x( 1:index(end) ), N, d0, confidence);
[h_hat_1, epsilon_1, CI1] = myDirectLSPredictor(x( 1:index(end) ), N, d1, confidence);

h_hat = zeros(length(h_hat_0)+length(h_hat_1), 1);
h_hat(1:2:end) = h_hat_0;
h_hat(2:2:end) = h_hat_1;

CI = zeros( length(CI0)+length(CI1), 1);
CI(1:2:end) = CI0;
CI(2:2:end) = CI1;

n0 = length( r0(N:end));
n1 = length( r1(N:end));
p = N;
sigma2_w0 = epsilon_0/(n0-p);
sigma2_w1 = epsilon_1/(n1-p);

fprintf(['Test 1: simple filter\n',...
    'noiseVar = %.3f\n',...
    'estimated_0 = %.3f\n',...
    'estimated_1 = %.3f\n'],...
    noiseStddev, sqrt(sigma2_w0), sqrt(sigma2_w1));

figure
subplot(2,1,1)
stem(abs(h_hat), 'k'); hold on
stem(abs(b), 'rx')
plot(CI, 'k:')
xlabel('m');
title('$|\widehat{h_m}|$ - LEAST SQUARE ESTIMATE', 'Interpreter', 'latex');
legend('h_{hat}', 'h')
subplot(2,1,2)
stem(angle(h_hat), 'k'); hold on
stem(angle(b), 'rx')
title('\angle{h_m}');

% thresholding
h_hat( abs(h_hat)<CI ) = 0;

figure
subplot(2,1,1)
stem(abs(h_hat), 'k'); hold on
stem(abs(b), 'rx')
plot(CI, 'k:')
xlabel('m');
title('$|\widehat{h_m}|$ - LEAST SQUARE ESTIMATE THRESHOLDED', 'Interpreter', 'latex');
legend('h_{hat}', 'h')
subplot(2,1,2)
stem(angle(h_hat), 'k'); hold on
stem(angle(b), 'rx')
title('\angle{h_m}');