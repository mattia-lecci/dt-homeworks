function [h, epsilon, CI, SEER_n] = myLS(x, N, d, confidence)
%MYLS direct LS predictor
% Predicts the channel h using the direct method using the standard LS alg.
%
% INPUTS:
% * x: data vector of dimension K containing [x(0),...,x(K-1)]
% * N: filter order. N<=K
% * d: desired vector of dimension L=K-N+1 containing [d(N-1),...,d(K-1)]
% ( * confidence: confidence level required for CI. Default: 0.95 )
%
% OUTPUTS:
% * c: the estimated coefficients
% * epsilon: the estimated functional
% * CI: the confidence bound
% * SEER_n: normalized signal-to-estimation error ratio

% arguments check
narginchk(3,4)
if (nargin<4)
    confidence = 0.95;
end

K = length(x);
L = K-N+1;

if ( ~isvector(x) || ~isvector(d) )
    error('x and d have to be vectors')
end
if ( length(d)~=L )
    error('length(d)~=L')
end
if ( N>length(x) )
    error('Cannot be N>K')
end
if ( confidence>=1 || confidence<=0 )
    error('Confidence should be in (0,1) excluded')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(d,2)>1 )
    d = shiftdim(d);
end

% build necessary tools
I = buildI(x,N);
phiInv = inv( I'*I );

% estimate
h = phiInv*I'*d;
epsilon = d'*d - d'*I*phiInv*I'*d;
epsilon = abs(epsilon); % to avoid numerical problems. It should be real>0

% Confidence intervals
DOF = length(d)-N;
sigma2_hat = epsilon/DOF;
studentsConfidence = tinv( (1+confidence)/2, DOF);
CI = studentsConfidence*sqrt( sigma2_hat*diag(phiInv) );

% Normalized signal-to-estimation error ratio
SEER_n = 1/trace(phiInv);
end

function [I] = buildI(x, N)

K = length(x);
L = K-N+1;
I = zeros(L,N);

index = N:-1:1;
for i = 1:L
    I(i,:) = x(index);
    index = index+1;
end

end