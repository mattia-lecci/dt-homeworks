clear variables
close all
clc

% load data
load 'data for HW2.mat'
observation = shiftdim(observation); % column

% create reference x
x = zeros( length(observation)/2 ,1);
pn = pnseq( ones(5,1), '+-1');
L = length(pn);   % length pn seq
x( 1:(2*L) ) = repmat(pn,2,1);

% Split observation into two polyphase components
r0 = observation(1:2:end);
r1 = observation(2:2:end);

% parameters
confidence = 0.95;
exportPlots = false; % export plots

%% LS: choosing N with functional

% init functional vectors
J0_cor = zeros(L-1,1);
J1_cor = zeros(L-1,1);
J0_ls = zeros(L-1,1);
J1_ls = zeros(L-1,1);
Lambda = zeros(L-1,1);

for N = 1:L-1
    % utility vectors (possibly different from before)
    lastind = N+L-1; % index to consider
    d0 = r0(N:lastind);
    d1 = r1(N:lastind);
    
    [~, J0_cor(N)] = myCorrelationEstimator(d0, pn, N);
    [~, J1_cor(N)] = myCorrelationEstimator(d1, pn, N);
    [~, J0_ls(N), ~, Lambda(N)] = myLS(x( 1:lastind ), N, d0, confidence);
    [~, J1_ls(N), ~, ~        ] = myLS(x( 1:lastind ), N, d1, confidence);
end

% plot
%figure('position', [500 500 700 300])
figure
subplot(1,2,1)
plot( J0_cor, 'k--'); hold on; grid on
plot( J0_ls, 'k');
title('$\mathcal{E}^{(0)}$', 'interpreter','latex')
xlim([0.5 L-0.5])
ylim([0 45])
xlabel('N')
ylabel('$\mathcal{E}(N)$', 'interpreter','latex')
ax = gca;
ax.XTick = 0:5:L;
subplot(1,2,2)
plot( J1_cor, 'k--'); hold on; grid on
plot( J1_ls, 'k')
xlim([0.5 L-0.5])
ylim([0 45])
legend('Correlation', 'LS')
title('$\mathcal{E}^{(1)}$', 'interpreter','latex')
xlabel('N')
ax = gca;
ax.XTick = 0:5:L;
if (exportPlots)
    matlab2tikz('../Latex/img/functionals.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%figure('position',[500 500 400 580])
figure
plot( 10*log10(Lambda), 'k'); grid on
title('Signal-to-Estimation Error Ratio')
xlim([1 length(Lambda)])
xlabel('N')
ylabel('(\Lambda_n)_{dB}')
ax = gca;
ax.XTick = 0:5:L;
if (exportPlots)
    matlab2tikz('../Latex/img/seer.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end


%% correlation

N = 5;

% utility vectors
index = N:N+L-1; % index vector for correlation
d0 = r0(index);
d1 = r1(index);
nTc = 0:2*N-1;

% Correlation between polyphase component taken after the initial transient
% and one period of pn sequence
[h_hat_0, J0_cor] = myCorrelationEstimator(d0, pn, N);
[h_hat_1, J1_cor] = myCorrelationEstimator(d1, pn, N);

% Union of two polyphase component
h_hat_cor = zeros(length(h_hat_0)+length(h_hat_1), 1);
h_hat_cor(1:2:end) = h_hat_0;
h_hat_cor(2:2:end) = h_hat_1;

% noise estimation
n0 = length( d0 );
n1 = length( d1 );
noiseVar_cor(1) = J0_cor/(n0-N);
noiseVar_cor(2) = J1_cor/(n1-N);

fprintf(['Noise estimated variance (cor):\n',...
    'estimated_0 = %.3f\n',...
    'estimated_1 = %.3f\n',...
    'mean = %.3f\n'],...
    noiseVar_cor(1), noiseVar_cor(2), mean(noiseVar_cor));

figure(3)
subplot(2,2,1)
stem(abs(h_hat_0), 'k'); hold on; grid on
title('h0')
subplot(2,2,3)
stem(angle(h_hat_0), 'k'); hold on; grid on
subplot(2,2,2)
stem(abs(h_hat_1), 'k'); hold on; grid on
title('h1')
subplot(2,2,4)
stem(angle(h_hat_1), 'k'); hold on; grid on

figure(4)
subplot(2,1,1)
stem(nTc, abs(h_hat_cor), 'k'); hold on; grid on
xlim([nTc(1) nTc(end)])
title('Channel estimates');
ylabel('$|\widehat{h_i}|$', 'Interpreter', 'latex')
subplot(2,1,2)
stem(nTc, angle(h_hat_cor), 'k'); hold on; grid on
xlim([nTc(1) nTc(end)])
ylim([-pi pi])
ylabel('\angle{h_i}');
xlabel('iT_c');

%% LS: performing estimate with chosen N

[h_hat_0, epsilon_0, CI0, SEER_n] = myLS(x( 1:index(end) ), N, d0, confidence);
[h_hat_1, epsilon_1, CI1] = myLS(x( 1:index(end) ), N, d1, confidence);

h_hat_ls = zeros(length(h_hat_0)+length(h_hat_1), 1);
h_hat_ls(1:2:end) = h_hat_0;
h_hat_ls(2:2:end) = h_hat_1;

CI = zeros(length(CI0)+length(CI1), 1);
CI(1:2:end) = CI0;
CI(2:2:end) = CI1;

% noise estimation
noiseVar_ls(1) = epsilon_0/(n0-N);
noiseVar_ls(2) = epsilon_1/(n1-N);

fprintf(['Noise estimated variance (LS):\n',...
    'estimated_0 = %.3f\n',...
    'estimated_1 = %.3f\n',...
    'mean = %.3f\n',...
    'SEER_n = %.3f dB\n'],...
    noiseVar_ls(1), noiseVar_ls(2), mean(noiseVar_ls), 10*log10(SEER_n));

figure(3)
subplot(2,2,1)
stem(abs(h_hat_0), 'k*')
subplot(2,2,3)
stem(angle(h_hat_0), 'k*')
subplot(2,2,2)
stem(abs(h_hat_1), 'k*')
subplot(2,2,4)
stem(angle(h_hat_1), 'k*')

figure(4)
subplot(2,1,1)
stem(nTc, abs(h_hat_ls), 'k*')
plot(nTc, CI, 'k--')
legend('Correlation','LS',...
    sprintf('%.0f%% confidence', confidence*100), 'Location', 'northwest')
subplot(2,1,2)
stem(nTc, angle(h_hat_ls), 'k*')
if (exportPlots)
    matlab2tikz('../Latex/img/channel_estim.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%% THRESHOLDS

% naive thresh on corr
sigma_h = sqrt( mean(noiseVar_cor)/L );
thresh = icdf('normal', (1+confidence)/2, 0, sigma_h);
h_th_cor = h_hat_cor;
h_th_cor( abs(h_hat_cor)<=thresh ) = 0;
% threshold with CI
h_th_ci = h_hat_ls;
h_th_ci( abs(h_hat_ls)<=CI ) = 0;

% disp
disp('CORR, abs: ')
disp(abs( h_th_cor ).')
disp('CORR, angle: ')
disp(angle( h_th_cor ).')
disp('LS, abs: ')
disp(abs( h_th_ci ).')
disp('LS, angle: ')
disp(angle( h_th_ci ).')

% plot
figure
subplot(2,1,1)
stem(nTc, abs(h_th_ci), 'k*'); hold on; grid on
plot(nTc, CI, 'k--')
xlim([nTc(1) nTc(end)])
title('Thresholded LS estimate');
ylabel('$|\widehat{h_i}|$', 'Interpreter', 'latex')
subplot(2,1,2)
stem(nTc, angle(h_th_ci), 'k*'); grid on
xlim([nTc(1) nTc(end)])
ylim([-pi pi])
ylabel('\angle{h_i}');
xlabel('iT_c');
if (exportPlots)
    matlab2tikz('../Latex/img/estim_thresh.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%% reconstruction

% polyphase estimates
r0_cor = filter(h_th_cor(1:2:end), 1, x);
r0_ls = filter(h_th_ci(1:2:end), 1, x);
r1_cor = filter(h_th_cor(2:2:end), 1, x);
r1_ls = filter(h_th_ci(2:2:end), 1, x);

% plot
figure
subplot(2,2,1)
plot( real([r0, r0_cor, r0_ls]) ); grid on
legend('obs','cor','ls')
title('Even estimate: Re')
subplot(2,2,3)
plot( imag([r0, r0_cor, r0_ls]) ); grid on
title('Im')
subplot(2,2,2)
plot( real([r1, r1_cor, r1_ls]) ); grid on
legend('obs','cor','ls')
title('Odd estimate: Re')
subplot(2,2,4)
plot( imag([r1, r1_cor, r1_ls]) ); grid on
title('Im')

% upsampled input
upx = zeros( length(x)*2,1 );
upx(1:2:end) = x;

% total estimate
r_cor = filter(h_th_cor, 1, upx);
r_ls = filter(h_th_ci, 1, upx);

% estimate of the thresholded estimate
e = r_ls(length(h_th_ci)-1:end)-observation(length(h_th_ci)-1:end);
E = e'*e/(length(observation)-length(h_th_ci)-(length(h_th_ci)-1));

fprintf(['Noise estimated variance (LS thresh):\n',...
    'sigma_w^2 = %.3f\n'], E);

% plot
nTc = 0:length(observation)-1;

figure
subplot(2,1,1)
plot(nTc, real(r_ls), 'k' ); hold on; grid on
plot(nTc, real(observation), 'k--')
xlim([nTc(1) nTc(end)])
title('Estimated signal')
ylabel('Re[\cdot]')
subplot(2,1,2)
plot(nTc, imag(r_ls), 'k' ); hold on; grid on
plot(nTc, imag(observation), 'k--')
xlim([nTc(1) nTc(end)])
legend('LS','Observation', 'Location', 'southeast')
ylabel('Im[\cdot]')
xlabel('nT_c')
if (exportPlots)
    matlab2tikz('../Latex/img/signal_estim.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end