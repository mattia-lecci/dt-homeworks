function [h, PDP_C] = channelSimulation(Tc, fd, N, PDP, C, fs, method, Nf)
%CHANNELSIMULATION
% Simulates a radio (fading) channel with classical Delay Spectrum (Jakes)
% Different methods can be used (see: method)
%
% INPUTS:
% * Tc: channel's sampling time
% * fd: doppler spread
% * N: number of samples required (transient already discarded)
% * PDP: Required PDP of the rays. Must be real vector. Considered
%       Nh=length(PDP).
% * ( C: LOS component of h0. Has to be abs(C)<=1. Default: 0 )
% * ( fs: doppler shift. Either scalar (equal for all rays) or vector with
%       length(fs)==length(PDP). Default: 0 )
% * ( method: either 'filter', 'exp' or 'oldExp'. Default: 'filter' )
% * ( Nf: ignored if method=='filter'. Number of positive exponents (half
%       of the total amount). Default: 10 )
%
% OUTPUTS:
% * h: NxNh complex matrix containing h_i in each column starting from h0
% * PDP_C: comprehensive PDP (including C added to h0) normalize to unit
%       energy. Column vector.

% arg check
narginchk(4,8);
if ( nargin<5 )
    C = 0;
end
if ( nargin<6 )
    fs = zeros(length(PDP), 1);
end
if ( nargin<7 )
    method = 'filter';
end
if ( nargin<8 )
    Nf = 10;
end

if ( ~isscalar(Tc) || ~isscalar(fd) || ~isscalar(N)...
        || ~isscalar(C) )
    error('T_Q, fd, N, C need to be scalars')
end
if ( ~isvector(PDP) || ~isvector(fs) )
    error('PDP, fs need to be vectors')
end
if ( ~isscalar(fs) && length(fs)~=length(PDP) )
    error(['fs need either to be a scalar or a vector '...
        'with length(fs)==length(PDP)'])
end
if ( Tc<=0 || fd<=0 || N<=0 )
    error('T_Q,fd,N have to be >0')
end
if ( any(PDP<0) || any(fs<0) )
    error('PDP, fs need to be >=0')
end
if ( all(PDP==0) )
    error('PDP cannot be identically zero')
end
if ( abs(C)>1 )
    error('abs(C)>1');
end
if ( ~any( strcmp(method, {'filter', 'exp', 'oldExp'}) ) )
    error('method must be either filter, exp or oldExp')
end
if ( Nf<=0 )
    error('rays<=0')
end

% fix dimensions
PDP = shiftdim(PDP);
if ( isscalar(fs) )
    fs = ones( size(PDP) )*fs;
else
    fs = shiftdim(fs);
end
% normalize PDP power
PDP = PDP/sum(PDP)*(1 - abs(C)^2);

PDP_C = PDP; PDP_C(1) = PDP_C(1)+abs(C).^2; % total PDP to output

% init
Nc = length(PDP); % number of rays
tc = (0:N-1)'*Tc; % desired samples

% create un-equalized responses
if ( strcmp(method,'filter') )
    h = filterMethod(Tc,fd,N,Nc);
elseif ( strcmp(method,'exp') )
    h = expMethod(Tc,fd,N,Nc,Nf);
elseif ( strcmp(method,'oldExp') )
    h = oldExpMethod(Tc,fd,N,Nc,Nf);
else
    error('Unexpected condition')
end
% equalization
h = h.*sqrt(( ones( size(h,1),1)*PDP.' ));
% adding deterministic component
if ( C~=0 ) % improves performance if C==0
    h(:,1) = h(:,1) + C;
end
% doppler shift
if ( any(fs~=0) ) % improve performance if fs==0
    h = h.*exp(1j*2*pi*tc*fs.');
end

end

function out = filterMethod(Tc, fd, N, Nc)

% h_ds filter coefficients (for fd*Tp=0.1)
a = [1 -4.4153 8.6283 -9.4592,...
    6.1051 -1.3542 -3.3622 7.2390,...
    -7.9361 5.1221 -1.8401 2.8706e-1];
b = [1.3651e-4 8.1905e-4 2.0476e-3 2.7302e-3,...
    2.0476e-3 9.0939e-4 6.7852e-4 1.3550e-3,...
    1.8067e-3 1.3550e-3 5.3726e-4 6.1818e-5,...
    -7.1294e-5 -9.5058e-5 -7.1294e-5 -2.5505e-5,...
    1.3321e-5 4.5186e-5 6.0248e-5 4.5186e-5,...
    1.8074e-5 3.0124e-6];

b = b/filternorm(b,a); % normalizing to unit energy

% init
Tp = 0.1/fd;
L = Tp/Tc; % should be integer
N_bar = ceil( N/L ); % number of samples in Tp

Ntrans = 300; % transient given by the filter (b,a)
Ntot = Ntrans+N_bar;

out = crandn(Ntot, Nc);
% filter
out = filter(b,a,out);
% eliminate transient
out(1:Ntrans,:) = [];
% interp
tp = (0:N_bar-1)'*Tp; % current samples
tc = (0:N-1)'*Tc; % desired samples
out = interp1(tp, out, tc, 'spline');

end

function out = expMethod(Tc,fd,N,Nc,Nf)

% determining exp frequencies
deltaf = fd/Nf;
m = 1:Nf;
f = ( (m-0.5)*deltaf ).';

% init
out = zeros(N,Nc);
t = (0:N-1)'*Tc;
A_im = getAim(fd,f,deltaf);

% create sinusoids separately
for i = 1:Nc
%     phase_I = exp( 1j*rand(1)*2*pi );
%     phase_Q = exp( -1j*rand(1)*2*pi );
    
    for m = 1:Nf
        % random phase for each exp
        phi_I = rand(1)*2*pi;
        phi_Q = rand(1)*2*pi;
        
        tmp_I = exp(1j*2*pi*t*f(m)); % each column contains same f_m, each row same time
        tmp_Q = conj(tmp_I)*exp(1j*phi_Q);
        tmp_I = tmp_I*exp(1j*phi_I);
        
        out(:,i) = out(:,i) +...
            A_im(m)*( tmp_I + tmp_Q );
    end
    
    
end

end

function out = oldExpMethod(Tc,fd,N,Nc,Nf)

% determining exp frequencies
deltaf = fd/Nf;
m = 1:Nf;
f = ( (m-0.5)*deltaf ).';

% init
out = zeros(N,Nc);
t = (0:N-1)'*Tc;
A_im = getAim(fd,f,deltaf);

% create sinusoids separately
for i = 1:Nc
    phase_I = exp( 1j*rand(1)*2*pi );
    phase_Q = exp( -1j*rand(1)*2*pi );
    
    for m = 1:Nf
        % random phase for each exp
        phi_im = rand(1)*2*pi;
        
        tmp_I = exp(1j*(2*pi*t*f(m) + phi_im)); % each column contains same f_m, each row same time
        tmp_Q = conj(tmp_I);
        out(:,i) = out(:,i) +...
            A_im(m)*( tmp_I*phase_I + tmp_Q*phase_Q );
    end
    
    
end

end

function A_im = getAim(fd,f,deltaf)

% Assuming |f|<fd, Jake's Doppler Spectrum
D = 1/(pi*fd)./sqrt( 1 - (f/fd).^2 );
sigma = sqrt( D*deltaf );

A_im = sigma.*randn( size(sigma) );
end