function [h_hat, RSS] = myCorrelationEstimator(d, p, N)
%MYCORRELATIONESTIMATOR
% Estimates channel from given realization and pn sequence
%
% INPUTS:
% * d: channel realization. Assumed starting right after the transient and
%       L=length(p)=length(d) (i.e. d(1)=r(N),...,d(L)=r(L+N-1))
% * p: pn sequence used as input and repeated. No shift needed since it's
%       computed internally based on N
% * N: length of the channel estimation. Cannot be N>L
%
% OUTPUTS:
% * h_hat: channel estimation
% * RSS: residual sum of squares, i.e. ||d-d_hat||^2

% argcheck
narginchk(3,3)

if ( ~isvector(d) || ~isvector(p) )
    error('d and p have to be vectors')
end

L = length(p); % for later use

if ( ~isscalar(N) || N<1 )
    error('N has to be scalar >=1')
end
if ( N>L )
    error('N>L')
end
if ( length(d)~=L )
    error('length(d)~=L')
end

addpath('../../HW1/Matlab')

% init
index = N:L+N-1;
input = [p;...
    p(1:N-1)];

% computing correlation 
[corr, lags] = mycorrelation(d, input(index), N-1, 'circ',2);
h_hat = corr(find(lags==0):end);

d_hat = filter(h_hat,1,input);
RSS = sum( abs(d-d_hat(index)).^2 );

end