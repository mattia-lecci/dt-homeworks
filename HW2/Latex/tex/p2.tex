\section{Problem 2}
\textit{
Let {$h_i(nT_C)$}, $i = 0,1,..N_h-1$, be the impulse response of a radio channel at time $nT_C$, where '$i$' denotes the ray index at lag $iT_C$.
\begin{itemize}
	\item The random part of the Power Delay Profile can be modeled by \underline{two} rays with equal power and relative delay $3T_C$.
	\item The first ray $h_0$ has also a LOS component with a global Rice factor $(K)_{dB} = 2$ dB.
	\item Normalize the overall PDP to have unit statistical power. Report in a Table and Plot $E[|h_i|^2]$,\\
	$i=0,1,..,N_h -1$, in dB.
	\item The Doppler Spectrum  of each ray is 'classical' with $f_dT_C = 2.5 \cdot 10^{-4}$.
	\item Plot $|h_0(nT_C)|$ for $n = 0,1,...,12 \, 000$ (drop the transient!).
	\item Based on the first 1000 samples of $|h_0|$, plot the histogram of $\frac{|h_0|}{\sqrt{E[|h_0|^2]}}$ and compare it with the theoretical pdf. Discuss the result.
	\item Simulate 1000 realizations of the system. Plot the histogram of $\frac{|h_0(500)|}{\sqrt{E[|h_0|^2]}}$ and compare it with the theoretical pdf. Discuss the result.
\end{itemize}
}

In Fig.~\ref{fig:PDP} is shown the Power Delay Profile (PDP) of the requested channel. Only $h_0$ and $h_3$ are shown since they are the only non-zero rays. From the table it is also possible to see the normalization to unit energy of the PDP. Note that a global Rice Factor of 2 dB yields a $6.2$ dB difference between the two rays. In fact, in order to normalize the profile, the LOS component has to be equal to $C^2 = \frac{K}{K+1} = 0.6131 = -2.12 \ dB$, the two non-zero rays (namely rays 0 and 3) are equal to $E[|\tilde{h}_i|^2] \triangleq \sigma_i^2 = 0.1934 = -7.13 \ dB$, thus giving $E[|h_0|^2] = C^2 + \sigma_0^2 = 0.8066 = -0.93 \ dB$.



\begin{figure}[tb]
	\hfill%
	\begin{minipage}{.48\linewidth}
		\centering
		\resizebox{.8\linewidth}{!}{
			\input{img/PDP}
		}
	\end{minipage}
	\hfill
	\begin{minipage}{.48\linewidth}
		\centering
		\setlength{\extrarowheight}{.5mm}
		\setlength{\tabcolsep}{15pt}
		\begin{tabular}{|c|cc|}
			\hline 
			$\tau_n$ [$nT_c$]	& $E[|h_i|^2]$	& $E[|h_i|^2]_{dB}$ \\ 
			\hline
			0 					& 0.8066 		& -0.93 \\ 
			1 					& 0 			& $-\infty$ \\ 
			2 					& 0 			& $-\infty$ \\ 
			3 					& 0.1934 		& -7.13 \\ 
			\hline
			Tot & 1 &  \\ 
			\hline 
		\end{tabular}
	\end{minipage}
	\hfill
	
	\caption{Power Delay Profile of the 2-ray channel}
	\label{fig:PDP}
\end{figure}

Since $f_dT_c = 2.5\cdot10^{-4} \ll 1$ the channel is considered \emph{slowly fading} with respect to the channel sampling period. This means that the channel will have slow variations relatively to the sampling period $T_c$. We are going to use the convention $T_{coh} = \frac{1}{f_d}$ (\emph{coherence time}). In this case $T_{coh} = 4\,000 \ T_c$ which is, as we just said, quite long. In Fig.~\ref{fig:channel_plot} it is possible to see a realization of the channel's first ray $h_0(nT_c)$ over $12\,000$ samples, simulated using the `filter' method, described in the following sections. Note the slow variation due to the very large coherence time and the relatively low variance due to the high LOS component present in $h_0$.

\begin{figure}[b]
	\centering
	\resizebox{.4\linewidth}{!}{
		\input{img/channel_plot}
	}
	\caption{Realization of the channel's first ray $|h_0|$ simulated using the `filter' method. Only this method is reported since the different methods are basically indistinguishable}
	\label{fig:channel_plot}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%
\input{tex/simulation.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[tb]
	\hfill
	\begin{minipage}{0.48\linewidth}
		\centering
		\resizebox{.8\linewidth}{!}{
			\input{img/pdf_h}
		}
		\caption{PDF of $1\,000$ samples of $|h_0|$ vs. the ideal (Rician) PDF}
		\label{fig:pdf_h}
	\end{minipage}
	\hfill
	\begin{minipage}{0.48\linewidth}
		\centering
		\resizebox{.8\linewidth}{!}{
			\input{img/pdf_h500}
		}
		\caption{PDF of $|h_0(500)|$ over $1\,000$ realizations vs. ideal (Rician) PDF with $K_{dB}=5\,dB$}
		\label{fig:pdf_h500}
	\end{minipage}
	\hfill
\end{figure}

\vspace{5mm}

It is then asked to plot the histogram relative to the first $1\,000$ samples of the simulated channel. The plot is shown in Fig.~\ref{fig:pdf_h} compared with the ideal (Rician) distribution. Note that the histogram and the ideal distribution differ quite a lot: this is due to the slow fade of the channel. As previously discussed, in fact, $T_{coh} = 4\,000 \ T_c$, while here we only take a number of samples (i.e. a duration) equal to $T_{coh}/4$. Although the duration of the observation this is not really $\ll T_{coh}$, as we discussed in class the channel should be fairly constant, meaning with very little variations; the distribution, then, should be close to a Dirac's delta. The histogram shows a very spiky behavior, meaning that the channel is, in fact, almost constant (you can see it from the first $1\,000$ samples of Fig.~\ref{fig:channel_plot}). Running lots of different simulations we observed many different types of histograms, ranging from almost uniform distributions reaching pdf values of $3 \div 4$, up to very pronounced delta distributions with pdf's surpassing $100$. We then decided to report an average behavior in order to not exaggerate and give a very extreme impression.

Finally, it is asked to show the distribution of a specific sample of a specific ray over $1\,000$ realizations. Since we're basically calculating an empirical pdf of the random variable $|h_0(500 T_c)|$ over many independent realizations, we expect it to converge to the ideal distribution: the Rician distribution. For the ray $h_0$, in fact, the deterministic (LOS) component is not equal to 0, thus we will have a Rician distribution with the parameter $K>0$. In order to correctly compute the ideal distribution we note that the parameter $K$ that we need is different from the one given as a design parameter ($K_{dB}=2\ dB$). That one, in fact, is comprehensive of the whole PDP's randomness ($K=|C|^2/M_d$, where $M_d = \sum_i \sigma_i^2$), while we are only looking at one ray. In this case, then, we'll have to consider $K_0 = |C|^2/\sigma_0^2$.

From the calculations shown at the beginning of the section about the PDP normalization, it's easy to see that $K_{0,dB} = (|C|^2)_{dB} - (\sigma_0^2)_{dB} = 5\ dB = 3.17$. In Fig.~\ref{fig:pdf_h500} are shown the just described theoretical distribution along with the histogram of the $1\,000$ requested realizations of the channel.