\section{Problem 1} \label{sec:p1}
\newcommand{\E}{\mathcal{E}}
\newcommand{\I}{\mathbf{\mathcal{I}}}
\newcommand{\dd}{\mathbf{d}}
\newcommand{\hLS}{\mathbf{\widehat{h}}^{LS}}
\newcommand{\hCOR}{\mathbf{\widehat{h}}^{COR}}

\textit{
	Consider the observed signal {$r(nT_c)$} in the attached file. Its model is
}

\begin{figure}[h!]
	\centering
	\resizebox{.5\linewidth}{!}{
		\input{img/diag_text.tex}
	}
\end{figure}

\textit{
	where \{$x(kT)$\} is formed by \underline{two} periods of a max-length PN sequence of length 31 (initial condition of shift register is all 1's). The rest is zeros.
	\begin{itemize}
	\item \underline{Estimate} $h$ at $\frac{T}{2}$ and $\sigma_w^2$ by both COR and LS method. Compare results by a Table and Plot (mag.+phase).
	\item Determine suitable values for $N_D$ and $N_h$ where
	\begin{equation*}
		h_i = 0, \quad i < N_D, \quad i > N_D + N_h -1
	\end{equation*}
	i.e. the channel is affected by a delay of $N_D$ samples (at $\frac{T}{2}$) between input and output.
	\item Some coefficients of $\widehat{h}$ are small, maybe they are just noise. Determine a method to establish if $\widehat{h}_i$ is just noise, hence it should be set to zero.
	\end{itemize}
	In the end, what is the revised estimate of $h$ and $\sigma_w^2$?
}\\

First, we generated a maximal-length PN sequence of period $L = 2^r - 1 = 31$ ($r = 5$). To do so, we implemented the recursive equation
\begin{equation}
p(l) = p(l-3) \oplus p(l-5)
\end{equation}
assuming as initial condition a sequence of all 1's, as asked. Then, we built $\{x(kT)\}$ by repeating the generating sequence and and adding zero at the end in order to reach the correct length of the input signal. Of course, knowing that the sampling period of the output of the filter is twice the one at the input, the length of the input signal has to be half the length of the given observation.

As just mentioned, the estimate of the filter coefficients has to be done not at $T$ but at $T_C = \frac{T}{2}$: to do so we used the \emph{polyphase representation} of the system, designed exactly as shown in Figure~\ref{diag:polyphase}. First, we evaluated separately $\widehat{h}^{(0)}$ and $\widehat{h}^{(1)}$ and then we obtained $\widehat{h}$ by interlacing its polyphase components.

\begin{figure}[h!]
	\centering
	\resizebox{.7\linewidth}{!}{
		\input{img/diag_polyphase.tex}
	}
	\caption{Equivalent polyphase diagram}
	\label{diag:polyphase}
\end{figure}

The first thing to do is to decide the filters order. To do so, after setting $L = 31$ (length of PN sequence), we evaluated the functional $\E$ (later described) for both correlation and least square method as a function of N. This helps us choosing a correct estimate of the filter length, since a short filter would miss the complete behavior of the channel and a too long one would fit noise, yielding an imprecise estimate and a very low and incorrect noise variance estimate.
For the correlation method, defining as $d(k)$, $k=N-1,..,N+L-2$, the actual output and $\widehat{d}(k)$ the output evaluated with our estimated filter (and their vector representation $\dd$ and $\widehat{\dd}$), we calculated $\E$ as usual:
\begin{equation}
	\mathcal{E} = \sum_{k = N-1}^{(N-1)+(L-1)} |d(k) - \widehat{d}(k)|^2 = ||\dd - \widehat{\dd}||^2
\end{equation}
For the least square method, the formula used is the same, but it can be evaluated using the following matrices and vectors.

Considering $\mathbf{\widehat{h}}$ the estimate of the unknown system, we define:
\begin{itemize}
\item The correlation matrix of the input signal as $\Phi_{i,n} =  \sum_{k=N-1}^{(N-1)+(L-1)} x^{*}(k-i)x(k-n) \qquad i,n=0,1,...,N-1$
\item The cross-correlation vector $\vartheta = \sum_{k=N-1}^{(N-1)+(L-1)} d(k)x^*(k-n) \qquad n=0,1,...,N-1$
\item The energy of the desired signal $\mathcal{E}_d = \sum_{k=N-1}^{(N-1)+(L-1)}|d(k)|^2$
\end{itemize} 
The cost function becomes

\begin{equation}
	\E = \sum_{k=N-1}^{(N-1)+(L-1)} |d(k) - \widehat{d}(k)|^2 = \E_d - \mathbf{\widehat{h}}^H\mathbf{\vartheta} - \mathbf{\vartheta}^{H}\mathbf{\widehat{h}} + \mathbf{\widehat{h}}^{H} \mathbf{\Phi} \mathbf{\widehat{h}}
\end{equation}

and for the optimal estimate $\hLS$ (later described) it simplifies out as

\begin{equation}
	\E_{min} = \E_d - \mathbf{\vartheta} \hLS
\end{equation}

Another way to compute $\E$ with LS is by defining the following matrix. Considering $K = N + L - 1$

\begin{equation}
	\I = \begin{bmatrix}
	x(N-1) & x(N-2) & \cdots & x(0)   \\
	x(N)   & x(N-1) & \cdots & x(1)   \\ 
	\vdots & \vdots & \ddots & \vdots \\ 
	x(K-1) & x(K-2) & \cdots & x(K-N) 
	\end{bmatrix}
\end{equation}

we then obtain

\begin{equation}
	\E_{min} = \mathbf{d}^H\mathbf{d} - \mathbf{d}^H\I\I^\#\mathbf{d}
\end{equation}
where $\I^\# = (\I^H\I)^{-1}\I^H$ is the pseudo-inverse of $\I$.

As we can see from Fig.~\ref{fig:functionals} it is clear that a suitable value for \textbf{N} is 5. After one or two big steps down, in fact, depending on the polyphase component, the error stabilizes, decreasing only because it is fitting more and more noise. Actually, we could use $N=4$ for the first polyphase component, but since the difference is so little we prefer to maintain this symmetry. To sum up, in the two different algorithms described in next sections we will assume \textbf{N = 5, L = 31}.

\begin{figure}[tb]
	\hfill
	\begin{minipage}{.6\linewidth}
		\centering
		\resizebox{\linewidth}{!}{
			\input{img/functionals}
		}
		\caption{LS and Correlation methods functionals for both polyphase components}
		\label{fig:functionals}
	\end{minipage}
	\hfill
	\begin{minipage}{.35\linewidth}
		\vspace{4mm}
		\centering
		\resizebox{\linewidth}{!}{
			\input{img/seer}
		}
		\caption{Normalized Signal-to-Estimation Error Ratio of the LS estimate in dB}
		\label{fig:seer}
	\end{minipage}
	\hfill
\end{figure}

\subsection{Correlation method}
The Correlation method exploits the main property of PN sequences (being almost uncorrelated), correlating the input sequence with the output obtained from a filter, from instants $k = N-1$ to $N+L-2$ (the first $N-1$ samples are ignored as they're just part of the transient). Of course an FIR type of filter is assumed.

Let $d(k)$ be the output of the filter and $p(k)$ the PN sequence generated as described before; the evaluation of the correlation is performed as:

\begin{equation} \tag{3.331}
	\widehat r_{dx}(i) = \frac{1}{L} \sum_{k=(N-1)}^{(N-1)+(L-1)}d(k)p^*(k-i)_{modL} \simeq h_i \qquad i = 0,1,...,N-1
\end{equation}

Then, the variance of the noise is estimated as

\begin{equation}
	\widehat{\sigma}_w^2 = \frac{\E}{L-N}
\end{equation}

Instead of Eq.~3.364 from the book, we decided to use this slight variation since N is not really negligible with respect to L in this case, and this formula results in an unbiased estimate of the variance. Further motivations for choosing the unbiased estimate, instead of the biased one suggested by the book, will be given when talking about the LS estimator.

Since dividing the observation in its polyphase components retains the same noise in both of them, the estimation of its variance can be performed independently and should be very close; in fact, given that our algorithm returns two values of $\sigma_w^2$ that don't differ too much, we take for the overall estimate their mean, obtaining $\mathbf{ \widehat{\sigma}_w^2 = 0.115 }$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Least Square method}

Using the previously defined vectors/matrices, the solution to the least square problem can be equivalently found in one of the following ways:

\begin{equation} \label{eq:ls_solution}
	\hLS = \mathbf{\Phi}^{-1}\vartheta = \I^\# \mathbf{d} = (\I^H\I)^{-1}\I^H \mathbf{d}
\end{equation}

An (unbiased) estimate of the noise variance can be found just like before, as

\begin{equation}
	\mathbf{ \widehat{\sigma}_w^2 } = \frac{\E_{min}}{L-N} = \mathbf{ 0.109 }
\end{equation}

Note that $L \neq N$ should hold (otherwise we would get a system of N equations in N variables, hence a perfect fit, meaning $\mathcal{E}=0$) and for $L \gg N$, $N$ can be ignored resulting in the same equation as the book (Eq.~3.364, not reported here).
As you can see from Fig.~\ref{fig:channel_estim} and Tab.~\ref{tab:ch_estim}, where we showed the results of $\widehat{h}$ in $T_c = \frac{T}{2}$, the two different estimates yield very similar results, as we expected. In fact, looking at the first equality in Eq.~\ref{eq:ls_solution} and noting that $\vartheta$ is exactly $\hCOR$, then the LS solution is just an adjustment of the Correlation estimate through $\mathbf{\Phi}^{-1}$, based on the fact that the PN sequence is not perfectly uncorrelated. As we know, though, this effect is more and more negligible as L increases.

Moreover, as a consequence of these results, we came to the conclusion that $\mathbf{N_D = 5}$ and $\mathbf{N_h = 5}$, meaning that the channel is affected by a delay of 5 samples between input and output.

As a last observation, we also measured the performance of our estimate using the normalized Signal-to-Estimation Error Ratio $\Lambda_n$. Defining $\mathbf{h}$ and $\mathbf{\widehat{h}}$ respectively as the real and the estimated filter coefficients, the estimation error vector can be obtained as $\mathbf{\Delta h = \widehat{h} - h}$ and consequently the Signal-to-Estimation Error Ratio as

\begin{equation}
	\Lambda_e = \frac{||\mathbf{h}||^2}{E[||\mathbf{\Delta h}||^2]}
\end{equation}

Defining, then, the SNR of the system as $\mathbf{\Lambda} = \frac{M_x||\mathbf{h}||^2}{\sigma_w^2}$, the normalized Signal-to-Estimation Error Ration is obtained as

\begin{equation}
	\Lambda_n = \frac{\Lambda_e}{\Lambda}
\end{equation}

where in our case $M_x$, the statistical power of the input signal, is equal to 1.

Then, from the theory of LS estimators, we know that $\Lambda_n$ can be simply obtained as

\begin{equation}
	\Lambda_n = ( \text{tr} [\mathbf{\Phi}^{-1}])^{-1}
\end{equation}

Using this method we obtained $\mathbf{\Lambda_n = 7.9}$ dB for $N=5$. Given the behavior of the complete curve shown in Fig.~\ref{fig:seer}, the obtained $\mathbf{\Lambda_n}$ seems to be a very good result considering also the necessary trade-off on the decision of N given the value of functional, as previously discussed.

\begin{figure}[tb]
	\hfill%
	\begin{minipage}{.48\linewidth}
		\centering
		\resizebox{\linewidth}{!}{
			\input{img/channel_estim}
		}
		\caption{Channel estimates with Correlation and LS methods}
		\label{fig:channel_estim}
	\end{minipage}
	\hfill
	\begin{minipage}{.48\linewidth}
		\centering
		\resizebox{\linewidth}{!}{
			\input{img/signal_estim}
		}
		\caption{Comparison between the estimated output (using $\hLS_{th}$ from Tab.~\ref{tab:ch_estim}) and the given observation}
		\label{fig:signal_estim}
	\end{minipage}
\end{figure}

One last thing we'd like to remark: since we are dealing with a perfectly known input signal and a given output, we could use them all in order to have a more precise estimate. In fact, the LS algorithm is very general and can easily take care of the further information that we possess. We didn't report the results here since they are, once again, very similar to the previous ones. Furthermore, in a typical case we wouldn't probably have this much data, hence the decision of relying only on the bare minimum (L samples excluding the transient).

%%%%%%%%%%%%%%%%%%%%%%%%%%
\input{tex/thresholds.tex}
%%%%%%%%%%%%%%%%%%%%%%%%%%