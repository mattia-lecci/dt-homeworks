\section{APPENDIX}
\begin{figure}[tb]
	\centering
	\hfill
	\subfigure[Filter method]{
		\includegraphics[width=.45\linewidth]{img/trace_filter}
		\label{fig:trace_filter}
	}
	\hfill
	\subfigure[Old exp method]{
		\includegraphics[width=.45\linewidth]{img/trace_oldExp}
		\label{fig:trace_oldExp}
	}
	\hfill
	\subfigure[New exp method]{
		\includegraphics[width=.45\linewidth]{img/trace_exp}
		\label{fig:trace_exp}
	}
	\hfill
	\caption{Channel's 3D histograms}
\end{figure}

In Figs.~\ref{fig:trace_filter},~\ref{fig:trace_oldExp}~and~\ref{fig:trace_exp} you can see the density distributions of the channel traces respectively for the `filter' method, the `old' exponentials approximation method and the `new' (proposed) exponentials approximation method. The graphs show how many times the channel realization were inside any of the plotted squares. It's basically a 3D version of the usual histogram without any normalization, just a simple counting.

For a very high number of samples you would expect to see an approximation of a 3D Gaussian distribution, but here, as discussed in Problem 2, the number of samples with respect to the chosen Doppler spread $f_d$ is quite low. You can see, then, a hint of the `path' that the channel follows and how quickly/slowly it transits around values. You would expect, then, a pseudo random path, which you can clearly see in Fig.~\ref{fig:trace_filter}-\ref{fig:trace_exp}, but not in Fig.~\ref{fig:trace_oldExp}, which actually looks oddly deterministic.

Here we will show that this behavior is not due to an erroneous implementation, but actually from how the model is described in the book. We report here the fundamental equation (Eq.~\ref{eq:exp_book} from the book), calling $g_i'$ instead of $h_i'$ for the i-th ray to use the same notation of the book:

\begin{equation} \tag{4.252} \label{eq:exp_book}
	g_i'(\ell T_p) = \sum_{m=1}^{N_f} A_{i,m}
	\left[
	e^{j (2 \pi f_m \ell T_p + \varphi_{i,m})} e^{j\Phi_{i,I}} + 
	e^{-j (2 \pi f_m \ell T_p + \varphi_{i,m})} e^{-j\Phi_{i,Q}}
	\right]
\end{equation}

Now, note that Eq.~\ref{eq:exp_book} can be also rewritten as

\begin{align*}
	g_i'(\ell T_p)
	& =
		\left(
		\sum_{m=1}^{N_f} A_{i,m}
		e^{j (2 \pi f_m \ell T_p + \varphi_{i,m})}
		\right) e^{j\Phi_{i,I}} + 
		\left(
		\sum_{m=1}^{N_f} A_{i,m}
		e^{-j (2 \pi f_m \ell T_p + \varphi_{i,m})}
		\right) e^{-j\Phi_{i,Q}}\\
	& \triangleq G(\ell T_p)e^{j\Phi_{i,I}} + G^*(\ell T_p)e^{-j\Phi_{i,Q}}
\end{align*}

where $G(nT_p)=|G(nT_p)| \ e^{j \phi(nT_p)}$ is a complex valued discrete function with both amplitude and phase varying over time. Now, factoring out commons terms

\begin{align*}
	g_i'(\ell T_p)
	& = |G(\ell T_p)| \ e^{j \frac{\Phi_{i,I}-\Phi_{i,Q}}{2} }
		\left(
		e^{j \phi(\ell T_p)} e^{j \frac{\Phi_{i,I}+\Phi_{i,Q}}{2} } +
		e^{-j \phi(\ell T_p)} e^{-j \frac{\Phi_{i,I}+\Phi_{i,Q}}{2} }
		\right)\\
	&= 2 \cos \left( \phi(\ell T_p) + \frac{\Phi_{i,I}+\Phi_{i,Q}}{2} \right)
	|G(\ell T_p)| \ e^{j \frac{\Phi_{i,I}-\Phi_{i,Q}}{2} }
\end{align*}

which is a complex valued function but with constant phase (excluding an additive factor $\pi$ given by the cosine when the sign changes), completely determined by $\Phi_{i,I}$ and $\Phi_{i,Q}$. Therefore, as you can see from Fig.~\ref{fig:trace_oldExp}, the trace of the channel follows a straight line in the complex plane randomly jumping back and forth.

Since the problem lies in the fact that the two terms $G$ and $G^*$ are deterministically correlated (in fact one is the complex conjugate of the other), a simple correction to this surrealistic behavior would be that of independently generating different phases for every ray, for every exponential (different for positive and negative frequencies this time!). It is then clear that $e^{j\Phi_{i,I}}$ and $e^{-j\Phi_{i,Q}}$ are now useless, so we can eliminate them.

Generating $\varphi_{i,m_+}$ and $\varphi_{i,m_-}$ as independent uniform random variables in $[0, 2\pi)$, the new formula now becomes

\begin{equation*}
g_i'(\ell T_p) = \sum_{m=1}^{N_f} A_{i,m}
\left[
	e^{j (2 \pi f_m \ell T_p + \varphi_{i,m_+})} + 
	e^{-j (2 \pi f_m \ell T_p + \varphi_{i,m_-})}
\right]
\end{equation*}

As it can be noted from Fig.~\ref{fig:trace_exp}, its implementation results in a much more realistic behavior, similar to the `filter' method, getting rid of the semi-deterministic behavior of the book's version.