\subsection{Thresholds}
\newcommand{\varW}{\widehat{\sigma}_{w}^2}
\newcommand{\varCOR}{\widehat{\sigma}_{COR}^2}

As we learned from the \emph{Machine Learning} course, it can be shown that:

\begin{equation*}
	\mathbf{Var}[\hLS] = (\I^H\I)^{-1} \sigma_w^2
\end{equation*}

where $\mathbf{Var}[\hLS]$ is the covariance matrix of the estimator. Note that $(\I^H\I)^{-1} = \mathbf{\Phi}^{-1}$, has a constant diagonal equal to $v \triangleq \frac{L-N+2}{(L+1)(L-N+1)}$ (Eq.~3.380). Hence, the variance of each component of the estimate (which is in fact the diagonal of the covariance matrix) is constant and equal to $v \, \sigma_w^2$.

%Considering $\widehat{\dd}$ as the estimate of the observation given by the estimate of the channel (i.e. $\widehat{d}_n = (x * \widehat{h})(n)$), an unbiased estimate of the noise variance is the following:
%
%\begin{equation} \label{eq:sigma_ls_hat}
%	\varW = \frac{ || \dd - \widehat{\dd} ||^2 }{L-N} = \frac{\mathcal{E}}{L-N} \approx  \frac{\mathcal{E}}{L}
%\end{equation}
%
%Note that $L \neq N$ should hold (otherwise we would get a system of N equations in N variables, hence a perfect fit, meaning $\mathcal{E}=0$) and for $L \gg N$, $N$ can be ignored resulting in the same equation as the book (Eq.~3.364, not reported since it's the last equality of Eq.~\ref{eq:sigma_ls_hat}).

Since also $\varW$ is just an estimate, the most correct way to proceed would be that of using a \textit{Student's t} distribution with $L-N$ \textit{degrees of freedom} (DOF) and variance equal to $v \, \varW$ for every component. The \emph{Student's t} distribution is used instead of the usual Gaussian distribution because it incorporates the added `randomness' given by the uncertainty of the the estimate of $\widehat{\sigma}_w^2$. This is translated in a shape very similar to the Normal distribution, but with more prominent tails resulting in a higher probability of sampling a realization farther from the mean. This uncertainty obviously is less of a problem as $L \gg N$, in fact as the DOF tend to infinity, the \textit{Student's t} distribution tends to a Gaussian distribution. We call $z_\alpha^{(L-N)}$ the $\frac{1+\alpha}{2}$ percentile of the distribution with $L-N$ DOF.

Then, choosing an adequate confidence level $\alpha$, it can be shown that a corresponding confidence interval (CI) for $|\hLS_i|$ is

\begin{equation} \label{eq:CI}
	(|\hLS_i| - z_\alpha^{(L-N)}\sqrt{v} \, \widehat{\sigma}_w^2, \ |\hLS_i| + z_\alpha^{(L-N)}\sqrt{v} \, \widehat{\sigma}_w^2)
\end{equation}

Here we always used a confidence level $\alpha=95\%$, since it's high enough to make sure that most of the noise is ignored, but not too high to ignore eventual small useful components.

Now that we developed a way of obtaining confidence intervals of an estimate, we proceed with a simple statistical significance test. We set a null hypothesis and check the probability of this hypothesis of being true. In this case our null hypothesis is $\widehat{h}_i=0$ and we're going to check the confidence of this being true. If the confidence interval defined in Eq.~\ref{eq:CI} includes $0$ (the horizontal axis), then we'll assume that sample to be $0$. Equivalently, using the monolateral CI centered in $0$, we assume the sample to be null if it is contained in that interval (see Fig.~\ref{fig:channel_estim}, Tab.~\ref{tab:ch_estim}).

\begin{table}[b]
	\centering
	\begin{tabular}{|c|cccccccccc|}
		\hline % titles
		$nT_c:$	& $0$	& $1$	&$2$	&$3$	&$4$	&$5$	&$6$	&$7$	&$8$	&$9$ \\ 
		
		\hline % h_cor
		$|\hCOR|$	& $0.08$ & $0.05$ & $0.06$ & $0.13$ & $0.04$ & $1.03$ & $0.95$ & $0.05$ & $0.03$ & $0.58$\\
		
		$\angle\hCOR$	& $-1.47$ & $-1.18$ & $-2.91$ & $-3.12$ & $-2.91$ & $0.53$ & $0.79$ & $0.78$ & $2.89$ & $-0.66$\\
		
		\hline % h_ls
		$|\hLS|$	 & $0.07$ & $0.08$ & $0.03$ & $0.08$ & $0.02$ & $1.04$ & $0.95$ & $0.09$ & $0.03$ & $0.60$\\
		
		$\angle\hLS$	 & $-1.13$& $-0.57$& $2.90$& $3.11$& $2.63$& $0.51$& $0.79$& $0.48$& $1.75$& $-0.61$\\
		
		\hline \hline % h_th_cor
		$|\hCOR_{th}|$	 & $0$& $0$& $0$& $0.13$& $0$& $1.03$& $0.95$& $0$& $0$& $0.58$\\ 
		
		$\angle\hCOR_{th}$ & $0$& $0$& $0$& $-3.12$& $0$& $0.53$& $0.79$& $0$& $0$& $-0.66$\\
		
		\hline % h_th_ls
		$|\hLS_{th}|$	& $0$& $0$& $0$& $0$& $0$& $1.04$& $0.95$& $0$& $0$& $0.60$\\ 
		
		$\angle\hLS_{th}$	& $0$& $0$& $0$& $0$& $0$& $0.51$& $0.79$& $0$& $0$& $-0.61$\\ 
		\hline 
	\end{tabular}
	\caption{Coefficients of the estimated channel in magnitude and phase (radians in $[-\pi, \pi)$). The coefficient $\hCOR_{th}(3T_c)$ should be ignored as discussed in the \emph{Thresholds} section}
	\label{tab:ch_estim}
\end{table}

This is very formal and precise but we wanted to find a simpler, more intuitive way of proceeding. For the correlation method, for example, we could find from the book that

\begin{equation} \tag{3.330} \label{eq:cor_var}
	\text{var}\left[ \widehat{h}_i^{COR} \right] =
		\text{var}\left[ \frac{1}{L}\sum_{k=N-1}^{(N-1)+(L-1)} w(k) p^*(k-n)_{mod\,L} \right]
		\approx \frac{\sigma_w^2}{L} \triangleq \sigma_{COR}^2
\end{equation}

Now, following the idea of confidence intervals previously stated, we could think of defining a simplified version considering $\varCOR \approx \frac{\widehat{\sigma}_w^2}{L}$ as Gaussian, thus using the percentiles of a Gaussian distribution (it follows from before that we can write them as $z_\alpha^{(\infty)}$). For a confidence of $\alpha=95\%$, $z_\alpha^{(\infty)} \approx 1.96$. Since the LS and Correlation estimators are so similar, we could use this approximated result for both of them. In Fig.~\ref{fig:channel_estim} is only reported the previous, more precise CI since the approximated version is so similar that it would have made the plot less clear.

In Tab.~\ref{tab:ch_estim} it is possible to see the effects of the thresholding methods just described combined with the Correlation and LS estimates. Using a confidence of $\alpha=95\%$, the `precise' threshold combined with the LS estimate yields a very clean channel which corresponds to what we expected, while the `simplified' threshold combined with the Correlation estimate results in a bit less precise estimate, where a sample at $3T_c$ is kept while, looking at Fig.~\ref{fig:channel_estim}, it looks just like fitted noise. Using a confidence level of $\alpha=99\%$ though, this extra sample is removed. As we previously said, we also tried to use the whole input and output data in order to have a more precise estimate. This of course reduced the confidence interval yielding a better, although negligible, result.

In conclusion, both thresholding methods are very similar as well as both estimates. Since usually $L$ and (at least a bound on) $N$ are known, as well as the PN sequence sent (which determines $\I$), almost everything can be precomputed and simply stored in memory resulting in a very fast and efficient way of estimating a channel. Even $z_\alpha^{(L-N)}$ could be precomputed once $\alpha$ is decided. Thus, for the LS method, $\I$, $\I^\#$, $z_\alpha^{(L-N)} \sqrt{ v }$ can be precomputed and stored, leaving only simple calculations to be performed, such as

\begin{align*}
	\hat{h}_{LS} = \I^\#\dd  && \widehat{\dd} = \I \, \widehat{h}_{LS}\\
	\varW = \frac{ || \dd - \widehat{\dd} ||^2 }{L-N} && CI: \ z_\alpha^{(L-N)} \sqrt{ v } \, \varW
\end{align*}

Thus, we see no performance difference between the `simplified' and the `precise' threshold methods since all of the complexity can be precomputed once and stored in memory. The only difference seems to be conceptual, meaning it is simpler to come up with and to accept, and avoids tedious calculations.

To conclude this section, we re-estimated the variance of the noise using $\hLS_{th}$. Of course, since the the estimate is different from the LS estimate (which we recall is the one that minimizes the squared error), the estimated noise variance will inevitably be bigger than the one estimated before. In fact, after a simple calculation, we obtain the new estimate $\mathbf{ \widehat{\sigma}_w^2 = 0.121 }$.