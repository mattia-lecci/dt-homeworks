clear variables
close all
clc

load 'data for HW1.mat'
x = shiftdim(x); % column vector

% parameters
NFFT = 8192;
N = floor( length(x)/5 ); % just for correlogram
window = hamming(length(x)); % hamming window for the entire signal
window_welch = hamming(100); % hamming window for the welch estimate
overlap = floor( length(window_welch)/5 );
nAR = 7; % order of the AR model
range = 40; % dB

% PSD estimates
Xper = myperiodogram(x,NFFT);
Xwelch = mywelch(x,NFFT);
Xcor = mycorrelogram(x, N, NFFT);
[Xar, a] = myAR(x, nAR, NFFT);

Xper_w = myperiodogram(x, NFFT, window);
Xwelch_w = mywelch(x, NFFT, window_welch, overlap);
Xcor_w = mycorrelogram(x, N, NFFT, window);

f = (0:NFFT-1).'/NFFT;

%% PLOTS
% rectangular window
PSD = [Xper, Xwelch, Xcor, Xar];
maxPower = max(max( 10*log10(PSD) ));
minPower = maxPower-range; % y axis range in dB
leg = {'Correlogram', 'Periodogram', 'Welch', 'AR model'};

figure
plot(f, 10*log10(Xcor)); hold on
plot(f, 10*log10(Xper));
plot(f, 10*log10(Xwelch), 'LineWidth', 1.5);
plot(f, 10*log10(Xar), 'LineWidth', 1);
legend(leg)
ylim([minPower, maxPower])
xlim([0 1]);
title('PSD estimates - Rectangular Window', 'FontSize', 15)
xlabel('f (normalized)')
ylabel('PSD_{dB}')

figure
plot(f, 10*log10(Xcor)); hold on
plot(f, 10*log10(Xper));
plot(f, 10*log10(Xwelch), 'LineWidth', 1.5);
plot(f, 10*log10(Xar), 'LineWidth', 1);
legend(leg)
ylim([20, maxPower])
xlim([0.15 0.4]);
xlabel('f (normalized)')
ylabel('PSD_{dB}')

% hamming window
PSD = [Xper_w, Xwelch_w, Xcor_w, Xar];
maxPower = max(max( 10*log10(PSD) ));
minPower = maxPower-range; % y axis range in dB

figure
plot(f, 10*log10(Xcor_w)); hold on
plot(f, 10*log10(Xper_w));
plot(f, 10*log10(Xwelch_w), 'LineWidth', 1.5);
plot(f, 10*log10(Xar), 'LineWidth', 1);
legend(leg)
ylim([minPower, maxPower])
xlim([0 1]);
title('PSD estimates - Hamming Window', 'FontSize', 15)
xlabel('f (normalized)')
ylabel('PSD_{dB}')

figure
plot(f, 10*log10(Xcor_w)); hold on
plot(f, 10*log10(Xper_w));
plot(f, 10*log10(Xwelch_w), 'LineWidth', 1.5);
plot(f, 10*log10(Xar), 'LineWidth', 1);
legend(leg)
ylim([20, maxPower])
xlim([0.15 0.4]);
xlabel('f (normalized)')
ylabel('PSD_{dB}')


% zero/pole plot of AR model
figure
zplane(1, [1, a]);