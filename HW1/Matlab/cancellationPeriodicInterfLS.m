%CANCELLATIONPERIODICINTERFLS
% Cancellation (extraction) of a sinusoidal interferer on a wideband signal
% using the LS method
%
% INPUTS:
% * x: signal with interference
% * N: order of the filter to be used (>=1)
% * D: delay to introduce in order to decorrelate the signal (>=0)
%
% OUTPUTS:
% * y: prediction (containing the interferer)
% * e: error of the filter per sample (containing the filtered signal)
% * c_ls: coefficients used by the LS filter

function [y, e, c_ls] = cancellationPeriodicInterfLS(x, N, D)
% arguments check
narginchk(3,3)

if ( ~isvector(x) )
    error('x has to be a vector')
end
if ( ~isscalar(N) || ~isscalar(D) )
    error('N and D have to be scalars')
end
if ( N<=0 )
    error('N<=0')
end
if ( D<0 )
    error('D<0')
end

% cancellation
x_delayed = x(1:end-D);
d = x(D+N:end);

c_ls = myDirectLSPredictor(x_delayed, N, d);
[y,e] = predictUsingLS(x_delayed, c_ls);

end