%MYWELCH estimates the PSD of the given signal
% Function that calculates the welch estimator of the given signal as
% column vector
%
% INPUTS
% * x: the signal to analyze
% * ( NFFT: the length of the FFT to compute. Default: length(window) )
% * ( window: the window function to apply to x before computing the FFT.
% Default: rectangular )
% * ( overlap: number of overlapping samples. Default: length(window)/2 )
%
% OUTPUTS
% * Xwelch: the computed welch. Sample time Ts=1 is assumed

function [Xwelch] = mywelch(x, NFFT, window, overlap)
% check args
narginchk(1,4);
if (nargin<2)
    NFFT = length(window);
end
if (nargin<3)
    Ns_star = 8;
    K = length(x);
    D = floor( 2*K/(Ns_star+1) );
    window = ones(D,1); % default: do not pad
end
if (nargin<4)
    overlap = floor( length(window)/2 ); % default: 50%
end

% validity check
if ( ~isvector(x) || ~isvector(window) )
    error('Inputs x and window need to be vectors')
end
if ( length(x) < length(window) )
    error('The window must be shorter than the signal')
end
if ( overlap >= length(window) )
    error('Cannot overlap more than the window length')
end
if ( NFFT<=0 )
    error('NFFT <= 0')
end
if ( ~isreal(window) )
    error('Window has to be real')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(window,2)>1 )
    window = shiftdim(window);
end

% naming same variables
K = length(x);
D = length(window);
S = overlap;
Ns = floor( (K-D)/(D-S)+1 );

% compute and add periodograms (average at the end)
Xwelch = zeros(NFFT,1);
index = 1:D;
for s = 0:Ns-1
    Xwelch = Xwelch + myperiodogram(x(index), NFFT, window);
    
    index = index + D-S;
end

% average the periodograms
Xwelch = Xwelch/Ns;

end