clear variables
close all
clc

% hw data
load 'data for HW1.mat'

% debug (artificial) data
% t = (0:0.01:1000);
% x = 4*cos(2*pi*1*t) + -1*cos(2*pi*3*t) + 0.8*cos(2*pi*10*t) + 1.5*randn(size(t));

x = shiftdim(x); % column vector

% parameters
N = 7; % filter order
mu_tilde = 0.02;

%% ESTIMATES
[c_ls, epsilon] = myDirectLSPredictor(x(1:end-1), N, x(N+1:end));
[y_k_ls, e_k_ls] = predictUsingLS(x, c_ls);

[c_k, e_k_lms, y_k_lms] = myLMSHelper(x, N, mu_tilde);

J_min_hat = epsilon/( length(x)-N+1 ); % estimator of J_min

%% PLOTS
lsColor = [217 83 25]/255;
lmsColor = [126 47 142]/255;

% signals
figure
subplot(2,1,1)
plot(real(x)); hold on; grid on
plot(real(y_k_ls), 'Color', lsColor);
plot(real(y_k_lms), 'Color', lmsColor);
title('Re[x(k)]') 
legend('x', 'LS', 'LMS', 'Orientation', 'horizontal') 
xlim([0 200])
ylim([-30 30])

subplot(2,1,2)
plot(imag(x)); hold on; grid on
plot(imag(y_k_ls), 'Color', lsColor);
plot(imag(y_k_lms), 'Color', lmsColor);
title('Im[x(k)]')
xlabel('k')
xlim([0 200])
ylim([-30 30])

% coefficients
figure
subplot(2,1,1)
plot([0 size(c_k,1)-1], real([c_ls, c_ls].'), 'k', 'Color', 0.5*[1 1 1]); hold on
plot(real(c_k), 'LineWidth', 1);
title('Re[c_i(k)]')
xlim([0 length(c_k)-1])

subplot(2,1,2)
plot([0 size(c_k,1)-1], imag([c_ls, c_ls].'), 'k', 'Color', 0.5*[1 1 1]); hold on
plot(imag(c_k), 'LineWidth', 1);
title('Im[c_i(k)]')
xlabel('k')
xlim([0 length(c_k)-1])

% error
figure
plot( 20*log10( abs(e_k_ls)), 'Color', lsColor); hold on; grid on
plot( 20*log10( abs(e_k_lms)), 'Color', lmsColor);
plot([0 length(e_k_ls)-1], 10*log10(abs(J_min_hat*[1 1])), 'r--', 'Linewidth', 1.5)
title('Prediction Error', 'FontSize', 15)
xlabel('k')
ylabel('|f_N(k)|_{dB}^2')
legend('LS', 'LMS')
xlim([0 length(e_k_ls)-1])

%% J_min(N)
mu_tilde = 0.02;
maxN = 20;

J_min_hat = zeros(maxN,1);

% ESTIMATES
for N = 1:maxN
[~, epsilon] = myDirectLSPredictor(x(1:end-1), N, x(N+1:end));
J_min_hat(N) = epsilon/( length(x)-N+1 ); % estimator of J_min
end

figure
stem(10*log10(abs(J_min_hat))); hold on;grid on
plot([1 maxN], 10*log10(abs(J_min_hat(end)*[1 1])), 'LineWidth', 1.5)
xlim([0.5 maxN])
legend({'$\widehat{J}_{min}(N)$'},'Interpreter','latex', 'FontSize',14)
title('$\mathbf{\widehat{J}_{min}(N)}$', 'Interpreter','latex', 'FontSize',15)
xlabel('N')
ylabel('$|\cdot|_{dB}$', 'Interpreter','latex', 'FontSize',16)

%% Whitening filter
NFFT = 8192;
E = fft(e_k_ls,NFFT);
f = (0:NFFT-1)'/NFFT;
[corr,lags] = mycorrelation(e_k_ls,e_k_ls,250);

figure
subplot(2,1,1)
plot(f,10*log10(abs(E)))
title('DFT( f_N )')
ylim([10 30])
subplot(2,1,2)
plot(lags,abs(corr))
title('r_{fN}(n)')