clear variables
close all
clc

% parameters
a_star = [1.0000   -0.9970    0.9940   -0.9910];
var = 5;
len = 1e6;
nAR = length(a_star)-1; % order of the AR model
NFFT = 2048;

% artificial AR
x = sqrt(var)*randn(len, 1);
x = filter(1,a_star, x);


[Par, a, sigma_w2] = myAR(x, nAR, NFFT);
f = (0:NFFT-1).'/NFFT;

A = freqz(a_star, 1, NFFT, 'whole');
Par_star = var./(abs(A).^2);

%% PLOTS
leg = {'orig', 'estim'};

figure
plot(f,10*log10(abs(Par_star))); hold on
plot(f,10*log10(abs(Par)));
title('PSD')
legend(leg)

figure
stem(a_star); hold on
stem([1,a])
title('Coefficients')
legend(leg)

figure
zplane(1,a_star)
title('Original AR')

figure
zplane(1,[1, a]);
title('Computed AR')
