%MYCORRELOGRAM estimates the PSD of the given signal
% Utilizes an unbiased estimate of the autocorrelation function as column
%
% INPUTS:
% * x: signal to analyze
% * ( N: number of samples from the autocorrelation to use. The estimate is then
% based on samples r(-N),...,r(0),...,r(N) of the autocorrelation function.
% Cannot be N>length(x)-1. Default: length(x)/5 )
% * ( NFFT: number of points of the FFT. Default: 2N+1 )
% * ( window: window function to use. Default: rectangular )
%
% OUTPUTS:
% * Xcor: PSD estimate of x. Column vector of length NFFT

function [Xcor] = mycorrelogram(x, N, NFFT, window)
% check args
narginchk(1,4);
if (nargin<2)
    N = floor( length(x)/5 ); % default: 1/5 of the length
end
if (nargin<3)
    NFFT = 2*N+1;
end
if (nargin<4)
    window = ones( size(x) ); % default: barlett
end

% validity check
if ( ~isvector(x) || ~isvector(window) )
    error('Inputs x and window need to be vectors')
end
if ( length(x) ~= length(window) )
    error('x and window need to be of the same length')
end
if ( N > (length(x)-1) )
    error('N > (length(x)-1)')
end
if ( NFFT<=0 )
    error('NFFT <= 0')
end
if ( ~isreal(window) )
    error('Window has to be real')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(window,2)>1 )
    window = shiftdim(window);
end

% computing truncated correlation
xw = x.*window;
r_hat = mycorrelation(xw, xw, N);

%% computing PSD
if ( NFFT<length(r_hat) )
    ind_remove = ceil( (length(r_hat)-NFFT)/2 ); % ceil if odd
    r_hat = r_hat( (ind_remove+1):(end-ind_remove) ); % remove first and last elements
end
% zero padd
r_hat = [r_hat;...
    zeros( NFFT-length(r_hat), 1)];
% make r_hat symmetric
shift = min(N, floor( ((NFFT-1)/2) ));
r_hat = circshift(r_hat, [-shift,0]);

Xcor = fft(r_hat);
Xcor = abs(Xcor); % to avoid numeric approximation problems

end