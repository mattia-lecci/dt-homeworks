clear variables
close all
clc

% hw data
load 'data for HW1.mat'
x = shiftdim(x); % column vector

%% PARAMETERS
NFFT = 8192;
f = (0:NFFT-1)'/NFFT;
nTones = 7;

%% 2.a: DFT

window = hamming( length(x) );
X = fft(x.*window, NFFT)/sum(window);

% plotting magnitude/phase
figure
subplot(2,1,1)
plot(f, abs(X))
title('|X(f)|')

subplot(2,1,2)
phase = angle(X);
plot(f, angle(X))
title('arg( X(f) ) [rad]')
xlim([0.15 0.4])

% estimates
[~, loc] = findpeaks(abs(X), 'NPeaks', nTones, 'SortStr', 'descend', 'MinPeakProminence',.4);

f0 = f(loc);
phi0 = angle(X(loc));
A = abs(X(loc));

% printing results
disp('2A) DFT')
for ind = 1:length(f0)
    fprintf('%d: f0=%.3f, A=%.2f, phi0=%.2f rad\n', ind, f0(ind), A(ind), phi0(ind));
end

%% 2.b: correlation

% parameters
maxLag = 100;

% reference
t = (0:length(x)-1)'; % time vector
f0 = [f0; 0.5]; % frequencies found in 2a+dummy
test = exp(1j*2*pi*t*f0'); % column i contains exp @freq f=f0(i)

disp('2B) Correlation')

for ind = 1:size(test, 2)
    [corr, lags] = mycorrelation(x, test(:,ind), maxLag);
    
    % plot
    figure
    plot(lags, real(corr))
    title(['f_0 = ', sprintf('%.3f',f0(ind))], 'FontSize', 15)
    
    % estimate
    ampPhase = corr( lags==0 );
    fprintf('%i: f0=%.3f, A=%.2f, phi0=%.2f rad \n', ind, f0(ind), abs(ampPhase), angle(ampPhase)); 
end

%% 2.c AR Model

% model
[~, a] = myAR(x, nTones, NFFT);
a = [1, a];

% plot
h = freqz(1, a, NFFT, 'whole', 1);

figure
subplot(2,1,1)
plot(f,abs(h))
title('AR model', 'FontSize', 15)
ylabel('1/|A(f)|')
subplot(2,1,2)
plot(f, angle(h))
xlabel('Frequency (normalized)')
ylabel('arg( 1/A(f) ) [rad]')

figure
zplane(1,a)

% estimates
[A, locs] = findpeaks(abs(h));
phi0 = angle( h(locs) );
f0 = f(locs);

% sort by amplitude
[A, ind] = sort(A, 'descend');
phi0 = phi0(ind);
f0 = f0(ind);

disp('2C) AR')
for i = 1:length(locs)
fprintf('%d. f0=%.3f, A=%.2f, phi0=%.2f \n', i, f0(i), A(i), phi0(i));
end

%% 2.d Iterative cancellation

% just an idea of the first proposal with a mually designed IIR filter
% at known frequncies (the last one is dummy just to see the case where
% only noise is extracted)
f0 = [0.221 0.210 0.333 0.5];
deltaf = 0.002; % passband bandwidth

theta0 = 2*pi*f0;
r = 1-pi*deltaf;

% passband parameters
b = pi*deltaf;

for ind = 1:length(theta0)
    a = [1, -r*exp(1j*theta0(ind) )];
    
    x1 = filter(b,a,x);
    
    figure
    subplot(2,1,1)
    plot( real(x1) );
    subplot(2,1,2)
    plot( 10*log10(abs(myperiodogram(x1,NFFT)) ));
end