%RECOVERAMPPHASEFROMCORRELATION
% Utility method: recovers amplitude and initial phase of a complex
% exponential from the given signal, given its frequency.
%
% INPUTS:
% * x: data vector
% * f0: frequency of the complex exponentials to find. Can be a vector
% * ( oversample: oversampling factor of the correlation. Can be used to
% obtain more precise estimates. Computationally heavier. If oversample=1
% no oversample will be done. Default: 1 )
% * ( maxLag: max lag to consider while interpolating. Higher lags mean
% more accurate interpolation, but also higher computational cost. It will
% be ignored if oversample==1. Default: length(x)/3 )
%
% OUTPUTS:
% * A: vector containing estimated non-negative amplitudes. Same length as
% f0
% * phi0: vector containing estimated phases in [-pi,pi]. Same length as f0


function [A, phi0] = recoverAmpPhaseFromCorrelation(x, f0, oversample, maxLag)
% arg check
narginchk(2,4);
if ( nargin<3 )
    oversample = 1; % default: do not oversample
end
if ( nargin<4 )
    maxLag = ceil( length(x)/3 ); % default length if interpolating
end

if ( ~isvector(x) || ~isvector(f0) )
    error('x and f0 have to be vectors')
end
if ( ~isscalar(maxLag) || maxLag<1 )
    error('maxLag has to be a scalar >=1')
end
if ( oversample<1 )
    error('oversample<1')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(f0,2)>1 )
    f0 = shiftdim(f0);
end

% init/utilities
t = 0:length(x)-1; % reference time
if ( oversample==1 )
    maxLag = ceil( 1./(2*f0) ); % consider only a period
else
    maxLag = maxLag*ones( size(f0) ); % consider a longer signal for better interpolation
end

A = zeros( size(f0) );
phi0 = zeros( size(f0) );

for i = 1:length(f0)
    % reference signal
    ref = exp(1j*2*pi*f0(i)*t);
    
    % correlation+interpolation
    [corr, lags] = mycorrelation(ref, x, maxLag(i));
    if ( oversample==1 )
        phi = lags/maxLag(i)*pi;
    else
        % interpolate
        lags = interp(lags, oversample);
        corr = interp(real(corr), oversample);
        % consider only 1 period
        maxLagPhi = ceil( 1/(2*f0(i)) );
        limit = (lags>=-maxLagPhi) & (lags<=maxLagPhi); % mask
        lags = lags(limit); corr = corr(limit);
        phi = lags/maxLagPhi*pi;
    end
    
    % phi0, A
    [pk, loc] = findpeaks(real(corr), 'NPeaks', 1, 'SortStr', 'descend');
    A(i) = pk;
    phi0(i) = phi(loc);
end

end