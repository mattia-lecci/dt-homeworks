%PREDICTUSINGLS
% Given the input signal x and the coefficients of the LS predictor,
% predict x(k) for each k=1,...,length(x)
%
% INPUTS:
% * x: data vector
% * c_ls: coefficients vector
%
% OUTPUTS:
% * y_k_ls: predicted output
% * e_k: error vector d(k)-y_k_ls, where d(k)=x(k)

function [y, e] = predictUsingLS(x, c_ls)
% arguments check
narginchk(2,2)

if ( ~isvector(x) || ~isvector(c_ls) )
    error('x and c_ls have to be vectors')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(c_ls,2)>1 )
    c_ls = shiftdim(c_ls);
end

% init
y = zeros(length(x),1);
e = zeros(length(x),1);
N = length(c_ls);

% transient
% for k=1 x(k-1)=x(0)=0, thus y(1)=0 and e(1)=x(1)
e(1) = x(1);
for k = 2:N
    % x(k-1)
    x_k_1 = [zeros(N-k+1,1);...
        x(k-1:-1:1)];
    
    y(k) = c_ls.'*x_k_1;
    e(k) = x(k)-y(k);
end

% post transient
index = N:-1:1;
for k = N+1:length(y)
    % LS
    y(k) = c_ls.'*x(index);
    e(k) = x(k)-y(k);
    
    index = index+1;
end

end