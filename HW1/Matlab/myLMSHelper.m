%MYLMSHELPER
% Helper function for the LMS 1-step forward predictor algorithm
%
% INPUTS:
% * x: data vector (both input and desired)
% * N: filter order
% * mu_tilde
%
% OUTPUTS:
% * c_k: matrix containing estimated sequence of coefficients in each row
% * e_k: error vector x(k)-y(k) (column) of length(y)=length(x)
% * y_k: prediction (column)

function [c, e, y] = myLMSHelper(x, N, mu_tilde)
% arguments check
narginchk(3,3)

if ( ~isvector(x) )
    error('x has to be a vector')
end
if ( N<1 )
    error('N<1')
end
if ( mu_tilde<=0 || mu_tilde>=2 )
    warning('For stability resons, mu_tilde should be in (0,2)')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end

% init
c = zeros(length(x), N);
e = zeros(length(x), 1);
y = zeros(length(x), 1);

% transient
% for k=1 x(k-1)=x(0)=0, thus y(1)=0 and e(1)=x(1)
e(1) = x(1);
for k = 2:N
    x_k_1 = [zeros(N-k+1,1);...
        x(k-1:-1:1)];
    
    x_power = x_k_1'*x_k_1;
    mu = mu_tilde/x_power;
    [c(k+1,:), y(k), e(k)] = myLMSPredictor(x_k_1, c(k,:), x(k), mu);
end

index = N:-1:1;
for k = N+1:length(x)
    x_k_1 = x(index);
    x_power = x_k_1'*x_k_1;
    mu = mu_tilde/x_power;
    [c(k+1,:), y(k), e(k)] = myLMSPredictor(x_k_1, c(k,:), x(k), mu);
    
    index = index+1;
end

end