clear variables
close all
clc

load 'data for HW1.mat'

% param
N = 2;
D = 1;

[y,e, c_ls] = cancellationPeriodicInterfLS(x,N,D);

figure
subplot(2,2,1)
plot(real(y));grid on
title('y(k)')
subplot(2,2,3)
plot(10*log10(abs(fft(y))));grid on
subplot(2,2,2)
plot(real(e));grid on
title('e(k)')
subplot(2,2,4)
plot(10*log10(abs(fft(e))));grid on

%% model
NFFT = 8192;
Xper = myperiodogram(x,NFFT);
f = (0:NFFT-1)'/NFFT;

figure
plot(f,10*log10(Xper));hold on;grid on
t = 0:499;
x = zeros(size(t));
A = [9.54 4.40 1.27];
f0=[0.221 0.210 0.333];
phi0=[1.65 0.61 2.32];
for i=1:3
x = x+A(i)*exp(1j*(2*pi*f0(i)*t+phi0(i)));
end
x = x+7.7*randn(size(x));
REPROD = myperiodogram(x,NFFT);
plot(f,10*log10(REPROD))
ylim([15 50])

%% reproduction
clear variables
close all
clc

load 'data for HW1.mat'

t = (0:499)';
f0 = [0.221 0.210 0.333]';
phi0 = [1.66 0.51 2.42]';
A = [9.82 4.56 1.28]';

amp = A.*exp(1j*phi0);
reprod = exp(1j*2*pi*t*f0.')*amp + 7*randn(size(t));

figure
plot(real(x));hold on
plot(real(reprod))
xlim([0 500])

NFFT = 8192;
X = myperiodogram(x,NFFT);
REPROD = myperiodogram(reprod,NFFT);
f = (0:NFFT-1)'/NFFT;
figure
plot(f,10*log10(abs(X))); hold on; grid on
plot(f,10*log10(abs(REPROD)))
ylim([5 50])