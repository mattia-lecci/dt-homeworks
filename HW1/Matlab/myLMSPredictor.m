%MYLMSPREDICTOR
% Predictor based on the standard LMS algorithm
%
% INPUTS:
% x: x(k) data vector of length N containing [x(k),...,x(k-(N-1))]
% c: c(k) current coefficients containing [c_0(k),...,c_N-1(k)]
% d: d(k) current desired value
% mu
%
% OUTPUTS:
% next_c: c(k+1), estimate of the next coefficients
% y: y(k), estimate of the next sample
% e: e(k), error of the estimate

function [next_c, y, e] = myLMSPredictor(x, c, d, mu)
% arguments check
narginchk(4,4)

if ( ~isvector(x) || ~isvector(c) )
    error('x and c have to be vectors')
end
if ( length(x)~=length(c) )
    error('length(x)~=length(c)')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(c,2)>1 )
    c = shiftdim(c);
end

% prediction
y = c.'*x;
e = d - y;
next_c = c + mu*e*conj(x);

end