function [c, epsilon] = myDirectLSPredictor(x, N, d)
%MYDIRECTLSPREDICTOR direct LS predictor
% Predicts the coefficients vector c using the direct method (pseudo-inverting
% matrix T)
%
% INPUTS:
% * x: data vector of dimension K containing [x(0),...,x(K-1)]
% * N: filter order. N<=K
% * d: desired vector of dimension L=K-N+1 containing [d(N-1),...,d(K-1)]
%
% OUTPUTS:
% * c: the estimated coefficients
% * epsilon: the estimated error

% arguments check
narginchk(3,3)

K = length(x);
L = K-N+1;

if ( ~isvector(x) || ~isvector(d) )
    error('x and d have to be vectors')
end
if ( length(d)~=L )
    error('length(d)~=L')
end
if ( N>length(x) )
    error('Cannot be N>K')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(d,2)>1 )
    d = shiftdim(d);
end

% build necessary tools
T = buildT(x,N);
pinvT = pinv(T);

c = pinvT*d;
epsilon = d'*d - d'*T*pinvT*d;

end

function [T] = buildT(x, N)

K = length(x);
L = K-N+1;
T = zeros(L,N);

index = N:-1:1;
for i = 1:L
    T(i,:) = x(index);
    index = index+1;
end

end