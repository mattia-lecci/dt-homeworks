%MYPERIODOGRAM estimates the PSD of the given signal
% Function that calculates the periodogram of the given signal as column
%
% INPUTS
% * x: the signal to analyze
% * ( NFFT: the length of the FFT to compute. Default: length(x) )
% * ( window: the window function to apply to x before computing the FFT.
% Default: rectangular )
%
% OUTPUTS
% * Xper: the computed periodogram. Sample time Ts=1 is assumed

function [Xper] = myperiodogram(x, NFFT, window)
% check args
narginchk(1,3);
if (nargin<2)
    NFFT = length(x); % default: do not pad
end
if (nargin<3)
    window = ones( size(x) ); % default: rectangular
end

% validity check
if ( ~isvector(x) || ~isvector(window) )
    error('Inputs x and window need to be vectors')
end
if ( length(x) ~= length(window) )
    error('x and window need to be of the same length')
end
if ( NFFT<=0 )
    error('NFFT <= 0')
end
if ( ~isreal(window) )
    error('Window has to be real')
end

% column vectors. We usually handle column vectors anyway
if ( size(x,2)>1 )
    x = shiftdim(x);
end
if ( size(window,2)>1 )
    window = shiftdim(window);
end

% window power
Mw = sum(window.^2)/length(window);

% PSD estimate
Xper = fft(x.*window, NFFT);
normalization = min( NFFT, length(x) );
Xper = abs(Xper).^2/(normalization*Mw);

end