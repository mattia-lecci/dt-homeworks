%MYCORRELATION complex cross-correlation
% Returns the complex correlation between x and y (conjugating y) as column
% As of now, only vectors of the same size are supported if method '0pad'
% is selected, otherwise no length constraint are present.
% Complex inputs are handled correctly.
% Note that in general Pxy(-n)=Pyx(n)*
%
% INPUTS:
% * x: (complex) data vector
% * ( y: (complex) data vector. If computing autocorrelation it is not
% necessary )
% * ( maxLag: compute correlation only between -maxLag and maxLag.
% Default: length(x)-1 )
% * ( type: either 'circ' or '0pad'. Default: '0pad' )
% * ( per: 1 if x is periodic, 2 if y is periodic. Default: 2 )
%
% OUTPUTS:
% * corr: unbiased estimated of correlation computed
% * lags: corresponding lags of the output corr

function [corr, lags] = mycorrelation(x, y, maxLag, type, per)
% check args
narginchk(1, 5);
if (nargin<2)
    y = x;
end
if (nargin<3)
    maxLag = length(x)-1;
end
if (nargin<4)
    type = '0pad';
end
if (nargin<5)
    per = 2;
end

% validity check
if ( ~isvector(x) || ~isvector(y) )
    error('Inputs x and y need to be vectors')
end
if ( ~any( strcmp(type, {'0pad', 'circ'})) )
    error('type must be either 0pad or circ')
end
if ( per<1 || per>2 )
    error('per must be either 1 or 2')
end

% column vectors
x = shiftdim(x);
y = shiftdim(y);

% consider only the case length(x)=length(y)
% in general an xcorr is not hermitian. If it's an autocorr, then it is
if ( strcmp(type, '0pad') )
    if ( length(x) ~= length(y) )
        error('Only supported xcorr between vectors of the same length')
    end
    
    % perform correlation
    negCorr = positiveCorrelation(y, x, maxLag);
    zeroCorr = (y'*x)/length(x);
    posCorr = positiveCorrelation(x, y, maxLag);
    
    % correlation is hermitian
    corr = [conj( flipud( negCorr ));...
        zeroCorr;...
        posCorr];
    
elseif ( strcmp(type, 'circ') )
    corr = circCorrelation(x,y,per,maxLag);
end

lags = (-maxLag:maxLag)';

end


%% utility
function [posCorr] = positiveCorrelation(x1, x2, maxLag)

% consider only the case length(x)=length(y)
N = length(x1);
index = 1:N;

posCorr = zeros(maxLag, 1);
for i = 1:maxLag
    % shift y to the right
    indx1 = index(1+i:end);
    indx2 = index(1:end-i);
    
    % conjugate y and perform unbiased estimate
    tmp = x2(indx2)'*x1(indx1);
    posCorr(i) = tmp/(N-i);
end

end

function [x1,y1] = prepareSignals(x,y,per)

lx = length(x);
ly = length(y);

% if the non periodic is shorter, zero pad
if ( per==1 && ly<lx )
    x1 = x;
    y1 = [y;...
        zeros(lx-ly, 1)];
elseif ( per==2 && lx<ly )
    x1 = [x;...
        zeros(ly-lx, 1)];
    y1 = y;
end    

end

function corr = circCorrelation(x,y,per,maxLag)

% create index vector and find period
if ( per==1 )
    N = length(x); % period
    L = length(y); % correlation length
else
    N = length(y);
    L = length(x);
end
index = 1:L;

corr = zeros(2*maxLag+1, 1);
for i = -maxLag:maxLag
    indper = mod( index-i-1, N )+1;
    
    % conjugate y and perform unbiased estimate
    if ( per==1 )
        corr(i+maxLag+1) = ( y'*x(indper) )/L;
    else
        corr(i+maxLag+1) = ( y(indper)'*x )/L;
    end
end

end