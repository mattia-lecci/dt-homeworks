%MYAR estimates the PSD of the given signal
% Function that calculates the AutoRegressive model of the given signal
%
% INPUTS
% * x: the signal to analyze
% * nAR: the number of coefficients to evaluate
% * ( NFFT: the length of the FFT to compute. Default: length(x) )
%
% OUTPUTS
% * Par: the computed PSD.
% * a: coefficients of the AR model (row)
% * var_w: variance of the noise used for the AR model

function [Par, a, var_w] = myAR(x, nAR, NFFT)
% check args
narginchk(2,3);
if ( nargin<3 )
    NFFT = length(x);
end

if ( ~isvector(x))
    error('Input x must be a vector')
end
if ( nAR < 1 )
    error('Input nAR must be greater than zero')
end

[r_hat, lags] = mycorrelation(x, x, nAR);

% Inizialization of the autocorrelation matrix
R = zeros(nAR, nAR);

% Help variables for consenquently shift the autocorrelation vector
first = find( lags==1 ); % 1
last = length(lags); % N
index = first:last;

% creating r and R
r = r_hat(index);
for i = 0:nAR-1
    index = index-1;
    R(:, i+1) = r_hat(index);
end

a = -pinv(R)*r;

% Just the formula to evaluate the variance of the noise
var_w = real(r_hat(first-1) + r'*a); % to avoid numerical approximations

A = freqz([1; a], 1, NFFT, 'whole');
Par = var_w./(abs(A).^2);

% row
a = a.';

end


