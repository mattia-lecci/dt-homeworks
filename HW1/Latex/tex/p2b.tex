\subsection{b) Correlation}
In Prob.~1 (Eq.\ref{eq:model}) we modeled the given signal as
\begin{equation*}
	x(k) = w(k) + \sum_{i=1}^{N} A_i e^{j \varphi_i} \ e^{j2 \pi f_i k}
\end{equation*}
where $w(k)$ is white noise.\\
Let's look, now, at the deterministic correlation between power signals:
\begin{equation}
R_{xy}(n) = \lim_{N \rightarrow +\infty} \ \frac{T_c}{(2N+1)T_c} \sum_{k=-N}^N x(k)y^*(k-n)
\end{equation}
We can exploit this correlation to find sinusoidal components in a given unknown signal. In fact these results hold:
\begin{eqnarray}
R_{xy}(n) = \lim_{N \rightarrow \infty} \frac{1}{2N+1} \sum_{k=-N}^N \left( Ae^{j\varphi_1} e^{j2 \pi f_1 k T_c} \right) \left( e^{j2 \pi f_2 (k-n)T_c} \right)^* =%
\left\{
\begin{array}{ll}
Ae^{j\varphi_1} e^{j2 \pi f_0 nT_c}	& f_1 = f_2 \triangleq f_0\\
0									& f_1 \neq f_2
\end{array}
\right.\\
%-------------------
E[R_{xy}(n)] = E\left[ \lim_{N \rightarrow \infty} \frac{1}{2N+1} \sum_{k=-N}^N w(k) \left( e^{j2 \pi f_2 (k-n)} \right)^* \right] = \lim_{N \rightarrow +\infty} \frac{1}{2N+1} \sum_{k=-N}^N E[w(k)] e^{-j2 \pi f_0 (k-n)T_c} = 0
\label{eq:noise_orthog}
\end{eqnarray}

We could easily prove Eq.~\ref{eq:noise_orthog} but probably a much stronger result exists.

Thus, considering $x(k)$ the signal to analyze and $y(k)$ a reference signal (a complex exponential with given frequency $f_0$, unitary amplitude and zero phase), by computing $R_{xy}(n)$ and evaluating it in $n=0$, one could obtain informations on the content of $x(k)$.

If a sinusoidal component is present, in fact, it could be easily checked the behavior of the correlation with the adequate reference signal: if a sinusoid with the same period is present it can be concluded that there actually is such a tone, otherwise if only the zero signal (or random noise) can be seen then that particular tone is not present.

\begin{figure}[t]
	\centering
	\hfill
	\subfigure[Correlation at known frequency. Note the oscillatory behaviour. Finite lenght implies frequency are not perfectly othogonal] {\label{fig:corr_02}\includegraphics[width=.45\linewidth]{img/corr_02}}
	\hfill
	\subfigure[Correlation at random frequency. Note the extremely irregular behaviour and much lower amplitudes] {\label{fig:corr_05}\includegraphics[width=.45\linewidth]{img/corr_05}}
	\hfill
	\caption{Correlations with reference sinusoid}
\end{figure}

Without using any other tool (e.g. a DFT), one should theoretically create $P$ reference signals with different frequencies, one for each frequency to check. For example, since a 3 digit precision is required, without having any clue on the input signal $P=1000$ reference signals should be created, correlated and checked. This is obviously not very practical.\\
Moreover, since we have a finite length signal, different frequencies are not perfectly orthogonal and since it is also quite short, noise may be particularly troubling.

\begin{table}[b]
	\centering
	\begin{tabular}{|c|ccccccc|}
		\hline 
		Correlation method & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ 
		\hline
		$f_0$ & 0.221 & 0.210 & 0.333 & 0.104 & 0.682 & 0.058 & 0.829 \\ 
		A & 9.54 & 4.40 & 1.27 & 0.56 & 0.74 & 0.45 & 0.55 \\ 
		$\varphi_0$ [rad] & 1.65 & 0.61 & 2.50 & 2.27 & -2.06 & 1.10 & 0.60 \\ 
		\hline 
	\end{tabular}
	\caption{Using the frequencies found the DFT (to avoid computing and manually checking $1000$ correlations). Note the similar results given by the two methods, at least for the three biggest peaks.}
\end{table}

PROS:
\begin{itemize}
	\item Correlation is a simple operation, although it may be quite costly
	\item If the reference signal is correct, you only need to check the correlation in $n=0$
\end{itemize}
CONS:
\begin{itemize}
	\item As previously said, correlation is not very computationally efficient
	\item For finite length signals, different frequencies are not perfectly orthogonal (Fig.~\ref{fig:corr_05})
	\item Without using any other tool, it should be checked manually whether the correlation looks like a complex sinusoid with the same frequency or not
	\item A lot of different references should be created, correlated and checked
\end{itemize}
ASSUMPTIONS:
\begin{itemize}
	\item Different frequencies are assumed orthogonal
	\item Noise is ignored even though it may bias the results
\end{itemize}