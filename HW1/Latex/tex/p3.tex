\section{Problem 3}
\textit{
	Run the predictor for the process using the LMS algorithm. Plot $|f_n(k)|^2$, in dB, vs $k=0,1,...,499$.\\
	Draw also the convergence value as given by the direct method. The value of N should be as determined in Problem 1. \\
	In another plot draw $Re[c_i(k)]$ and $Im[c_i(k)]$ vs $k=0,1,...,499$ , for vs $i=1,2,...,N$ (same figure), together with values at convergence as determined by the direct method. Report value of $ \widetilde{\mu}$ and $N$.
}
\\

%Given \textbf{x}(k-1) we want an estimate of \textbf{x}(k). This estimate can be done implementing a \textit{one step forward linear predictor} of order \textit{N}, an application of the Wiener filter.\\
%We want to minimize $J = [|f_N(k)|^2]$ given that: 
%\begin{equation}
%f_N(k)=d(k) - \sum_{n=1}^{N}c_nx(k-n) 
%\end{equation}
%The do so, we need to find optimum coefficients of the filter. The following described and implemented methods are useful when just a single realization the signal is available. Given the considerations made for the Problem 1 we use $N = ???$.


\subsection{Direct method}
To determine the coefficients with the \textbf{\textit{LS method}} first we need to define:
\begin{itemize}
	\item $ \textbf{T} = \begin{bmatrix}
	x(N-1) & x(N-2) & \cdots & x(0)   \\ 
	x(N)   & x(N-1) & \cdots & x(1)   \\ 
	\vdots & \vdots & \ddots & \vdots \\ 
	x(K-1) & x(K-2) & \cdots & x(K-N) 
	\end{bmatrix} $
	\item $\Phi_{i,n} =  \sum_{k=N-1}^{K-1} x^{*}(k-i)x(k-n) = \mathbf{T}^{H}\mathbf{T} \qquad i,n=0,1,...,N-1$
	\item $\vartheta = \sum_{k=N-1}^{K-1} d(k)x^*(k-n) \qquad n=0,1,...,N-1$
	\item $\mathcal{E}_d = \sum_{k=N-1}^{K-1}|d(k)|^2$
	\item $\mathcal{E} = \sum_{k=N-1}^{K-1} |e(k)|^2 =\mathcal{E}_d - \textbf{c}^H\mathbf{\vartheta}-\mathbf{\vartheta}^{H}\mathbf{c}+\mathbf{c}^{H} \mathbf{\Phi} \mathbf{c}$

\end{itemize}
where $\mathcal{E}$ is the functional that we want to minimize. Given that, after calculating its gradient we find a close form expression for $c_{ls}$:
\begin{equation}
	\mathbf{\Phi} \, \mathbf{c_{ls}} = \mathbf{\vartheta}
\end{equation}
Note that if $\Phi^{-1}$ exists, $\Phi^{-1}\vartheta$ and $\mathbf{T^\#d}$ coincide (where $\mathbf{T^\#}$ is the pseudo-inverse of \textbf{T}). Then, since matrices T and d are easier to construct that $\Phi$ and $\vartheta$ and we are not implementing the algorithm into an actual device (thus we are not concerned with computational complexity), we decided to always use the following:
\begin{equation}
	\mathbf{c}_{ls} = \mathbf{T}^{\#} \mathbf{d}
\end{equation}


\subsection{Least mean square algorithm}
\begin{equation}
	\mathbf{c}(k+1) = \mathbf{c}(k) + \mu \mathbf{e}(k)\mathbf{x}^*(k)
\end{equation}

\begin{figure}[t]
	\centering
	\hfill
	\subfigure[Convergence of coefficients. As you can see after $\approx 100$ samples they are almost stable even though they do not perfectly converge to the optimal values predicted by the LS algorithm] {\label{fig:predCoeff}\includegraphics[width=.48\linewidth]{img/predictedCoefficients}}
	\hfill
	\subfigure[Actual and predicted versions of the signal for k=0,...,200 with both LS and LMS predictors] {\label{fig:predSgn}\includegraphics[width=.48\linewidth]{img/predictedSignal}}
	\hfill
	\caption{Coefficients and prediction}
\end{figure}

The LMS algorithm relies on an iterative procedure which keeps on updating the coefficients, searching for convergence values. The key for assuring stability is the choice of a good value of $\widetilde{\mu} = M_x\mu \:\: (0 < \widetilde{\mu} < 2)$: in fact if we choose $\widetilde{\mu}$ too small convergence comes after a long number of iteration, which is not good in general but in particular when we have a realization with a limited number of samples to analyze; on the other hand, if we set up a prediction with $\widetilde{\mu}$ too high, the coefficients will be affected by a really high variance and $\mathcal{E}$ will increase with respect to $\widehat{J}_{min}$. Given that, we ran different predictions and we found $\widetilde{\mu} = 0.02$ giving a good trade off between variance and convergence speed. This is shown by the trend of coefficients for $k = 0,...,499 $ plotted in Figure~\ref{fig:predCoeff}, where we can see how after 100-150 iterations there is a sort of stability around the optimum value $c_{ls}$ calculated with the direct method and plotted as continuous lines; it is confirmed also by how the estimate of $J_{min}$ converges very closely to the one of the LS method.

\begin{figure}[t]
	\centering
	\includegraphics[width=0.6\linewidth]{img/whitening}
	\caption{Whitening behavior of the LS filter. The DFT in dB units and the autocorrelation in linear units of $f_N(k)$ are shown}
	\label{fig:whitening}
\end{figure}

In order to test the goodness of our predictions we compared the given signal with our predicted versions. Figure~\ref{fig:predSgn} shows clearly what already stated: thanks to the direct computation, LS starts to follow the signal almost immediately (of course after the transient of N-1 samples); LMS instead, as expected, needs a longer transient to settle the coefficients on the right value. 

As we can see in Figure~\ref{fig:predErr}, using as reference $\widehat{J}_{min} = \frac{\mathcal{E}}{K-N+1}$ (plotted with dashed red line), we compared errors obtained with the two algorithms: both of them follow closely the predicted $\widehat{J}_{min}$. Moreover it can be clearly seen that LMS starts with an higher error and converges after some values of k. The reason why LS and LMS almost perfectly overlap is that the chosen $\tilde{\mu}$ is really small and so the added noise is almost negligible.
In Figure~\ref{fig:Jmin} we show $\widehat{J}_{min}$ as a function of N, confirming that our choice of N in Problem~1 was actually good (even though there is a very small improvement from N=1 to N=20 as shown in Fig.~\ref{fig:Jmin}). In conclusion, in order to evaluate if the value of N that we've chosen for our filter was high enough we also tested if, using as input the AR model, the output was white noise (confirming the property of whitening filter). In Figure~\ref{fig:whitening} we confirmed that our assumptions were good.

\begin{figure}[t]
	\centering
	\hfill
	\subfigure[Prediction error with LS and LMS. Dashed line is $\widehat{J}_{min}$]{\label{fig:predErr}\includegraphics[width=.48\linewidth]{img/predictionError}}
	\hfill
	\subfigure[{Estimated $\widehat{J}_{min}$ as a function of N}]{\label{fig:Jmin}\includegraphics[width=.48\linewidth]{img/jmin}}
	\hfill
	\caption{Errors and $\widehat{J}_{min}$}
\end{figure}