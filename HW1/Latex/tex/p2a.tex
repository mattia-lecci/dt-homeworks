\subsection{a) DFT}
Consider a discrete time complex exponential signal $x(k) = Ae^{j \varphi} \ e^{j2 \pi f_0 k}$, at a fixed frequency $f_0$, amplitude $A$ and phase $\varphi$.\\
As we know, the DTFT (\emph{Discrete Time Fourier Transform}) of such signal is simply $X(f) = Ae^{j\varphi} \ \delta(f-f_0)$. Consider now a window $w(k)$. Then, from well known properties of the \textit{Fourier Transform}, the windowed signal will have a frequency spectrum equal to the spectrum of the window, centered in $f_0$ and multiplied by $Ae^{j\varphi}$:

\begin{equation*}
\begin{array}{lcl}
	x(k) = Ae^{j \varphi} \ e^{j2 \pi f_0 k} & \longrightarrow & X(f) = Ae^{j\varphi} \ \delta(f-f_0)\\
	x_w(k) = x(k)w(k)						 & \longrightarrow & X_w(f) = Ae^{j\varphi} \ W(f-f_0)
\end{array}
\end{equation*}

Since Matlab assumes the window to be rectangular as follows:
\begin{equation*}
w_K(k) =%
\left\{
\begin{array}{ll}
1 & 0 \leq k \leq K-1\\
0 & \text{otherwise}
\end{array}
\right.
\end{equation*}
its transform is equal to
\begin{equation*}
	W_K(f) = e^{-j\pi f(K-1)} \frac{\sin(\pi fK)}{\sin(\pi f)}
\end{equation*}
which in $f=0$ is equal to $K$, hence the phase shift is ignored.

In fact, this holds for this particular window:
\begin{equation*}
\frac{X_{w_K}(f_0)}{K} = Ae^{j \varphi}
\end{equation*}
Since in general $W(0) = \sum_k w(k) \triangleq Area_w$, it is sufficient to normalize $X_w(f_0)$ by the area of the arbitrarily chosen window in order to obtain the same result.
The frequency $f_0$ can be easily found by searching for $\arg\max |X_w(f)|$. Of course we cannot compute the DTFT: we have to rely only on the DFT which unfortunately introduces errors due to the sampling in frequency. If the sampling is high enough, though, the "discrete" peak will be very close to the actual "continuous" peak, hence the estimate will be good enough.

These results hold if we only have one pure complex tone. As soon as we introduce other tones and/or noise, we will have estimates that won't be perfect.\\

\begin{figure}[t]
	\centering
	\includegraphics[width=0.6\linewidth]{img/DFT}
	\caption{DFT in linear units}
	\label{fig:dft}
\end{figure}


If we consider two tones, for example, assuming that they have sufficiently different frequencies, the side lobes of each other's window will interfere slightly shifting the peaks and modifying amplitudes and phases. We can mitigate the shift of the peaks and the amplitude change by choosing a window with low side lobes (e.g. hamming window), which however will have a larger main lobe thus losing spectral resolution. In this case, the tones seem to be far enough, then probably a hamming window would be the best choice.\\
Of course noise will also affect the spectrum of the signal especially if it's not very long. It will in fact introduce "spectral noise", making appear false peaks and staining amplitudes, frequencies and phases of the signals we are interested in.

\begin{table}[b]
	\centering
	\begin{tabular}{|c|ccccccc|}
		\hline 
		DFT method & 1 & 2 & 3 & 4 & 5 & 6 & 7 \\ 
		\hline 
		$f_0$ & 0.221 & 0.210 & 0.333 & 0.104 & 0.682 & 0.058 & 0.829 \\ 
		A 	  & 9.82 & 4.56 & 1.28 & 0.92 & 0.86 & 0.82 & 0.80 \\ 
		$\varphi_0$ [rad] & 1.66 & 0.51 & 2.42 & 2.00 & -1.91 & 0.77 & 0.80 \\
		\hline
	\end{tabular}
	\caption{Estimates of the sinusoidal components. Note that of the frequency found are the ones predicted by us in Prob.1}
\end{table}

PROS:
\begin{itemize}
	\item Simple and classical way to proceed
	\item Modern FFT algorithms have great performance and can be highly optimized and retailed to the specific device
	\item Quite simple to automatize
	\item Can show at a glance the general behavior of the signal
\end{itemize}
CONS:
\begin{itemize}
	\item The complexity is still $O(N logN)$
	\item False peaks introduced by noise may be misleading
	\item Should be done with a high enough sampling rate (in frequency) in order to correctly locate peaks
	\item Different windows have different effects on the result, hence many of them should be used in order to avoid misinterpretations of the spectrum
\end{itemize}
ASSUMPTIONS:
\begin{itemize}
	\item If we are looking for pure tones, we assume that they are far apart enough so that they can be resolved
	\item Even though it obviously affects the results, we do not consider the contribution given by the noise
\end{itemize}