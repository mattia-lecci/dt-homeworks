\section{Problem 4}
\textit{
	Show equation (1.447) of the textbook
}\\

The problem requires the proof the the Wiener-Kinchine Theorem
\begin{equation} \tag{1.447}
	E\left[ \frac{1}{KT_c} \left| T_c \sum_{k=0}^{K-1} x(k) e^{-j2 \pi fkT_c} \right|^2 \right] = \mathcal{P}_x(f)
\end{equation}
Since $\mathcal{P}_x(f) = T_c \mathcal{F}[r_x(n)]$, the equation only makes sense if the Fourier Transform of the autocorrelation exists. This is true if $r_x(n) \in \ell^1$, i.e. if
\begin{equation} \label{eq:r_l1}
\sum_{n=-\infty}^{+\infty} |r_x(n)| < \infty
\end{equation}

The generalization to $\ell^2$ sequences would be much more difficult and we still haven't the right tools to formally prove it. Anyway, since we are following the course \emph{Mathematical Methods for Information Engineering}, we will integrate the newly acquired mathematical concepts to give a more precise demonstration.

Recall: the book defines the Power Spectral Density of a discrete random process in Equation (1.247) as
\begin{align} \tag{1.247}
\mathcal{P}_x(f) = T_c \ \mathcal{F}[r_x(n)] = T_c \sum_{n=-\infty}^{+\infty} r_x(n) e^{-j2 \pi fnT_c}
\end{align}

Let's find the equivalence in steps.
\begin{align*}
\left| T_c \sum_{k=0}^{K-1} x(k) e^{-j2 \pi fkT_c} \right|^2
& = \left( T_c \sum_{l=0}^{K-1} x(l) e^{-j2 \pi flT_c} \right) \left( T_c \sum_{m=0}^{K-1} x(m) e^{-j2 \pi fmT_c} \right)^*\\
& = T_c^2 \sum_{l,m=0}^{K-1} x(l)x^*(m) e^{-j2 \pi f(l-m)T_c}
\end{align*}

Taking the expectation we obtain
\begin{align*}
E\left[ \left| T_c \sum_{k=0}^{K-1} x(k) e^{-j2 \pi fkT_c} \right|^2 \right]
& = T_c^2 \sum_{m=0}^{K-1} \sum_{l=0}^{K-1} E[x(l)x^*(m)] e^{-j2 \pi f(l-m)T_c}\\
& = T_c^2 \sum_{m=0}^{K-1} \sum_{l=0}^{K-1} r_x(l-m) e^{-j2 \pi f(l-m)T_c}\\
& = T_c^2 \sum_{n=-(K-1)}^{K-1} (K-|n|) r_x(n) e^{-j2 \pi fnT_c}
\end{align*}
The last equality can be proved with this simple reasoning: since both $l,m \in [0, K-1]$, obviously their difference will be included in $[-(K-1),K-1]$. Since there are $K$ ways to obtain $n=l-m=0$ ($l=m=0,1,...,K-1$), $K-1$ ways to obtain $1$ ($l=m+1=1,2,...,K-1$), the same for $-1$ with inverted indexes, and so on up to $\pm (K-1)$ (which can be obtained only when $l=K-1, \, m=0$ and vice versa), the equality is proved.

Now passing to the limit
\begin{align*}
\lim_{K \rightarrow +\infty} \ E\left[ \frac{1}{KT_c} \left| T_c \sum_{k=0}^{K-1} x(k) e^{-j2 \pi fkT_c} \right|^2 \right]
& = \lim_{K \rightarrow +\infty} T_c \sum_{n=-(K-1)}^{K-1} \left( 1-\frac{|n|}{K} \right) r_x(n) e^{-j2 \pi fnT_c}\\
& = \lim_{K \rightarrow +\infty} T_c \sum_{n=-\infty}^{+\infty} g_K(n)
\end{align*}
where we defined
\begin{align*}
g_K(n) \triangleq
\left\{%
\begin{array}{ll}
\left( 1-\frac{|n|}{K} \right) r_x(n) e^{-j2 \pi fnT_c} & |k| \leq K-1, \; K>0\\
0 														& \text{otherwise}
\end{array}
\right.
\end{align*}

Defining also $g(n) = r_x(n)e^{-j2 \pi fnT_c}$, note that
\begin{itemize}
	\item $g_K(n) \xrightarrow{K\longrightarrow \infty} g(n),  \; \forall n$ (i.e. pointwise convergence)
	\item $|g_K(n)| \leq |g(n)| \; \forall K,n$
	\item $\sum_{n=-\infty}^{+\infty} g(n) = \mathcal{F}[r_x(n)] = \frac{\mathcal{P}_x(f)}{T_c}$
\end{itemize}

Hence, from Lebesgue's \emph{Dominated Convergence Theorem} (Th.\ref{thm:dom_conv}) (see below) we have that

\begin{align} \label{eq:WK_last}
\lim_{K \rightarrow +\infty} \ E\left[ \frac{1}{KT_c} \left| T_c \sum_{k=0}^{K-1} x(k) e^{-j2 \pi fkT_c} \right|^2 \right]
= \lim_{K \rightarrow +\infty} T_c \sum_{n=-\infty}^{+\infty} g_K(n)
= T_c \sum_{n=-\infty}^{+\infty} g(n)
= \mathcal{P}_x(f)
\end{align}

Hence proving Equation~(1.447)

\begin{thm}[Dominated Convergence Theorem] \label{thm:dom_conv}
	Given the measure space $(X, \mathcal{A}, \mu)$, let ${f_n}$ be a sequence of measurable functions on such space. If
	\begin{itemize}
		\item $f_n \rightarrow f$ pointwise
		\item $\exists h(x)$ s.t. $|f_n(x)| \leq h(x) \; \forall x \in X$
		\item $h(x)$ is a (Lebesgue) integrable function, i.e. $\int_X |h| \, d\mu < \infty$
	\end{itemize}
	then $f$ is integrable and
	\begin{equation*}
	\lim_{n \rightarrow \infty} \int_X f_n \, d\mu = \int_X f \, d\mu
	\end{equation*}
\end{thm}

Considering the measure space $(\mathbb{Z}, \mathbb{P}(\mathbb{Z}), \mu)$, where $\mathbb{P}(\mathbb{Z})$ is the power set of the integers and $\mu$ the counting measure, namely
\begin{itemize}
	\item $A$ finite set, then $\mu(A) = |A|$ (cardinality of $A$)
	\item $A$ infinite set, then $\mu(A) = +\infty$
\end{itemize}
we obtain the result for doubly infinite series and sequences of discrete time functions (in fact in abstract integration, sums and series are just integrals defined on a proper measure space).\\
Taking, then, $f_n = g_K$, $f = g$ and $h(n) = |g(n)| = |r_x(n)|$ and recalling Eq.\ref{eq:r_l1}, we justify Eq.\ref{eq:WK_last}:

\begin{equation*}
	\lim_{n \rightarrow \infty} \int_X f_n \, d\mu
	= \lim_{K \rightarrow \infty} \sum_{n=-\infty}^{+\infty} g_K(n)
	= \sum_{n=-\infty}^{+\infty} g(n)
	= \sum_{n=-\infty}^{+\infty} r_x(n)e^{-j2 \pi fnT_c}
	= \mathcal{F}[r_x(n)]
	= \frac{ \mathcal{P}_x(f) }{T_c}
\end{equation*}