\section{Problem 1}
\textit{
	From the attached file containing 500 samples of the signal, determine an estimate of its PSD by using the following methods: \textit{Correlogram}, \textit{Periodogram}, \textit{Welch}, \textit{AR model}.\\
	Report all four estimates on the same figure, using different colors. The frequency scale should be [0, 1], while the amplitude should be in dB, with a dynamic of 40 dB. Do not make use of MATLAB macros for the spectral estimate. \\ 
	Report values of all \textbf{parameters}.
	Determine  mathematical model of the given process.\\
	Plot zeros of A(z) in the z-plane.
}

\subsection{Periodogram}
The periodogram method to estimate PSD uses an estimate of the statistical power of $x(k)$ and the properties of Fourier transform to obtain: 
\begin{equation}
	\mathcal{P}_{PER}(f) = \frac{|\mathcal{X}(f)|^2}{KT_{c}M_{w}}
\end{equation}
where $M_w$ is the normalized energy of the window and $K$ the length of he signal. For this one and any further evaluation, we always assumed the sample time $\boldsymbol{T_c = 1}$ and we set the length of the FFT (and thus the frequency sampling) to $\boldsymbol{NFFT = 8192}$. %The value of $K$ we've chosen is the \textbf{minimum} between $NFFT$ and the length of the signal, because of course we need to take into account if the signal has been truncated by the FFT.

\subsection{Welch}
The welch method is used in order to lower the variance of the PSD. Different partially overlapping subsequences of consecutive \textbf{D} samples are extracted, the Periodogram is computed for each subsequence and as last step, the PSD is estimated as the average of all Periodograms.\\
In formula:
\begin{equation}
	\mathcal{P}_{WE}(f) = \frac{1}{N{s}}\sum_{s=0}^{N_s-1}\mathcal{P}^{(s)}_{PER}(f)
\end{equation}
where $S$ is the number of overlapping samples and $N_s = \left \lfloor \frac{K - D}{D - S} + 1 \right \rfloor$ is the number of subsequences. By default we calculated the number D of samples required to obtain \emph{at least} 8 subsequences with \textbf{50\%} overlap, since the signal was very short. Calculating D as $\lfloor 2K/(N_s+1) \rfloor$ we obtained $D = 111$ and $S = 55$. Other tests have been done but these parameters already gave nice results.
\subsection{Correlogram}
Correlogram uses an estimate of the ACS (AutoCorrelation Sequence), obtaining the PSD as the Fourier transform of the ACS windowed from -L to L 
\begin{equation}
\mathcal{P}_{COR}(f) = T_{c}\sum_{n = - L}^{L}w(n)\hat{r}_x(n)e^{-2\pi fnT_c}
\end{equation}
where we've chosen $L = 100$ and we used the \textit{unbiased} ACS estimate
\begin{equation}
\hat{r}_{x}(n) = \frac{1}{K-n}\sum_{k = n}^{K-1}x(k)x^*(k-n) \qquad n = 0,1,...,K-1
\end{equation}
adding the adequate symmetries.

\subsection{AR model}
The last estimate we made relies on the design of an autoregressive model of order N for the signal of interest. We could assume the model of the signal to be an AR model with white noise as input, namely:
 \begin{equation}
 x(k) = -\sum_{n = 1}^{N}a_nx(k-n) + w(k)
 \end{equation}
where w(k) is white noise with variance $\sigma_w^2$. Given that, we can easily obtain an evaluation of the PSD as:
 \begin{equation}
P_x(z) = P_w(z)\frac{1}{A(z)A^*(\frac{1}{z^*})} \longrightarrow \mathcal{P}_x(f) = \frac{T_c\sigma_w^2}{|\mathcal{A}(f)|^2} 
\end{equation}
In Figure~\ref{fig:AR}, zeros and poles of the computed filter are shown. We've chosen $N=7$, in fact it performed a good trade off between complexity and accuracy of the estimate. A more detailed explanation for this choice is given in Probs.2 and 3.

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\linewidth]{img/zeroPoleAR}
	\caption{Zeros and poles of $1/A(z)$}
	\label{fig:AR}
\end{figure}

All different results for each method are shown in Figure~\ref{fig:PSD}.\\
We can see that Periodogram gives the best performance in terms of resolution of the estimate; in fact, there are seven principal PSD peaks that can be seen without further signal processing (this suggests the presence of complex tones in the signal); three peaks are much more visible than the the other ones, which are suggested by the combination of all methods of Problem 2. As expected the Welch method, implementing an average of subsequences, performs a less precise investigation of the peaks due to the shorter windows needed to extract the subsequences and, thus, the larger lobes; the decision between different values of $D$, $S$ and $N_s$ was based on this problem. Correlogram follows the same trend with the addition of noisy lobes next to the principal ones; this can cause a difficult interpretation if two peaks are too close. Finally, the need to choose a low order $N$ of the filter for the AR model in order to bound the complexity of the estimation, ended up with not so good results: it can be noticed that the model detects only one peak, which in this case is the average of the two highest neighbors peaks detected with the other methods, and it just follows the trend of the PSD without any other prominent peak.\\
The zoom on the three main peaks in Figure~\ref{fig:PSDzoom} highlights what stated so far in terms of resolution, in particular about the noisy lobes created by the Correlogram.\\
\begin{figure}[t]
	\centering
	\hfill
	\subfigure[Rectangular window]{\label{fig:PSD}\includegraphics[width=.45\linewidth]{img/PSDestimates}}
	\hfill
	\subfigure[Hamming window]{\label{fig:PSDhamming}\includegraphics[width=.45\linewidth]{img/PSDestimates_h}}
	\hfill
	\vspace{-3mm}
	\caption{Power Spectral Density estimation}
\end{figure}
\begin{figure}[t]
	\centering
	\hfill
	\subfigure[Rectangular window]{\label{fig:PSDzoom}\includegraphics[width=.45\linewidth]{img/PSDestimatesZOOM}}
	\hfill
	\subfigure[Hamming window]{\label{fig:PSDHammingZoom}\includegraphics[width=.45\linewidth]{img/PSDestimatesZOOM_h}}
	\hfill
	\vspace{-3mm}
	\caption{Power Spectral Density estimation - ZOOM}
\end{figure}
In order to study with more accuracy the problem, we set up an estimate using the Hamming window instead of the Rectangular one for Periodogram, Correlogram and Welch methods - Figure~\ref{fig:PSDhamming}. As we know from the theory, the Hamming window has a larger central lobe than the Rectangular one in frequency, but the secondary lobes decay much faster, resulting in a smoother spectrum. Thanks to the implementation of this window we've been able to confirm the third peak detected before and a few other; however, if we'd used just this setting, with Welch we wouldn't be able to distinguish the two closest spectral line as highlighted in Figure~\ref{fig:PSDHammingZoom}: in order to clearly detect both peaks we would need longer subsequences, thus decreasing their number and increasing the variance of the estimate.\\
Thanks to all these observations, we chose as a mathematical model for the signal the following:
\begin{equation} \label{eq:model}
	x(k) = w(k) + \sum_{i=1}^{N} A_i e^{j \varphi_i} \ e^{j2 \pi f_i k}
\end{equation}
where $A_i$, $f_i$ and $\varphi_i$ are respectively the amplitude, the frequency and the phase shift of the complex tones; w(k) is white noise: in fact in the plot of the autocorrelation of the signal in Figure~\ref{fig:ACX} we can clearly see a peak in zero which decays immediately after one lag.

\begin{table}[b]
	\centering
	\begin{tabular}{|c|ccccccc|}
		\hline 
		Frequency estimate &  &  &  &  &  & &  \\ 
		\hline 
		$f_0$ & 0.221 & 0.210 & 0.333 & 0.058 & 0.909 & 0.104 & 0.830\\ 
		\hline 
	\end{tabular} 
	\caption{Frequency of the sinusoidal components estimated looking at the PSD plots with many different parameters. The first three are quite sure, the last four less so}
\end{table}

\begin{figure}[t]
	\centering
	\includegraphics[width=.5\linewidth]{img/correlation}
	\caption{Autocorrelation of x(k)}
	\label{fig:ACX}
\end{figure}