classdef GeneralDFE < handle
    
    properties
        BMAP;
        g1;
        g2;
        c; % equalizer of length M1
        b; % decision feedback filter of length M2 (excluding g(1+0)=0) (row)
        
        psi;
        t_psi;
        h;
        n_h;
        Jmin;
        
        t0; % #samples
        D; % delay given y equalizer c
        Ds; % delay given by sampling in t0
        ch; % channel object
        F0; % sampling rate before equalizer is T/F0
        
        memory_ak; % memory of the last D+Ds symbols ak
        memory_rc; % memory of the last length(g1)-1 samples rc
        memory_xk; % memory of the last length(c)-1 samples xk
        memory_xMFk; % memory of the last length(c)-1 samples xFFk
        memory_akD_hat; % memory of the last M2 samples of hat(a_{k-D})
            % in reverse order (ready to be filtered by b with matrix
            % multiplication)
    end
    
    methods
        function obj = GeneralDFE(BMAP,ch)
            obj.BMAP = BMAP;
            obj.ch = ch;
        end
        
        function Jmin = train(obj,trainSeq,t0,M1,M2,D,g1Type,g2Type,F0)
            
            if ( nargin<7 )
                g1Type = 'mf';
            end
            if ( nargin<8 )
                g2Type = '1';
            end
            if (nargin<9)
                F0 = 1;
            end
            
            obj.reset();
            % utility
            qc = obj.ch.getQc();
            Q0 = obj.ch.qc.InterpolationFactor;
            sigmaw2 = obj.ch.sigmaw2;
            
            % memorize useful info
            obj.t0 = t0;
            obj.D = D; % delay from eq
            obj.Ds = floor(t0/Q0); % delay given by sampling in t0
            obj.F0 = F0;
            samplingFactor = Q0/obj.F0; % T/F0 / T/Q0
            
            Dtot = obj.D+obj.Ds;
            
            % set filters
            obj.setG1(g1Type);
            obj.setG2(g2Type);
            
            % compute h
            [obj.h,obj.n_h] = geth(qc,obj.g1,obj.t0,Q0,obj.F0,obj.g2);
            lh = sum(obj.n_h>=0); % only the "causal" part
                       
            % find equalizer/feedback
            [rw,n_rw] = getWtildeCorr(sigmaw2,Q0,obj.g1,obj.F0,obj.g2);
            [c_num,obj.b,obj.psi,obj.t_psi,Jmin] = computeEq_FB(M1,M2,obj.D,obj.BMAP,obj.h,obj.n_h,rw,n_rw,obj.F0);
            obj.Jmin = Jmin;
            obj.b = obj.b.'; % row for comodity
            
            if (length(c_num)<2) % The number of filter coefficients in the polyphase matrix must be at least two
                c_num = [c_num;0];
            end
            obj.c = dsp.FIRDecimator(obj.F0,c_num);
            
            
            % update memory of filters
            trainSeq = complex(shiftdim(trainSeq)); % complex column vector
            ak = [trainSeq;...
                trainSeq(1:lh+M1-1)]*obj.BMAP(1); % training sequqnce on BMAP

            rc = obj.ch.send(ak);
            [rr,obj.memory_rc] = filter(obj.g1,1,rc);
            xk = rr(1+obj.t0:samplingFactor:end); % shorter than ak due to t0
            [xMFk,obj.memory_xk] = filter(obj.g2,1,xk);
            [xFF,obj.memory_xMFk] = myDecFilter(obj.c,xMFk);
            
            % update object's memory
            obj.memory_akD_hat = ak(end-Dtot:-1:end-Dtot-M2+1);
            obj.memory_ak = zeros(Dtot,1);
            obj.updateMemoryAk(ak);
        end
        
        function [akD_hat,yk_normalized] = send(obj,ak,showConstellation)
            if (nargin<3)
                showConstellation = false;
            end
            
            rc = obj.ch.send(ak);
            [akD_hat,yk_normalized] = obj.receive(rc,showConstellation);
            
        end
        
        function [akD_hat,yk_normalized] = receive(obj,rc,showConstellation)
            % Assuming BMAP is QPSK
            % Note: the output is an estimate of  a_{k-D}
            
            if (nargin<3)
                showConstellation = false;
            end
            
            obj.checkIfTrained(); % otherwise error
            Q0 = obj.ch.qc.InterpolationFactor;
            samplingFactor = Q0/obj.F0; % assuming integer
            
            % if trained => t0 is already in steady state, don't have to
            % wait again t0, just smoothly continue the sampling
            % same for c intended as decimator
            t0mod = mod(obj.t0,samplingFactor);
            sampDelay = obj.getSampDelay();
            
            % use initial conditions, save final conditions
            [rr,obj.memory_rc] = filter(obj.g1,1,rc,obj.memory_rc);
            xk_prime = rr(1+t0mod:samplingFactor:end);
            
            [xk,obj.memory_xk] = filter(obj.g2,1,xk_prime,obj.memory_xk);
            
            [xFFk,obj.memory_xMFk] = myDecFilter(obj.c,xk,sampDelay,obj.memory_xMFk);
            
            % decision+feedback
            [akD_hat,yk_normalized] = obj.computeakD_hat(xFFk,showConstellation);
            
        end
        
        function reset(obj)
            % Resets memory, keeping all filter as they are
            obj.memory_ak = zeros(size(obj.memory_ak));
            obj.memory_rc = zeros(size(obj.memory_rc));
            obj.memory_xk = zeros(size(obj.memory_xk));
            obj.memory_xMFk = zeros(size(obj.memory_xMFk));
            obj.memory_akD_hat = zeros(size(obj.memory_akD_hat));
            obj.ch.reset();
        end
        
        function updateMemoryAk(obj,ak)
            Dtot = obj.D+obj.Ds;
            n = min(length(ak),Dtot);
            obj.memory_ak = circshift(obj.memory_ak,-n);
            obj.memory_ak(end-n+1:end) = ak(end-n+1:end);
        end
        
        %% Esthetics      
        function plotG1(obj,title, save)
            %PLOTG1 Plot |g_1|
            n = (0:length(obj.g1)-1)';
            Q0 = obj.ch.qc.InterpolationFactor;
            
            mystem(n,abs(obj.g1),title,...
                sprintf('$n \\frac{T}{%d}$',Q0),'$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/g1aafdfe2.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function [G1,f] = plotG1Freq(obj,NFFT,tit,save)
            %PLOTG1FREQ Plot |G_1(f)|
            Q0 = obj.ch.qc.InterpolationFactor;
            T = obj.ch.T;
            
            G1 = fftshift( fft(obj.g1,NFFT)); % for f in (-1/T,1/T]
            f = (-NFFT/2+1:NFFT/2)/NFFT*Q0/T;
            
            figure
            plot(f, 10*log10(abs(G1)), 'k','Linewidth',1); grid on
            title( tit,'interpreter','latex')
            xlabel('$f$ [Hz]','interpreter','latex')
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            ylim([-10 5])
           if (save)
                matlab2tikz('../Latex/img/GAA2.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function [G2,f] = plotG2Freq(obj,NFFT,tit,save)
            %PLOTG1FREQ Plot |G_1(f)|
            T = obj.ch.T;
            
            G2 = fftshift( fft(obj.g2,NFFT)); % for f in (-1/T,1/T]
            f = (-NFFT/2+1:NFFT/2)/NFFT*obj.F0/T;
            
            figure
            plot(f, 10*log10(abs(G2)), 'k','Linewidth',1); grid on
            title( tit,'interpreter','latex')
            xlabel('$f$ [Hz]','interpreter','latex')
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            ylim([-15 5])
            if (save)
                matlab2tikz('../Latex/img/GM2.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function plotG2(obj,title,save)
            %PLOTG2 Plot |g_m|
            n = (0:length(obj.g2)-1)';
            
            mystem(n,abs(obj.g2),title,...
                sprintf('$n \\frac{T}{%d}$',obj.F0),'$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/gmaafmfdfe.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function plotC(obj,save)
           %PLOTC Plot |c|
            n = (0:length(obj.c.Numerator)-1)';
            n_high = n( mod(n,obj.F0)==0 );
            if (obj.F0 ==1)
                tit = '$|c|@T$';
                xlab = '$nT$';
            else
                tit = sprintf('$|c|@ \\frac{T}{%d}$',obj.F0);
                xlab = sprintf('$n \\frac{T}{%d}$',obj.F0);
            end
            
            mystem(n,abs(obj.c.Numerator), tit,...
                xlab,'$|\cdot|$','HighlightInteger',obj.F0)
            if (save)
                matlab2tikz('../Latex/img/caafdfe.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n_high),','...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function plotB(obj,save)
            %PLOTB Plot |b|
            n = (1:length(obj.b))';
            
            mystem(n,abs(obj.b), '$|b|$','$nT$','$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/baafdfe.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function plotPSI(obj,save)
            %PLOTPSI Plot |\psi|
            n_psi = obj.t_psi*obj.F0;
            n_high = n_psi( mod(n_psi,obj.F0)==0 );
            if (obj.F0 ==1)
                tit = '$|\psi_n|@T$';
                xlab = '$nT$';
            else
                tit = sprintf('$|\\psi_n|@ \\frac{T}{%d}$',obj.F0);
                xlab = sprintf('$n \\frac{T}{%d}$',obj.F0);
            end
            
            mystem(n_psi,abs(obj.psi),tit,...
                xlab,'$|\cdot|$','HighlightInteger',obj.F0)
            if (save)
                matlab2tikz('../Latex/img/PSIaafdfe.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n_high),','...
                    'ylabel style={font=\Large}']);
            end
        end
    end
    
    methods(Access=private)
        function [akD_hat,yk_normalized] = computeakD_hat(obj,xFF,showConstellation)
            
            if (nargin<3)
                showConstellation = false;
            end
            
            % init
            akD_hat = zeros(length(xFF),1);
            psiD = obj.psi( obj.t_psi==obj.D );
            
            yk_normalized = zeros(length(akD_hat),1);
            for k = 1:length(akD_hat)
                xFBk = obj.b*obj.memory_akD_hat; % filter b (row)
                yk = xFF(k)+xFBk;
                yk_normalized(k) = yk/psiD;
                
                % decision
                signI = sign(real(obj.BMAP))==sign( real(yk_normalized(k)) );
                signQ = sign(imag(obj.BMAP))==sign( imag(yk_normalized(k)) );
                akD_hat(k) = obj.BMAP(signI & signQ);
                
                % update memory
                obj.memory_akD_hat = circshift(obj.memory_akD_hat,1);
                obj.memory_akD_hat(1) = akD_hat(k);
            end
            
            if (showConstellation)
                plotConstellation(yk_normalized,obj.BMAP);
            end
        end
        
        function setG1(obj,type)
            
            if( strcmp(type,'mf') )
                
                qc1 = obj.ch.qc.Numerator; % without delay
                qc1 = shiftdim(qc1); % column
                obj.g1 = conj(flipud(qc1)); % gm(n)=qc*(n0-n), n0 is implicit
                
            elseif( strcmp(type,'aaf') )
                
                % utilities
                T = obj.ch.T;
                Q0 = obj.ch.qc.InterpolationFactor;
                
                % calculate AA parameters
                rp = 1; % 1dB passband ripple
                deltap = (10^(rp/20)-1)/(10^(rp/20)+1); % passband ripple
                deltas = sqrt( 0.1/(Q0-1) ); % stopband ripple
                freqs = [1/(2*T) 3/(4*T)]; % pass,stop frequencies
                a = [1 0]; % ideal amplitudes
                dev = [deltap deltas];
                
                % design
                [n,f0,a0,w] = firpmord(freqs, a, dev, 2/T);
                if ( mod(n,2)== 1) % force odd length (even order)
                    n = n+1;
                end
                obj.g1 = firpm(n,f0,a0,w);
                
            else
                error('Type of filter g1 not recognized. It should be either mf or aaf');
            end
            
        end
        
        function setG2(obj,type)
            
            if( strcmp(type,'1') ) % delta response
                obj.g2 = 1;
            elseif( strcmp(type,'mf') )
                qc = obj.ch.getQc();
                Q0 = obj.ch.qc.InterpolationFactor;
                
                h_pre_g2 = geth(qc,obj.g1,obj.t0,Q0,obj.F0);
                h_pre_g2 = shiftdim(h_pre_g2);
                % create match
                obj.g2 = conj(flipud(h_pre_g2)); % gm(n)=h*(n0-n), n0 is implicit
                
            else
                error('Type of filter g2 not recognized. It should be either mf or 1');
            end
            
        end
        
        function checkIfTrained(obj)
            if ( isempty(obj.g1) || isempty(obj.c) ||...
                    isempty(obj.t0) || isempty(obj.D) ||...
                    isempty(obj.Ds) || isempty(obj.ch) ||...
                    isempty(obj.g2))
                error('You need to train the receiver first')
            end
        end
        
        function sampDelay = getSampDelay(obj)
            
            Q0 = obj.ch.qc.InterpolationFactor;
            samplingFactor = Q0/obj.F0;
            
            t0mod = mod(obj.t0,Q0); % t0 restricted to 1 period
            TQsamples = Q0-t0mod; % number of valid samples@TQ (before sampler)
            TFsamples = ceil( TQsamples/samplingFactor ); % valid samples@TF (after sampler)
            sampDelay = obj.F0-TFsamples;
            
        end
                
    end
    
end