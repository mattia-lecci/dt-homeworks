%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Needs path ../../HW2/Matlab %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef MF_LE < handle
    properties
        BMAP;
        gm;
        c; % equalizer of length M1
        
        psi;
        t_psi;
        h;
        n_h;
        
        t0; % #samples
        D; % delay given by equalizer c
        Ds; % delay given by sampling in t0
        ch; % channel object
        
        memory_ak; % memory of the last D+Ds symbols ak
        memory_rc; % memory of the last length(gm)-1 samples rc
        memory_xk; % memory of the last length(c)-1 samples xk
    end
    
    methods
        function obj = MF_LE(BMAP,ch)
            %MF_LE Class constructor
            obj.BMAP = BMAP;
            obj.ch = ch;
        end
        
        function Jmin = train(obj,trainSeq,t0,M1,D)
            obj.reset();
            % utility
            qc = obj.ch.getQc();
            Q0 = obj.ch.qc.InterpolationFactor;
            sigmaw2 = obj.ch.sigmaw2;
            
            % memorize useful info
            obj.setGm(); % channel assumed known
            obj.t0 = t0;
            obj.D = D; % delay from eq
            obj.Ds = floor(t0/Q0); % delay given by sampling in t0
            
            Dtot = obj.D+obj.Ds;
            
            % compute h
            [obj.h,obj.n_h] = geth(qc,obj.gm,obj.t0,Q0);
            lh = sum(obj.n_h>=0); % only the "causal" part

            % find equalizer
            [rw,n_rw] = getWtildeCorr(sigmaw2,Q0,obj.gm);
            [obj.c,~,obj.psi,obj.t_psi,Jmin] = computeEq_FB(M1,0,obj.D,obj.BMAP,obj.h,obj.n_h,rw,n_rw);
            
            % update memory
            trainSeq = complex(shiftdim(trainSeq)); % complex column vector
            ak = [trainSeq;...
                trainSeq(1:lh+M1-1)]*obj.BMAP(1); % training sequence on BMAP
            
            % just before equalizer
            rc = obj.ch.send(ak);
            [rr,obj.memory_rc] = filter(obj.gm,1,rc);
            xk = rr(1+obj.t0:Q0:end); % shorter than ak due to t0
            [yk,obj.memory_xk] = filter(obj.c,1,xk);
            obj.memory_ak = zeros(Dtot,1); % initialize length of memory_ak needed
            obj.updateMemoryAk(ak);
        end
        
        function akD_hat = receive(obj,rc,showConstellation)
            % Assuming BMAP is QPSK
            % Note: the output is an estimate of  a_{k-D}
            
            obj.checkIfTrained(); % otherwise error
            Q0 = obj.ch.qc.InterpolationFactor;
            psiD = obj.psi( obj.t_psi==obj.D );
            
            % if trained => t0 is already in steady state, don't have to
            % wait again t0, just smoothly continue the sampling
            t0mod = mod(obj.t0,Q0);
            
            % use initial conditions, save final conditions
            [rr,obj.memory_rc] = filter(obj.gm,1,rc,obj.memory_rc);
            xk = rr(1+t0mod:Q0:end);
            [yk,obj.memory_xk] = filter(obj.c,1,xk,obj.memory_xk);
            yk = yk/psiD; % normalize wrt psi(D)
            
            % decision
            akD_hat = zeros(size(yk));
            for i = 1:length(yk)
                signI = sign(real(obj.BMAP))==sign(real(yk(i)));
                signQ = sign(imag(obj.BMAP))==sign(imag(yk(i)));
                akD_hat(i) = obj.BMAP(signI & signQ);
            end
            
            % plot
            if (showConstellation)
                plotConstellation(yk,obj.BMAP);
            end
        end
        
        function reset(obj)
            % Resets memory, keeping all filter as they are
            obj.memory_ak = zeros(size(obj.memory_ak));
            obj.memory_rc = zeros(size(obj.memory_rc));
            obj.memory_xk = zeros(size(obj.memory_xk));
            obj.ch.reset();
        end
        
        function updateMemoryAk(obj,ak)
            Dtot = obj.D+obj.Ds;
            n = min(length(ak),Dtot);
            obj.memory_ak = circshift(obj.memory_ak,-n);
            obj.memory_ak(end-n+1:end) = ak(end-n+1:end);
        end
        
        %% Esthetics
        
        function plotGm(obj, save)
            %PLOTGM Plot |g_m|
            n = (0:length(obj.gm)-1)';
            Q0 = obj.ch.qc.InterpolationFactor;
            
            mystem(n,abs(obj.gm),sprintf('$|g_M(n \\frac{T}{%d})|$',Q0),...
                sprintf('$n \\frac{T}{%d}$',Q0),'$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/Gmmfle.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
        end
        function plotC(obj, save)
            %PLOTC Plot |c|
            n = (0:length(obj.c)-1)';
            
            mystem(n,abs(obj.c),'$|c|$','$nT$','$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/cmfle.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
        end
        function plotPSI(obj, save)
            %PLOTPSI Plot |\psi|
            mystem(obj.t_psi,abs(obj.psi),'$|\psi_n|$','$nT$','$|\cdot|$')
            if (save)
                matlab2tikz('../Latex/img/PSImfle.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(obj.t_psi),','...
                    'ylabel style={font=\Large}']);
            end
        end
    end
    
    methods(Access=private)
        function setGm(obj)
            % matched filter based on channel
            qc1 = obj.ch.qc.Numerator; % without delay
            qc1 = shiftdim(qc1); % column
            obj.gm = conj(flipud(qc1)); % gm(n)=qc*(n0-n), n0 is implicit
        end
        
        function checkIfTrained(obj)
            if ( isempty(obj.gm) || isempty(obj.c) ||...
                    isempty(obj.t0) || isempty(obj.D) ||...
                    isempty(obj.Ds) || isempty(obj.ch) )
                error('You need to train the receiver first')
            end
        end

    end
end