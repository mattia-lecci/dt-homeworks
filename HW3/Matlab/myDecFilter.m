function [y,memory] = myDecFilter(filter,x,sampDelay,initCond)

% arg check
narginchk(2,4);

if (nargin<3)
    sampDelay = 0;
end
if (nargin<4)
    initCond = [];
end

M = filter.DecimationFactor;
N = length(filter.Numerator);
if ( sampDelay>=M )
    error('Incorrect sampDelay')
end
if ( ~isempty(initCond) && length(initCond)~=N-1 )
    error('Wrong initCond length')
end

% init
if ( isempty(initCond) )
    initCond = zeros(N-1,1);
end

% adjusting input
adjustIn = mod( M-mod(N-1,M),M ); % front zeros needed in order to correctly sample
x = shiftdim(x); % column
x = [zeros(adjustIn,1); initCond; x]; % adding initCond and zeros
x(1:sampDelay) = []; % remove for sampling delay
adjustFin = mod( M-mod( length(x),M),M ); % last zeros in order to make length of signal as multiple of M
x = [x; zeros(adjustFin,1)];

y = filter.step(x);
elim = ( adjustIn+length(initCond) )/M; % samples to eliminate
y(1:elim) = []; % eliminate transient

% final conditions
memory = x(end-(N-1)-adjustFin+1:end-adjustFin);

end