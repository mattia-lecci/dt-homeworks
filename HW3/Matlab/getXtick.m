function str = getXtick(n)

str = ['xtick={',num2str( n(1) )];
for i = 2:length(n)
    str = [str, ',', num2str( n(i) )];
end
str = [str, '}'];

end