function [Pe,Niter,errors] = estimateErrorRate(obj,minIter,maxIter,minErr,showConstellation)

if (nargin<5)
    showConstellation = false;
end
if (minIter>maxIter)
    error('minIter>maxIter')
end

% Assuming already trained
% computing Pe: init
Dtot = obj.D+obj.Ds;
Nsymb = minIter+Dtot;
Niter = 0;
errors = 0;

while ( Niter<=maxIter )
    ak = createRandomSymbols(obj.BMAP,Nsymb);
    rc = obj.ch.send(ak);
    
    % different receive methods based on class
    switch class(obj)
        case 'AWGN'
            akD_hat = obj.receive(rc);
        case 'MF_LE'
            akD_hat = obj.receive(rc,showConstellation); showConstellation = false; % show only once
        case 'GeneralDFE'
            akD_hat = obj.receive(rc,showConstellation); showConstellation = false; % show only once
        case 'VA'
            akD_hat = obj.receive(rc);
        case 'FBA_MaxLogMap'
            akD_hat = obj.receive(rc,[]);
        otherwise
            error('Class not recognized')
    end
    
    % useful symbols/estimates
    if ( Niter==0 )
        useful_ak = ak(1:end-Dtot);
        useful_akD_hat = akD_hat(Dtot+1:end);
        
        Niter = length(useful_ak);
    else
        useful_ak = [obj.memory_ak;...
            ak(1:end-Dtot)];
        useful_akD_hat = akD_hat;
        
        Niter = Niter+Nsymb;
    end
    
% [corak,lagak] = mycorrelation(ak,akD_hat,10);
% [coraku,lagaku] = mycorrelation(useful_ak,useful_akD_hat,10);
% figure; subplot(2,1,1)
% stem(lagak,abs(corak)); title('corr ak')
% subplot(2,1,2)
% stem(lagaku,abs(coraku)); title('corr useful ak')
    
    obj.updateMemoryAk(ak);
    errors = errors+sum( abs(useful_ak-useful_akD_hat)>1e-5 ); % to avoid numerical errors
    
    if ( errors>=minErr || Niter>=maxIter )
        break;
    else
        % don't exceed maxIter symbols to detect
        maxSymb = maxIter-Niter;
        Nsymb = min(2*Nsymb,maxSymb);
    end
end

Pe = errors/Niter;

end