clear variables
close all
clc

% param
M = 16; % choose any integer power of 2
bpS = log2(M);
showtext = true; % show symbol/codeword

% data
n = 0:M-1;
bl = de2bi(n);
bl = bl';
bl = bl(:)';

%% PAM

map = createBMAP('pam', M);
ak = encodeBits(bl, map);

% plot
xlimit = [min(map) max(map)] + [-1 1];
ylimit = [-1 1];

figure
plot([0 0], ylimit, 'k'); hold on; grid on
scatter(map, zeros(size(map)),'k','filled')
title( sprintf('%d-PAM',M) )
xlim(xlimit)
ylim(ylimit)
ax = gca;
ax.YTick = 0;
ax.XTick = sort(map);

if ( showtext )
    for i = 1:length(ak)
        
        x = ak(i);
        y = 0;
        b_str = dec2bin(n(i), bpS);
        a_str = num2str(ak(i));
        
        text(x,y+0.1,b_str, 'HorizontalAlignment', 'center');
        text(x,y-0.1,a_str, 'HorizontalAlignment', 'center');
        
    end
end

%% QAM

if ( mod(sqrt(M),1)<1e-6 ) % only if perfect square
    
    map = createBMAP('qam', M);
    ak = encodeBits(bl, map);
    
    % plot
    xlimit = [min(real(map)) max(real(map))] + [-1 1];
    ylimit = [min(imag(map)) max(imag(map))] + [-1 1];
    
    figure
    plot(xlimit, [0 0], 'k'); hold on; grid on
    plot([0 0], ylimit, 'k')
    scatter(real(map), imag(map),'k','filled')
    title( sprintf('%d-QAM',M) )
    xlim(xlimit)
    ylim(ylimit)
    daspect([1 1 1])
    ax = gca;
    ax.XTick = sort(unique(real(map)));
    ax.YTick = sort(unique(imag(map)));
    
    if ( showtext )
        for i = 1:M
            
            x = real(ak(i));
            y = imag(ak(i));
            b_str = dec2bin(n(i), bpS);
            a_str = num2str(ak(i));
            
            text(x,y+0.3,b_str, 'HorizontalAlignment', 'center');
            text(x,y-0.3,a_str, 'HorizontalAlignment', 'center');
            
        end
    end
end

%% PSK

map = createBMAP('psk', M);
ak = encodeBits(bl, map);

% plot
xlimit = 2*[-1 1];
ylimit = 2*[-1 1];
t = 0:0.1:2*pi+0.1;
circ = sqrt(2)*exp(1j*t);

figure
plot(xlimit, [0 0], 'k'); hold on; grid on
plot([0 0], ylimit, 'k')
plot(real(circ),imag(circ), 'k:')
scatter(real(map), imag(map),'k','filled')
title( sprintf('%d-PSK',M) )
xlim(xlimit)
ylim(ylimit)
daspect([1 1 1])

if ( showtext )
    for i = 1:M
        
        x = real(ak(i));
        y = imag(ak(i));
        b_str = dec2bin(n(i), bpS);
        a_str = num2str(ak(i),2);
        
        text(x,y+0.1,b_str, 'HorizontalAlignment', 'center');
        text(x,y-0.1,a_str, 'HorizontalAlignment', 'center');
        
    end
end