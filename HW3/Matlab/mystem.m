function mystem(x,y,mytitle,myxlabel,myylabel,highlight,F0)
% Just a useful utility

if (nargin<6)
    highlight = 'HighlightInteger';
end
if (nargin<7)
    F0 = 1;
end

if ( strcmp(highlight,'HighlightInteger') )
    intInd = mod(x,F0)==0;
    xHighlight = x(intInd);
    yHighlight = y(intInd);
else % highlight everything
    xHighlight = x;
    yHighlight = y;
end

figure
stem(x,y,'k','LineWidth',1); grid on; hold on
stem(xHighlight,yHighlight,'filled','k', 'LineWidth',1.5)
title(sprintf('%s',mytitle),'interpreter','latex')
xlabel(sprintf('%s',myxlabel),'interpreter','latex')
ylabel(sprintf('%s',myylabel),'interpreter','latex')

end