function [r_wtilde,n_rwtilde] = getWtildeCorr(sigmaw2,Q0,g1,F0,g2)

if (nargin<4)
    F0 = 1;
end
if (nargin<5)
    g2 = 1;
end

% utilities
samplingFactor = Q0/F0;
[r_g1,n_rg1] = xcorr(g1);
[r_g2,n_rg2] = xcorr(g2);

% noise corr after g1@TQ
r_wr = sigmaw2*r_g1;
n_rwr = n_rg1;

% noise corr after sampler@TF
ind = mod(n_rwr,samplingFactor)==0;
r_wprime = r_wr(ind);
n_rwprime = n_rwr(ind)/samplingFactor;

% noise corr after g2@TF
r_wtilde = conv(r_wprime,r_g2);
n_rwtilde = (n_rg2(1)+n_rwprime(1)):(n_rg2(end)+n_rwprime(end));

end