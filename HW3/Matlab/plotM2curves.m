function plotM2curves(Dlist,gamma,M2list,delta)

Dlist = shiftdim(Dlist);
repD = repmat(Dlist,size(gamma,2),1);

ind = findEquivalent(gamma,delta);
colors = jet(max(ind));

figure; hold on; grid on
for i = 1:size(gamma,3)
    gammaM2 = gamma(:,:,i);
    plot(repD,10*log10(gammaM2(:)),'color',colors(ind(i),:),'LineWidth',1)
end

legend( strcat({'M2='},num2str(M2list')) );
xlabel('D')
ylabel('\gamma_{min,dB}')
title('\gamma_{min}')

end

function ind = findEquivalent(gamma,delta)

ind = zeros( size(gamma,3),1 );
ind(1) = 1;

for i = 2:size(gamma,3)
    maxDiff = max(max( gamma(:,:,i)-gamma(:,:,i-1)));
    
    if (maxDiff<=delta)
        ind(i) = ind(i-1);
    else
        ind(i) = ind(i-1)+1;
    end
end

end