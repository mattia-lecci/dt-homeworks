function [h,n_h] = geth(qc,g1,t0,Q0,F0,g2)

if (nargin<5)
    F0 = 1;
end
if (nargin<6)
    g2 = 1;
end

% init
samplingFactor = Q0/F0;
t0mod = mod(t0,samplingFactor);

% after g1
h1 = conv(qc,g1);

% downsample
h2 = h1(1+t0mod:samplingFactor:end);

% timing
n0 = (t0-t0mod)/samplingFactor;
n2 = (0:length(h2)-1)-n0;

% after g2
h3 = conv(h2, g2);
n3 = n2(1):( n2(end)+length(g2)-1 );

% trim
ind0 = find(h3,1); % first non-zero
indLast = find(h3,1,'last'); % last non-zero
h = h3(ind0:indLast);
n_h = n3(ind0:indLast).'; % column

end