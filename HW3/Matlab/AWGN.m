classdef AWGN < handle
    
    properties
        BMAP;
        ch; % Channel object
        
        memory_ak=[];
        D=0;
        Ds=0;
    end
    
    methods
        function obj = AWGN(BMAP,T)
            obj.BMAP = BMAP;
            
            % default channel (delta response)
            obj.ch = Channel(1,1,1,T);
        end
        
        function ak_hat = receive(obj,rc)
           %RECEIVE Decode ak_hat from r_c assuming BMAP is a QPSK
           
           % note: only decision since rc==yk
           ak_hat = zeros(size(rc));
           for i = 1:length(rc)
               signI = sign(real(obj.BMAP))==sign(real(rc(i)));
               signQ = sign(imag(obj.BMAP))==sign(imag(rc(i)));
               ak_hat(i) = obj.BMAP(signI & signQ);
           end
        end
        
        function updateMemoryAk(obj,ak)
            % do nothing, just for homegeneity
        end

    end
    
end