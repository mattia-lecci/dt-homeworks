function [corr,lags] = fractionalCorrelation(x,p,Q0,mMax)

% init
corr = zeros(mMax+1,1);
lags = (0:mMax)';

% utility
L = length(p);
downIndex = (0:L-1)'*Q0 + 1; % downsampled indexes starting from m=0

for m = lags
    corr(m+1) = 1/L*p'*x(downIndex);
    downIndex = downIndex+1; % advance by 1 with m
end

end