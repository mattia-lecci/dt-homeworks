function ak = createRandomSymbols(BMAP,Nsymb)
%CREATERABDINSYMBOLS
%
% INPUTS
% * BMAP: a valid BMAP. See createBMAP.m
% * Nsymb: number of symbols to create
%
% OUTPUTS
% * ak: column vector of random symbols

% check arg
narginchk(2,2)

if (Nsymb<0)
    error('Nsymb<0')
end

% create ak
M = length(BMAP);
ind = randi([1 M], Nsymb,1);
ak = BMAP(ind);

ak = shiftdim(ak); % column

end