function [copt,bopt,psi,t_psi,Jmin] = computeEq_FB(M1,M2,D,BMAP,h,n_h,rw,n_rw,F0)
% INPUTS:
% * n_h: vector containing indexes, not times. Intended as h( n_h*T/F0 )

if (nargin<9)
    F0 = 1;
end

BMAP = shiftdim(BMAP);
h = shiftdim(h);
n_h = shiftdim(n_h);
rw = shiftdim(rw);
n_rw = shiftdim(n_rw);

% utilities
[rw,n_rw] = extendRw(rw,n_rw,M1); % extend with 0 to avoid out of bound index
rwind0 = find( n_rw==0 ); % find the index of r_w(0)

sigmaa2 = var(BMAP);
[h,n_h] = extendH(h,n_h,M1,M2,D,F0);
indF0_D = find( n_h==F0*D ); % index of F0*D

% p column vector
p = 0:M1-1; % indexes of vector p
p_ = sigmaa2*conj( h(indF0_D-p) ); % vector p

% R matrix
sum1mat = zeros(M1);
sum2mat = sum1mat;
rwMat = sum1mat;
for p = 0:M1-1
    for q = 0:M1-1
        
        sum1mat(1+p,1+q) = getSum1(h,n_h,p,q,F0);
        sum2mat(1+p,1+q) = getSum2(h,n_h,D,M2,p,q,F0);
        rwMat(1+p,1+q) = rw(rwind0+p-q);
        
    end
end
R = sigmaa2*(sum1mat-sum2mat) + rwMat;

copt = R\p_; % better than inv(R)*p_
[psi,t_psi] = computePsi(h,n_h,copt,F0);
bopt = computeBopt(psi,t_psi,D,M2);

% Jmin
Jmin = abs(sigmaa2 - p_'*copt); % to ensure real quantity

end


% utilities
function [rw,lag] = extendRw(rw,lag,M1)

% requirements
minInd = min( [lag(1) -(M1-1)] );
maxInd = max( [lag(end) M1-1] );

% zeros needed
pre0 = lag(1)-minInd;
post0 = maxInd-lag(end);

% extensions
rw = shiftdim(rw);
rw = [zeros(pre0,1); rw; zeros(post0,1)];

lag = shiftdim(lag);
lag = [(lag(1)-pre0:lag(1)-1)'; lag; (lag(end)+1:lag(end)+post0)'];

end

function [h,n_h] = extendH(h,n_h,M1,M2,D,F0)

N1 = n_h(1);
N2 = n_h(end);

% requirements
minInd = min( [N1, F0*D-(M1-1), D, -(M1-1), F0*(1+D)-(M1-1)] );
maxInd = max( [N2, F0*D, N2+M1-1, F0*(M2+D)] );

% zeros needed
pre0 = N1-minInd;
post0 = maxInd-N2;

% extensions
h = shiftdim(h); % column
h = [zeros(pre0,1); h; zeros(post0,1)];

n_h = shiftdim(n_h);
n_h = [(N1-pre0:N1-1)'; n_h; (N2+1:N2+post0)'];

end

function sum1 = getSum1(h,n_h,p,q,F0)
% considering sum from +N1 to N2 (different from book)
indN1 = find(h,1);
ind0 = find( n_h==0 );
indN2 = find(h,1,'last');

% calculation
F0n_pos = ind0:F0:( indN2+max([p,q]) );
F0n_neg = fliplr( (ind0-F0):-F0:indN1 );
F0n = [F0n_neg, F0n_pos];
sum1 = h(F0n-p)'*h(F0n-q);

end

function sum2 = getSum2(h,n_h,D,M2,p,q,F0)
% considering sum from +N1 to N2 (different from book)
ind1 = find( n_h==F0*(1+D) );
indM2 = find( n_h==F0*(M2+D) );

F0jD = ind1:F0:indM2;
sum2 = h( F0jD-p )'*h( F0jD-q );
% sum2 = 0;
% for j = ind1:indM2
%     sum2 = sum2 + h(j+D-q)*conj( h(j+D-p) );
% end

end

function bopt = computeBopt(psi,t_psi,D,M2)
% Note: delay is not included ( bopt(1)=psi(D+1) )

% utilities
afterDind = t_psi>D;
integerInd = mod(t_psi,1)==0;

validEntries = afterDind & integerInd;
nEntries = sum(validEntries);
nEntries = min([nEntries, M2]);

% extract b (only the part contained in psi)
if (nEntries>0)
    bopt = -psi( find(validEntries,nEntries) );
else
    bopt = [];
end

padLength = M2-length(bopt);
bopt = [bopt; zeros(padLength,1)];

end

function [psi,t_psi] = computePsi(h,n_h,c,F0)

psi = conv(h,c);
n_psi = (0:length(psi)-1)'+n_h(1);

% trim@TF
firstInd = find(psi,1); % first non-zero element of psi
lastInd = find(psi,1,'last');

psi = psi(firstInd:lastInd);
n_psi = n_psi(firstInd:lastInd); % normalized @T

% make it start at least from 0@TF
if (isempty(psi)) % to avoid outputting an empty vector
    psi = conv(h,c);
    n_psi = (0:length(psi)-1)'+n_h(1);
elseif ( n_psi(1)>0 )
    psi = [zeros(n_psi(1),1); psi];
    n_psi = [( 0:(n_psi(1)-1) )'; n_psi];
end

% normalize @T
t_psi = n_psi/F0;

end