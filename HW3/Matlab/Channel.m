%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Needs path ../../HW2/Matlab %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

classdef Channel < handle
    properties
        qc; % h_tx*g_c (FIR filter)
        delay; % channel delay
        sigmaw2; % (complex) noise power
        T; % sampling period Q0*TQ
        memory; % initial conditions of the channel
    end
    methods
        function obj = Channel(qc,Q0,sigmaw2,T)
            %CHANNEL Constructor
            [qc1,D] = Channel.trimFilter(qc);
            
            obj.qc = dsp.FIRInterpolator(Q0,qc1);
            obj.delay = D;
            obj.sigmaw2 = sigmaw2;
            obj.T = T;
            
            obj.memory = zeros(length(qc)-1,1);
        end
        
        function rc = send(obj,ak)
            M = length(obj.memory);
            A = length(ak);
            Q0 = obj.qc.InterpolationFactor;
            D = obj.delay;
            
            ak = shiftdim(ak);
                        
            sc = obj.qc.step([obj.memory; ak]);
            
            % adjusting sc
            sc = [zeros(D,1);...
                sc]; % adding delay
            sc = sc(Q0*M+1:end-D); % ignore memory transient and last D samples
            
            % adding noise
            w = crandn(length(sc),1,0,obj.sigmaw2);
            rc = sc + w;
            
            % update memory
            n = min(A,M);
            obj.memory = circshift(obj.memory,-n); % shift memory towards past
            obj.memory(end-n+1:end) = ak(end-n+1:end); % add new symbols
        end
        
        function qc = getQc(obj)
            % Returns the full impulse response (including delay)
            qc = [zeros(obj.delay,1);...
                obj.qc.Numerator(:)];
        end
        
        function reset(obj)
            %RESET Reset channel memory to zero
            obj.memory = zeros(size(obj.memory));
        end
        
        function qc = plotImpResp(obj,save)
            %PLOTImpResp Plot |q_c|
            qc = obj.getQc();
            Q0 = obj.qc.InterpolationFactor;
            
            n = (0:length(qc)-1)';
            mystem(n,abs(qc),sprintf('$|q_c(m \\frac{T}{%d})|$',Q0),...
                sprintf('$m \\frac{T}{%d}$',Q0),...
                '$|q_c|$');
            
            if (save)
                matlab2tikz('../Latex/img/qc.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
        end
        
        function [Qc,f] = plotFreqResp(obj,NFFT,save)
            qcdel = obj.getQc();
            Q0 = obj.qc.InterpolationFactor;
            
            Qc = fftshift( fft(qcdel,NFFT)); % for f in (-1/T,1/T]
            f = (-NFFT/2+1:NFFT/2)/NFFT*Q0/obj.T;
            
            figure
            plot(f, 10*log10(abs(Qc)), 'k', 'LineWidth',1); grid on
            title('$|Q_c(f)|$','interpreter','latex')
            xlabel('$f$ [Hz]','interpreter','latex')
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            ylim([-10 5])
            if (save)
                matlab2tikz('../Latex/img/Qf.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        %% check attributes
        function set.qc(obj,newqc)
            % to avoid problems with channel memory
            if ( isempty(obj.qc) )
                obj.qc = newqc;
            else
                error('Cannot modify channel response')
            end
        end
    end
    
    methods(Static, Access=private)
        function [qc1,D] = trimFilter(qc)
        % Eliminates leading/trailing zeros from filter qc
        ind0 = find(qc);
        D = ind0(1)-1;
        qc1 = qc(ind0(1):ind0(end));
        end
    end
end