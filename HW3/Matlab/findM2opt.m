clear variables
close all
clc

addpath('../../HW1/Matlab');
addpath('../../HW2/Matlab');

% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);

% parameters
M = 4;
T = 1;
Q0 = 2; % upsample ratio of the channel
N = length(qc); % channel length
pnBits = 6;

M1min = 2;
M1max = 8; M1tot = M1max-M1min+1;
Dmin = 0;
M2tot = 1+2*2;

% utility
BMAP = createBMAP('psk',M);
Ma = BMAP'*BMAP/length(BMAP);
Eqc = qc'*qc;

% preparing channel
sigmawc2 = computeSigmaw2(10,BMAP,Eqc); % Gamma=10dB
ch = Channel(qc,Q0,sigmawc2,T);

%% 
% utilities
pn = pnseq(ones(pnBits,1),'+-1');
leg = strcat({'M1='},num2str((M1min:M1max)'));
leg2 = strcat({'M1='},num2str((M1min:2*M1max)'));

% AAF+DFE@T
if (true)
    rx = GeneralDFE(BMAP,ch);
    
    t0 = 5;
    
    Dmax = M1max+4;
    Dlist = Dmin:Dmax;
    M1list = M1min:M1max;
    M2list = (5+M1min-1-Dmax):5;
    M2list = M2list( M2list>=0 );
    M2tot = length(M2list);
    
    col = M1max-M1min+1; % M1
    row = (M1max+3)-Dmin+1;% D
    gamma = zeros(row,col,M2tot);
    
    for iM1 = 1:length(M1list)
        for iD = 1:length(Dlist)
            for iM2 = 1:length(M2list)
                M1 = M1list(iM1);
                M2 = M2list(iM2);
                D = Dlist(iD);
                
                Jmin = rx.train(pn,t0,M1,M2,D,'aaf','1');
                
                psiD = rx.psi( rx.t_psi==D );
                gamma(iD,iM1,iM2) = 2*abs(psiD)^2/( Jmin-abs(1-psiD)^2*Ma );
            end
        end
    end
        
    % plot
    plotM2curves(Dlist,gamma,M2list,0.1);
    ylim([5 10])
end