function plotConstellation(yk,BMAP,alpha)

if (nargin<3)
    alpha = 0.1;
end

re = real(yk);
im = imag(yk);

figure
% axis
minx = min(real(BMAP))-2;
miny = min(imag(BMAP))-2;
maxx = max(real(BMAP))+2;
maxy = max(imag(BMAP))+2;
plot([minx maxx],[0 0],'k'); hold on
plot([0 0],[miny maxy],'k')
scatter(re,im,'kx','MarkerFaceAlpha',alpha,'MarkerEdgeAlpha',alpha)
plot(BMAP,'w*')

title('Constellation','interpreter','latex','FontSize',15)
xlabel('Re','interpreter','latex')
ylabel('Im','interpreter','latex')
axis([minx maxx miny maxy])
daspect([1 1 1])

% hist3
figure
hist3([re, im], [50,50])
xlabel('Re','interpreter','latex')
ylabel('Im','interpreter','latex')
title('Constellation','interpreter','latex','FontSize',15)

view(0,90)
axis([minx maxx miny maxy])
daspect([1 1 1])

set(get(gca,'child'),'FaceColor','interp',...
    'CDataMode','auto','Linestyle','none');
set(gca,'Xtick',[-1 1],'Ytick',[-1 1]);
c = colormap(gray);
colormap( flipud(c) );

end