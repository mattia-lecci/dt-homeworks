function plotGammaGraphs(D,gammaMin,legendEntries,colors)

figure
plot(D, gammaMin(:,1), 'Color',colors(1,:),'LineWidth',1);hold on; grid on

for i = 2:size(gammaMin,2)
    plot(D, gammaMin(:,i), 'Color',colors(i,:),'LineWidth',1)
end

title('\gamma_{min}')
xlabel('D')
ylabel('\gamma_{min,dB}')
legend(legendEntries)

end