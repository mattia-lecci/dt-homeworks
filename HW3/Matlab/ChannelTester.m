clear variables
close all
clc

addpath('../../HW2/Matlab')

% parameters
Q0 = 2;
qc = [0.5 1 0.5];
sigmaw2 = 0;
T = 1;

% channel
ch = Channel(qc, Q0, sigmaw2);
ch.plotImpResp(T);

% I/O
ak = pnseq(ones(3,1), '+-1');
rc = ch.send(ak);

stem(rc)

% checking mamory by adding 1 new input
rc2 = ch.send(2);

stem([rc;rc2])

%% testing delay
a = Channel([0 0 0 1 2 3], 10,0);
b = Channel([1 2 3], 10,0);
c = Channel([1 2 3 0 0 0], 10,0);
d = Channel([0 0 1 2 0 3 0 0 0], 10,0);
% note: channel length=13 -> output is always length(input)*Q0 => last ones are truncated
e = Channel([0 0 0 0 0 0 0 0 0 0 1 2 3], 10,0);
f = Channel([0 0 0 1],2,0);

ak = randn(30,1);
ak1 = ak(1:15);
ak2 = ak(16:end);

outa1 = a.send(ak1);
outa2 = a.send(ak2);
a.reset;
outa = a.send(ak);
figure
stem(outa); hold on
stem([outa1;outa2])
fprintf('%d',isequal(outa,[outa1;outa2]))

outb1 = b.send(ak1);
outb2 = b.send(ak2);
b.reset;
outb = b.send(ak);
figure
stem(outb); hold on
stem([outb1;outb2])
fprintf('%d',isequal(outb,[outb1;outb2]))

outc1 = c.send(ak1);
outc2 = c.send(ak2);
c.reset;
outc = c.send(ak);
figure
stem(outc); hold on
stem([outc1;outc2])
fprintf('%d',isequal(outc,[outc1;outc2]))

outd1 = d.send(ak1);
outd2 = d.send(ak2);
d.reset;
outd = d.send(ak);
figure
stem(outd); hold on
stem([outd1;outd2])
fprintf('%d',isequal(outd,[outd1;outd2]))

oute1 = e.send(ak1);
oute2 = e.send(ak2);
e.reset;
oute = e.send(ak);
figure
stem(oute); hold on
stem([oute1;oute2])
fprintf('%d',isequal(oute,[oute1;oute2]))

outf1 = f.send(ak1);
outf2 = f.send(ak2);
f.reset;
outf = f.send(ak);
figure
stem(outf); hold on
stem([outf1;outf2])
fprintf('%d\n',isequal(outf,[outf1;outf2]))