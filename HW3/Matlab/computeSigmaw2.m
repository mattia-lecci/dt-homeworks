function sigmaw2 = computeSigmaw2(SNRdb,BMAP,Eqc)
% Simple utility function
%
% INPUTS:
% * SNRdb: required SNR in dB
% * BMAP: valid BMAP. See createBMAP.m
% ( * Eqc: Energy of the channel's response. Default: 1 )
% ( * Q0: channel oversampling rate. Default: 1 )
%
% OUTPUTS:
% * sigmaw2: the correspondent noise variance

% check args
narginchk(2,4)

if (nargin<3)
    Eqc = 1;
end

% computation
SNR = 10^(SNRdb/10);
Ma = mean( abs(BMAP).^2 );
Msc = Ma*Eqc;

sigmaw2 = Msc/SNR;

end