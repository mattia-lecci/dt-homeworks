function [map] = createBMAP(modulation, M, showConstellation)
%CREATEBMAP Creates the bitmap for the requested modulation with gray
% coding. If requested also shows the constellation.
%
% INPUTS:
% * modulation: either 'pam','qam','psk'
% * M: symbol alphabet cardinality. It must be an integer power of 2 >=2.
%       If modulation=='qam' it also needs to be a perfect square
% ( * showConstellation: if true shows the requested constellation.
%       Default: false. )
%
% OUTPUTS:
% * map: the map for the requested modulation. It's a column vector of
%   length M containing all the symbols corresponding to the
%   decimal-equivalent bit group (starting from 1). For more infos check
%   encodeBits.m

% arg check
narginchk(2,3);

if ( nargin<3 )
    showConstellation = false;
end

if ( ~any(strcmp(modulation, {'pam','qam','psk'})) )
    error('Modulation not recognized. Choose either pam,qam,psk')
end
if ( M<=1 )
    error('M<=1')
end
if ( mod( log2(M),1 )>1e-10 )
    error('M must be an integer power of 2')
end
if ( strcmp(modulation,'qam') && mod( sqrt(M),1 )>1e-10 )
    error('If modulation==qam, M must be a perfect square')
end

% create map
switch modulation
    case 'pam'
        map = getPamMap(M);
    case 'qam'
        map = getQamMap(M);
    case 'psk'
        map = getPskMap(M);
end

% if requested, show the created constellation
if (showConstellation)
    
    figure
    scatter(real(map), imag(map),'k','filled'); grid on
    title( sprintf('%d-%s',M, upper(modulation)) )
    daspect([1 1 1])
    ax = gca;
    ax.XTick = sort(unique(real(map)));
    ax.YTick = sort(unique(imag(map)));
    
    % LABELS
    % create sequence to encode
    n = 0:M-1;
    bl = de2bi(n);
    bl = bl';
    bl = bl(:)';
    ak = encodeBits(bl,map);
    bpS = log2(M);
    
    % print labels
    for i = 1:M
        x = real(ak(i));
        y = imag(ak(i));
        b_str = dec2bin(n(i), bpS);
        
        text(x,y+0.3,b_str, 'HorizontalAlignment', 'center');
    end
    
end

end

function map = getPamMap(M)

% creating alphabet
n = (1:M)';
a_n = 2*n-1-M;

map = getGrayCode(a_n);

end

function map = getQamMap(M)
% from Fig. 6.39 pag. 482

% utility
bpS = log2(M);
indexI = bpS/2+1:bpS;
indexQ = 1:bpS/2;

% evalutaing singularly every possible symbol
n = (0:M-1)';
bit_n = de2bi(n);

symbIndI = bi2de(bit_n(:,indexI))+1;
symbIndQ = bi2de(bit_n(:,indexQ))+1;

% considering the two halves as pam modulations
pamMap = getPamMap( sqrt(M) );
a_I = pamMap(symbIndI);
a_Q = pamMap(symbIndQ);

% creating map
map = a_I + 1j*a_Q;
end

function map = getPskMap(M)

n = (1:M)';
phi_n = pi/M*(2*n-1);
a_n = sqrt(2)*exp(1j*phi_n);

map = getGrayCode(a_n);

end

function [map, codeword, bitTable] = getGrayCode(a_n)
% a_n: alphabet

% init
M = length(a_n);
map = zeros(M,1);
bitTable = [0 1]';
L = length(bitTable);

% creating bit table iteratively
while ( L<M )
    bitTable = [zeros(L,1), bitTable;...
        ones(L,1), flipud(bitTable)];
    
    L = length(bitTable);
end

% converting bits to matlab indexes
codeword = bi2de(bitTable)+1; % indexes

% creating map
for i = 1:M
    map(i) = a_n( codeword==i );
end

end