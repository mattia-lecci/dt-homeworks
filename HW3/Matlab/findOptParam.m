clear variables
close all
clc

addpath('../../HW1/Matlab');
addpath('../../HW2/Matlab');

% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);

% parameters
M = 4;
T = 1;
Q0 = 2; % upsample ratio of the channel
N = length(qc); % channel length

M1min = 1;
M1max = 6; M1tot = M1max-M1min+1;
Dmin = 0;

% utility
BMAP = createBMAP('psk',M);
Ma = BMAP'*BMAP/length(BMAP);
Eqc = qc'*qc;

% preparing channel
sigmawc2 = computeSigmaw2(10,BMAP,Eqc); % Gamma=10dB
ch = Channel(qc,Q0,sigmawc2,T);

%% Parameters
pnBits = 6;
pn = pnseq(ones(pnBits,1),'+-1');
leg = strcat({'M1='},num2str((M1min:M1max)'));
leg2 = strcat({'M1='},num2str((M1min:2*M1max)'));
colors = jet(M1tot);
colors2 = jet(M1tot*2);

% MF+LE@T
if (false)
    rx = MF_LE(BMAP,ch);
    
    t0 = 5;
    
    col = M1max-M1min+1;
    row = (M1max+3)-Dmin+1;
    Jmin = zeros(row,col);
    gamma = zeros(row,col);

    for M1 = M1min:M1max
        Dmax = M1+3;
        for D = Dmin:Dmax
            Jmin(D+1,M1) = rx.train(pn,t0,M1,D);
            
            psiD = rx.psi( rx.t_psi==D );
            gamma(D+1,M1) = 2*abs(psiD)^2/( Jmin(D+1,M1)-abs(1-psiD)^2*Ma );
        end
    end
    
    D_ = Dmin:Dmax;
    
    plotGammaGraphs(D_,10*log10(abs(gamma)),leg,colors);
    ylim([5 10])
end

% MF+DFE@T
if (false)
    rx = GeneralDFE(BMAP,ch);
    
    t0 = 5;
    
    col = M1max-M1min+1;
    row = (M1max+3)-Dmin+1;%%
    gamma = zeros(row,col);
    
    for M1 = M1min:M1max
        Dmax = M1+3;
        for D = Dmin:Dmax
            M2 = 4+M1-1-D; % to nullify all elements after D, considering t0=5
            Jmin = rx.train(pn,t0,M1,M2,D);
            
            psiD = rx.psi( rx.t_psi==D );
            gamma(D+1,M1) = 2*abs(psiD)^2/( Jmin-abs(1-psiD)^2*Ma );
        end
    end
    
    D_ = Dmin:Dmax;
    plotGammaGraphs(D_,10*log10(abs(gamma)),leg,colors);
    ylim([5 10])
end

% AAF+DFE@T/2
if (false)
    rx = GeneralDFE(BMAP,ch);
    
    t0 = 5;
    
    col = 2*M1max-M1min+1;
    row = (2*M1max+3)-Dmin+1;%%
    gamma = zeros(row,col);
    
    for M1 = M1min:2*M1max
        Dmax = M1+5;
        for D = Dmin:Dmax
            M2 = floor( (10+M1-1)/2 )-D; % to nullify all elements after D, considering t0=5
            Jmin = rx.train(pn,t0,M1,M2,D,'aaf','1',2);
            
            psiD = rx.psi( rx.t_psi==D );
            gamma(D+1,M1) = 2*abs(psiD)^2/( Jmin-abs(1-psiD)^2*Ma );
        end
    end
    
    D_ = Dmin:Dmax;
    plotGammaGraphs(D_,10*log10(abs(gamma)),leg2,colors2);
    ylim([5 10])
end

% AAF+MF+DFE@T/2
if (false)
    rx = GeneralDFE(BMAP,ch);
    
    t0 = 4;
    
    col = 2*M1max-M1min+1;
    row = (2*M1max+3)-Dmin+1;%%
    gamma = zeros(row,col);
    
    for M1 = M1min:2*M1max
        Dmax = M1+5;
        for D = Dmin:Dmax
            M2 = floor( (10+M1-1)/2 )-D; % to nullify all elements after D, considering t0=5
            Jmin = rx.train(pn,t0,M1,M2,D,'aaf','mf',2);
            
            psiD = rx.psi( rx.t_psi==D );
            gamma(D+1,M1) = 2*abs(psiD)^2/( Jmin-abs(1-psiD)^2*Ma );
        end
    end
    
    D_ = Dmin:Dmax;
    plotGammaGraphs(D_,10*log10(abs(gamma)),leg2,colors2);
    ylim([5 10])
end

% AAF+DFE@T
if (true)
    rx = GeneralDFE(BMAP,ch);
    
    t0 = 5;
    
    col = M1max-M1min+1;
    row = (M1max+3)-Dmin+1;%%
    gamma = zeros(row,col);
    
    for M1 = M1min:M1max
        Dmax = M1+4;
        for D = Dmin:Dmax
            M2 = 5+M1-1-D; % to nullify all elements after D, considering t0=5
            Jmin = rx.train(pn,t0,M1,M2,D,'aaf','1');
            
            psiD = rx.psi( rx.t_psi==D );
            gamma(D+1,M1) = 2*abs(psiD)^2/( Jmin-abs(1-psiD)^2*Ma );
        end
    end
    
    D_ = Dmin:Dmax;
    plotGammaGraphs(D_,10*log10(abs(gamma)),leg,colors);
    ylim([5 10])
end