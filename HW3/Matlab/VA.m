classdef VA < handle
    
    properties
        BMAP;
        gm;
        c; % equalizer of length M1
        
        t0; % #samples
        D; % delay given y equalizer c
        Ds; % delay given by sampling in t0
        ch; % channel object
        
        psi;
        t_psi;
        h;
        n_h;
        
        memory_ak; % memory of the last Ds symbols ak
        memory_rc; % memory of the last length(gm)-1 samples rc
        memory_xk; % memory of the last length(c)-1 samples xk
        
        L1;
        L2;
        GammaPast; % Gamma_k-1
        GammaPres; % Gamma_k
        T; % NsxM matrix containing incoming sigmas. Each row j contains
            % the index i of the possible incoming sigma_i's to sigma_j
        f; % NsxM matrix containing f(sigma_j,sigma_i) to the corresponding
            % transition matrix T
        Lpast; % NsxKd matrix containing the estimated symbols in k-1
        Lpres; % NsxKd matrix containing the estimated symbols in k
        sigmas; % list of possible sigmas. sigma_j is sigmas(j,:)
    end
    
    methods
        function obj = VA(BMAP,ch)
            % Constructor
            obj.BMAP = BMAP;
            obj.ch = ch;
        end
        
        function Jmin = train(obj,trainSeq,t0,M1,M2,D,Kd)
            obj.reset();
            % utility
            qc = obj.ch.getQc();
            Q0 = obj.ch.qc.InterpolationFactor;
            sigmaw2 = obj.ch.sigmaw2;
            
            % memorize useful info
            obj.setGm(); % channel assumed known
            obj.t0 = t0;
            obj.D = D; % delay from eq
            obj.Ds = floor(t0/Q0); % delay given by sampling in t0
            
            Dtot = obj.D+obj.Ds;
            
            % compute h
            [obj.h,obj.n_h] = geth(qc,obj.gm,obj.t0,Q0);
            lh = sum(obj.n_h>=0); % only the "causal" part

            % find equalizer
            [rw,n_rw] = getWtildeCorr(sigmaw2,Q0,obj.gm);
            [obj.c,~,obj.psi,obj.t_psi,Jmin] = computeEq_FB(M1,M2,obj.D,obj.BMAP,obj.h,obj.n_h,rw,n_rw);
            obj.L1 = 0; % assume equalizer gets rid of precursors
            obj.L2 = sum(obj.t_psi>obj.D);
            
            % update memory
            trainSeq = complex(shiftdim(trainSeq)); % complex column vector
            ak = [trainSeq;...
                trainSeq(1:lh+M1-1)]*obj.BMAP(1); % symbols of the bitmap
            
            rc = obj.ch.send(ak);
            [rr,obj.memory_rc] = filter(obj.gm,1,rc);
            xk = rr(1+obj.t0:Q0:end); % shorter than ak due to t0
            [yk,obj.memory_xk] = filter(obj.c,1,xk);
            obj.memory_ak = zeros(Dtot,1);
            obj.updateMemoryAk(ak);
            
            % computer Viterbi support matrices
            N = obj.L1+obj.L2;
            initCond = ak(end-Dtot:-1:end-Dtot-N+1);
            initCond = obj.symbol2ind(initCond);
            
            obj.createSigmas();
            obj.createGammas(initCond);
            obj.createT();
            obj.createL(Kd);
            obj.createF();
        end
        
        function akDs_hat = receive(obj,rc)
            % Assuming initial state is a_-L1-1
            % Note: the output is an estimate of  {a_k}
            
            % init
            obj.checkIfTrained(); % otherwise error
            Q0 = obj.ch.qc.InterpolationFactor;
            Kd = size(obj.Lpres,2); % trellis depth
            akDs_hat = zeros(length(rc)/Q0,1);
            
            % if trained => t0 is already in steady state, don't have to
            % wait again t0, just smoothly continue the sampling
            t0mod = mod(obj.t0,Q0);
            
            % use initial conditions, save final conditions
            [rr,obj.memory_rc] = filter(obj.gm,1,rc,obj.memory_rc);
            xk = rr(1+t0mod:Q0:end);
            [rhok,obj.memory_xk] = filter(obj.c,1,xk,obj.memory_xk);
            
            % Viterbi algorithm
            % Assuming GammaPast already set (either from train or last tx)
            for k = 1:length(rhok) % for each symbol rx'ed
                % in steady state: if Kd is large enough, the first column
                % should be constant and equal to the estimate
                if ( k>Kd ) % trellis memory full -> save the estimate
                    akDs_hat(k-Kd) = obj.Lpres(1,1);
                end
                
                for j = 1:length(obj.GammaPres) % for each sigmaj
                    i_indexes = obj.T(j,:); % {i: sigmai->sigmaj}
                    fji = obj.f(j,:).'; % f(sigmaj,sigmai) with sigmai corresponding to i_indexes
                    
                    transCost = abs( rhok(k)-fji ).^2; % transition cost/branch metric
                    totCost = obj.GammaPast(i_indexes) + transCost;
                    [min_tot,i] = min(totCost); % i is the index of T(j,:)
                    
                    % store info on Gamma
                    obj.GammaPres(j) = min_tot;
                    
                    % store info on L
                    iopt = obj.T(j,i); % index of sigma_i,opt
                    obj.Lpres(j,1:end-1) = obj.Lpast(iopt,2:end);
                    obj.Lpres(j,end) = obj.sigmas(j,1); % new element of sigmaj
                end
                
                % ready for next turn
                obj.GammaPast = obj.GammaPres;
                obj.Lpast = obj.Lpres;
            end
            
            % complete with the last Kd symbols which yield the minimum
            % GammaPresent
            [~,iopt] = min(obj.GammaPres);
            nelements = min(Kd,length(rhok)); % maybe sent less symbols than Kd
            akDs_hat(end-nelements+1:end) = obj.Lpres(iopt,end-nelements+1:end);
            
            % convert symbol indexes to symbols
            akDs_hat = obj.BMAP(akDs_hat);
            % Leaving everything untouched for next reception
        end
        
        function A = createPerms(obj)
            % Mainly for debugging purposes
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            A = zeros(Ns,N);
            
            for j = 1:Ns
                sigma = obj.sigmas(j,:);
                A(j,:) = obj.BMAP(sigma);
                ind = obj.sigma2ind(sigma);
                disp(num2str(ind))
            end
        end
        
        function reset(obj)
            % Resets memory and state, keeping all filter as they are
            obj.memory_ak = zeros(size(obj.memory_ak));
            obj.memory_rc = zeros(size(obj.memory_rc));
            obj.memory_xk = zeros(size(obj.memory_xk));
            obj.ch.reset();
            
            obj.T = zeros(size(obj.T));
            obj.GammaPast = zeros(size(obj.GammaPast));
            obj.GammaPres = zeros(size(obj.GammaPres));
            obj.Lpast = zeros(size(obj.Lpast));
            obj.Lpres = zeros(size(obj.Lpres));
            obj.f = zeros(size(obj.f));
        end
        
        function updateMemoryAk(obj,ak)
            Dtot = obj.D+obj.Ds;
            n = min(length(ak),Dtot);
            obj.memory_ak = circshift(obj.memory_ak,-n);
            obj.memory_ak(end-n+1:end) = ak(end-n+1:end);
        end
        
    end
    
    methods(Access=private)
        
        function createSigmas(obj)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            obj.sigmas = zeros(Ns,N);
            
            for i = 1:Ns
                obj.sigmas(i,:) = obj.ind2sigma(i);
            end
        end
        
        function createGammas(obj,InitCond)
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            obj.GammaPres = zeros(Ns,1);
            
            if ( isempty(InitCond) ) % uniform distribution
                obj.GammaPast = zeros(Ns,1);
            else
                i = obj.sigma2ind(InitCond);
                obj.GammaPast = Inf(Ns,1);
                obj.GammaPast(i) = 0;
            end
        end
        
        function createF(obj)
            if (isempty(obj.T))
                error('You need to create T before f')
            end
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            obj.f = zeros(size(obj.T));
            postPsi = shiftdim( obj.psi(end-obj.L2:end)); % column (excluding precursors)
            
            % use the same order as T
            fullState = zeros(1,length(postPsi));
            for j = 1:Ns
                sigmaj = obj.sigmas(j,:);
                fullState(1:end-1) = obj.BMAP(sigmaj);
                
                for i = 1:M
                    fullState(end) = obj.BMAP(i);
                    
                    obj.f(j,i) = fullState*postPsi;
                end
            end
        end
        
        function createT(obj)
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            obj.T = zeros(Ns,M,getMinType(Ns)); % to minimize memory usage
            
            % fixing sigmaj in row j, which are the possible (incoming)
            % sigmai?
            sigmai = zeros(1,N);
            for j = 1:Ns
                sigmaj = obj.sigmas(j,:);
                sigmai(1:end-1) = sigmaj(2:end); % first part of sigmai is last part of sigmaj
                
                for i = 1:M
                    sigmai(end) = i;
                    obj.T(j,i) = obj.sigma2ind(sigmai);
                end
            end
        end
        
        function createL(obj,Kd)
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            obj.Lpast = zeros(Ns,Kd);
            obj.Lpres = zeros(Ns,Kd);
        end
        
        function [h,n] = geth(obj)
            % Returns h_n after sampling, considering t0
            % and trimming leading/trailing zeros
            
            % init
            Q0 = obj.ch.qc.InterpolationFactor;
            
            qc = obj.ch.getQc();
            h1 = conv(qc,obj.gm);
            
            % downsample
            t0mod = mod(obj.t0,Q0);
            h = h1(1+t0mod:Q0:end);
            
            % timing
            n0 = (obj.t0-t0mod)/Q0;
            n = (0:length(h)-1)-n0;
            
            % trim
            ind0 = find(h,1); % first non-zero
            indLast = find(h,1,'last'); % last non-zero
            h = h(ind0:indLast);
            n = n(ind0:indLast);
        end
        
        function symb_ind = symbol2ind(obj,symbol)
            
            symb_ind = zeros(size(symbol));
            for i = 1:length(symbol)
                boolInd = abs( obj.BMAP-symbol(i) )<1e-10;
                symb_ind(i) = find(boolInd);
            end
            
        end
        
        function setGm(obj)
            % matched filter based on channel
            qc1 = obj.ch.qc.Numerator; % without delay
            qc1 = shiftdim(qc1); % column
            obj.gm = conj(flipud(qc1)); % gm(n)=qc*(n0-n), n0 is implicit
        end
        
        function checkIfTrained(obj)
            if ( isempty(obj.gm) || isempty(obj.c) ||...
                    isempty(obj.t0) || isempty(obj.D) ||...
                    isempty(obj.Ds) || isempty(obj.ch) )
                error('You need to train the receiver first')
            end
        end

        function ind = sigma2ind(obj,sigma)
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            
            if ( length(sigma)~=N )
                error('Incorrect length for sigma')
            end
            
            sigma_ = shiftdim(sigma)-1; % column
            expVector = M.^(N-1:-1:0); % row
            
            % basically a change of base
            ind = expVector*sigma_+1;
        end
        
        function sigma = ind2sigma(obj,ind)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            
            sigma = zeros(1,N);
            ind_ = ind-1;
            i = 0;
            
            % basically a change of base
            while ( ind_>0 )
                m = mod(ind_,M);
                sigma(end-i) = m;
                
                ind_ = (ind_-m)/M;
                i = i+1;
            end
            
            sigma = sigma+1;
        end

    end
end