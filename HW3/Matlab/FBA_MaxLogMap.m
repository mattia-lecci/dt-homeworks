classdef FBA_MaxLogMap < handle
    
    properties
        BMAP;
        gm;
        c; % equalizer of length M1
        
        t0; % #samples
        D; % delay given y equalizer c
        Ds; % delay given by sampling in t0
        ch; % channel object
        
        memory_ak; % memory of the last Ds symbols ak
        memory_rc; % memory of the last length(gm)-1 samples rc
        memory_xk; % memory of the last length(c)-1 samples xk
        
        psi;
        t_psi;
        h;
        n_h;
        
        L1;
        L2;
        T_prev; % NsxM matrix containing possible transitions from past to present
            % i.e. T_prev(j,:) contains the indexes of the only sigmas for which
            % a transition to j exists
        T_next; % NsxM matrix containing possible next transitions
            % i.e. T_next(i,:) contains the indexes of the only sigmas that
            % can be reached from i
        f; % NsxNs matrix containing f(j,i)
        sigmas; % NsxN matrix. sigmas(i,:) corresponds to sigmai
        b_; % NsxK+1 matrix \bar(b)
        f_past; % 1xNs vector containing \bar(f)_{k-1}
    end
    
    methods
        function obj = FBA_MaxLogMap(BMAP,ch)
            % Constructor
            obj.BMAP = BMAP;
            obj.ch = ch;
        end
        
        function Jmin = train(obj,trainSeq,t0,M1,M2,D)
            obj.reset();
            % utility
            qc = obj.ch.getQc();
            Q0 = obj.ch.qc.InterpolationFactor;
            sigmaw2 = obj.ch.sigmaw2;
            
            % memorize useful info
            obj.setGm(); % channel assumed known
            obj.t0 = t0;
            obj.D = D; % delay from eq
            obj.Ds = floor(t0/Q0); % delay given by sampling in t0
            
            Dtot = obj.D+obj.Ds;
            
            % compute h
            [obj.h,obj.n_h] = geth(qc,obj.gm,obj.t0,Q0);
            lh = sum(obj.n_h>=0); % only the "causal" part

            % find equalizer
            [rw,n_rw] = getWtildeCorr(sigmaw2,Q0,obj.gm);
            [obj.c,~,obj.psi,obj.t_psi,Jmin] = computeEq_FB(M1,M2,obj.D,obj.BMAP,obj.h,obj.n_h,rw,n_rw);
            obj.L1 = 0; % assume equalizer gets rid of precursors
            obj.L2 = sum(obj.t_psi>obj.D);
            
            % update memory
            trainSeq = shiftdim(trainSeq); % column
            ak = [trainSeq;...
                trainSeq(1:lh+M1-1)]*obj.BMAP(1); % symbols of the bitmap

            rc = obj.ch.send(ak);
            [rr,obj.memory_rc] = filter(obj.gm,1,rc);
            xk = rr(1+obj.t0:Q0:end); % shorter than ak due to t0
            [yk,obj.memory_xk] = filter(obj.c,1,xk);
            obj.memory_ak = zeros(Dtot,1);
            obj.updateMemoryAk(ak);
            
            % computer Viterbi support matrices
            N = obj.L1+obj.L2;
            initCond = ak(end-Dtot:-1:end-Dtot-N+1);
            initCond = obj.symbol2ind(initCond);
            
            obj.createSigmas();
            obj.createTprev();
            obj.createTnext();
            obj.createf();
            obj.create_f_past(initCond);
        end
        
        function [ak_hat,log_like] = receive(obj,rc,finalCond)
            
            if (nargin<3)
                finalCond = [];
            end
            
            % init
            obj.checkIfTrained(); % otherwise error
            Q0 = obj.ch.qc.InterpolationFactor;
            
            % if trained => t0 is already in steady state, don't have to
            % wait again t0, just smoothly continue the sampling
            t0mod = mod(obj.t0,Q0);
            
            % use initial conditions, save final conditions
            [rr,obj.memory_rc] = filter(obj.gm,1,rc,obj.memory_rc);
            xk = rr(1+t0mod:Q0:end);
            [rhok,obj.memory_xk] = filter(obj.c,1,xk,obj.memory_xk);
            
            % FBA algorithm
            obj.computeb_(rhok,finalCond);
            log_like = obj.compute_loglike(rhok);
            
            % Max-Log-MAP criterion
            [~,ak_hat_ind] = max(log_like);
            ak_hat = obj.BMAP(ak_hat_ind);
        end
        
        
        %% Public utilities
        
        function reset(obj)
            % Resets memory and state, keeping all filter as they are
            obj.memory_ak = zeros(size(obj.memory_ak));
            obj.memory_rc = zeros(size(obj.memory_rc));
            obj.memory_xk = zeros(size(obj.memory_xk));
            obj.ch.reset();
            
            obj.f = zeros(size(obj.f));
            obj.b_ = [];
            obj.f_past = zeros(size(obj.f_past));
        end
        
        function updateMemoryAk(obj,ak)
            Dtot = obj.D+obj.Ds;
            n = min(length(ak),Dtot);
            obj.memory_ak = circshift(obj.memory_ak,-n);
            obj.memory_ak(end-n+1:end) = ak(end-n+1:end);
        end
    end
    

    methods(Access=private)
        
        %% Creation
        function createSigmas(obj)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            obj.sigmas = zeros(Ns,N);
            
            for i = 1:Ns
                obj.sigmas(i,:) = obj.ind2sigma(i);
            end
        end
        
        function createTprev(obj)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            obj.T_prev = zeros(Ns,M);
            for j = 1:Ns
                sigmaj = obj.sigmas(j,:);
                sigmai = circshift(sigmaj, [0,-1]); % first part of sigmai is last part of sigmaj
                
                for m = 1:M
                    sigmai(end) = m; % find all sigmai's by changing last position
                    obj.T_prev(j,m) = obj.sigma2ind(sigmai);
                end
            end
        end
        
        function createTnext(obj)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            obj.T_next = zeros(Ns,M);
            for i = 1:Ns
                sigmai = obj.sigmas(i,:);
                sigmaj = circshift(sigmai, [0,1]); % last part of sigmaj is first part of sigmai
                
                for m = 1:M
                    sigmaj(1) = m; % find all sigmai's by changing last position
                    obj.T_next(i,m) = obj.sigma2ind(sigmaj);
                end
            end
        end
        
        function createf(obj)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            obj.f = NaN(Ns,Ns); % if the transition is invalid -> NaN
            psi_fin = obj.psi( obj.t_psi>=obj.D );
            psi_fin = shiftdim(psi_fin); % column
            
            if ( length(psi_fin)~=obj.L2+1 )
                error('psi_fin has invalid length')
            end
            
            % compute f(j,i)
            fullState_ind = zeros(obj.L2+1,1);
            for j = 1:Ns
                sigmaj = obj.sigmas(j,:);
                fullState_ind(1:end-1) = sigmaj; % initial part: sigmaj
                
                is = obj.T_prev(j,:);
                
                for m = 1:M
                    fullState_ind(end) = obj.sigmas(is(m),end); % last part: sigmai
                    fullState_symb = obj.BMAP(fullState_ind);
                    
                    obj.f(j,is(m)) = fullState_symb.'*psi_fin;
                end
            end
        end
        
        function create_f_past(obj,initCond)
            
            if (nargin<2)
                initCond = [];
            end
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            if ( ~isempty(initCond) && length(initCond)~=N )
                error('Invalid initial conditions')
            end
            
            if (isempty(initCond))
                obj.f_past = zeros(1,Ns);
            else
                obj.f_past = -Inf(1,Ns);
                
                ind = obj.sigma2ind(initCond);
                obj.f_past(ind) = 0;
            end
            
        end
        
        function createb_(obj,K,finalCond)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            
            if ( ~isempty(finalCond) && length(finalCond)~=N )
                error('Invalid final conditions')
            end
            
            obj.b_ = zeros(Ns,K+1);
            if (~isempty(finalCond)) % final conditions are known
                obj.b_(:,end) = -Inf;
                
                ind = obj.sigma2ind(finalCond);
                obj.b_(ind,end) = 0;
            end
        end
        
        %% Computation
        
        function computeb_(obj,rho,finalCond)
            % Assuming obj.b_ already initialized
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            K = length(rho);
            
            obj.createb_(K,finalCond);
            
            for k = K:-1:1
                for i = 1:Ns
                    js = obj.T_next(i,:); % possible j indexes from i
                    
                    if (k==K) % by def @pag.676
                        ck1 = zeros(size(js.'));
                    else
                        ck1 = -abs( rho(k+1)-obj.f(js,i) ).^2; % c_{k+1}(j|i) for all possible j
                    end
                    
                    obj.b_(i,k) = max( obj.b_(js,k+1)+ck1 );
                end
            end
        end
        
        function log_like = compute_loglike(obj,rho)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            Ns = M^N;
            K = length(rho);
            log_like = zeros(M,K); % log-likelihood for each rx for each symbol
            
            % create bitmasks for log-likelihood
            % recall: l_k(beta) = max{ v_k(i) } over {i: sigmai_1==beta}
            masks = false(Ns,M); % init
            for beta = 1:M
                masks(:,beta) = obj.sigmas(:,1)==beta;
            end
            
            % start forward + state + log-like
            fk_ = zeros(1,Ns); % \bar{f_k}
            for k = 1:K % for each symbol rx'ed
                for j = 1:Ns % for each sate
                    
                    is = obj.T_prev(j,:); % possible i indexes to j
                    ck = -abs( rho(k)-obj.f(j,is) ).^2; % c_k(j|i) for all possible i
                    fk_(j) = max( obj.f_past(is)+ck );
                    
                end
                % state metric
                vk_ = fk_.'+obj.b_(:,k); % \bar{v_k}(i) for each i
                
                % log likelihood
                for beta = 1:M
                    log_like(beta,k) = log(sum(exp( vk_(masks(:,beta)) )));
                end
                
                obj.f_past = fk_; % update on the fly f_k-1
            end
        end
        
        %% Private utilities
        
        function [h,n] = geth(obj)
            % Returns h_n after sampling, considering t0
            % and trimming leading/trailing zeros
            
            % init
            Q0 = obj.ch.qc.InterpolationFactor;
            
            qc = obj.ch.getQc();
            h1 = conv(qc,obj.gm);
            
            % downsample
            t0mod = mod(obj.t0,Q0);
            h = h1(1+t0mod:Q0:end);
            
            % timing
            n0 = (obj.t0-t0mod)/Q0;
            n = (0:length(h)-1)-n0;
            
            % trim
            ind0 = find(h,1); % first non-zero
            indLast = find(h,1,'last'); % last non-zero
            h = h(ind0:indLast);
            n = n(ind0:indLast);
        end
        
        function symb_ind = symbol2ind(obj,symbol)
            
            symb_ind = zeros(size(symbol));
            for i = 1:length(symbol)
                boolInd = abs( obj.BMAP-symbol(i) )<1e-10;
                symb_ind(i) = find(boolInd);
            end
            
        end
        
        function setGm(obj)
            % matched filter based on channel
            qc1 = obj.ch.qc.Numerator; % without delay
            qc1 = shiftdim(qc1); % column
            obj.gm = conj(flipud(qc1)); % gm(n)=qc*(n0-n), n0 is implicit
        end
        
        function checkIfTrained(obj)
            if ( isempty(obj.gm) || isempty(obj.c) ||...
                    isempty(obj.t0) || isempty(obj.D) ||...
                    isempty(obj.Ds) || isempty(obj.ch) )
                error('You need to train the receiver first')
            end
        end

        function ind = sigma2ind(obj,sigma)
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            
            if ( length(sigma)~=N )
                error('Incorrect length for sigma')
            end
            
            sigma_ = shiftdim(sigma)-1; % column
            expVector = M.^(N-1:-1:0); % row
            
            % basically a change of base
            ind = expVector*sigma_+1;
        end
        
        function sigma = ind2sigma(obj,ind)
            
            % init
            M = length(obj.BMAP);
            N = obj.L1+obj.L2;
            
            sigma = zeros(1,N);
            ind_ = ind-1;
            i = 0;
            
            % basically a change of base
            while ( ind_>0 )
                m = mod(ind_,M);
                sigma(end-i) = m;
                
                ind_ = (ind_-m)/M;
                i = i+1;
            end
            
            sigma = sigma+1;
        end
        
    end
end