clear variables
close all
clc

addpath('../../HW1/Matlab');
addpath('../../HW2/Matlab');

% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);

% parameters
M = 4;
T = 1;
Q0 = 2; % upsample ratio of the channel
pn_l = 5; % pn sequence's bits
N = length(qc); % channel length
NFFT = 1024;
Gamma_min = 8; % dB
Gamma_max = 14; % dB
Npts = 10; % #points in final plot
Pe_min = 1e-4; % inferior ylim
Pe_max = 1e-1; % superior ylim
minIter = 1e5; % min number of iterations to find Pe
maxIter = 2e6; % max number of iterations to find Pe
minErr = 1e3; % min number of errors to stop simulation earlier
showConstellation = false;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
saveMat = false;%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
savePlots = false;

% simulations to run
run_mfle = false;
run_mfdfe = false;
run_aafdfe2 = false;
run_aafmfdfe2 = false;
run_aafdfe = false;
run_va = false;
run_fba = false;
run_bound_sim = false;

% utility
BMAP = createBMAP('psk',M);
Ma = BMAP'*BMAP/length(BMAP);
Eqc = qc'*qc;

%% plot qc/Qc

% preparing channel
sigmawc2 = computeSigmaw2(10,BMAP,Eqc); % Gamma=10dB
ch = Channel(qc,Q0,sigmawc2,T);

% plots
ch.plotImpResp(savePlots);

ch.plotFreqResp(NFFT,savePlots);


%% RC parameters @10dB
SNRdb = 10;
pnBits = 6;
pn = pnseq(ones(pnBits,1),'+-1');

% MF+LE@T
if (run_mfle)
    t0_mfle = 5;
    M1_mfle = 9;
    D_mfle = 6;
    
    mfle = MF_LE(BMAP,ch);
    mfle.train(pn,t0_mfle,M1_mfle,D_mfle);
    
    mfle.plotGm(savePlots);
    mfle.plotC(savePlots);
    mfle.plotPSI(savePlots);
    
    %Pe = estimateErrorRate(mfle,minIter,maxIter,minErr,showConstellation);
end

% MF+DFE@T (parameters are used also by VA and FBA)
t0_mfdfe = 5;
M1_mfdfe = 5;
D_mfdfe = 6;
M2_mfdfe = 4+M1_mfdfe-1-D_mfdfe; % to nullify all elements after D, considering t0=5
if (run_mfdfe)

    mfdfe = GeneralDFE(BMAP,ch);
    mfdfe.train(pn,t0_mfdfe,M1_mfdfe,M2_mfdfe,D_mfdfe);
    
    mfdfe.plotG1( '$|g_M| @\frac{T}{2}$' , savePlots );
    mfdfe.plotC(savePlots);
    mfdfe.plotB(savePlots);
    mfdfe.plotPSI(savePlots);
    
    %Pe = estimateErrorRate(mfdfe,minIter,maxIter,minErr,showConstellation);
end


% AAF+DFE@T/2
if (run_aafdfe2)
    t0_aafdfe2 = 4;
    M1_aafdfe2 = 7;
    D_aafdfe2 = 4;
    M2_aafdfe2 = floor( (8+M1_aafdfe2-1)/2 )-D_aafdfe2; % to nullify all elements after D
    
    aafdfe2 = GeneralDFE(BMAP,ch);
    aafdfe2.train(pn,t0_aafdfe2,M1_aafdfe2,M2_aafdfe2,D_aafdfe2,'aaf','1',2);
    
    aafdfe2.plotG1( '$|g_{AA}| @\frac{T}{2}$', savePlots);
    aafdfe2.plotG1Freq(NFFT, '$|G_{AA}(f)|$', savePlots); ylim([-15 5])
    aafdfe2.plotC(savePlots);
    aafdfe2.plotB(savePlots);
    aafdfe2.plotPSI(savePlots);
    
    %Pe = estimateErrorRate(aafdfe2,minIter,maxIter,minErr,showConstellation);
end

% AAF+MF+DFE@T/2
if (run_aafmfdfe2)
    t0_aafmfdfe2 = 4;
    M1_aafmfdfe2 = 12;
    D_aafmfdfe2 = 8;
    M2_aafmfdfe2 = floor( (18+M1_aafmfdfe2-1)/2 )-D_aafmfdfe2; % to nullify all elements after D with t0=5
    
    aafmfdfe2 = GeneralDFE(BMAP,ch);
    aafmfdfe2.train(pn,t0_aafmfdfe2,M1_aafmfdfe2,M2_aafmfdfe2,D_aafmfdfe2,'aaf','mf',2);
    
    aafmfdfe2.plotG2( '$|g_{M}| @\frac{T}{2}$', savePlots );
    aafmfdfe2.plotG2Freq(NFFT, '$|G_M(f)|$', savePlots);
    aafmfdfe2.plotC(savePlots);
    aafmfdfe2.plotB(savePlots);
    aafmfdfe2.plotPSI(savePlots);
    
    %Pe = estimateErrorRate(aafmfdfe2,minIter,maxIter,minErr,showConstellation);
end

% AAF+DFE@T
if (run_aafdfe)
    t0_aafdfe = 5;
    M1_aafdfe = 3;
    D_aafdfe = 3;
    M2_aafdfe = 4+M1_aafdfe-1-D_aafdfe; % to nullify all elements after D
    
    aafdfe = GeneralDFE(BMAP,ch);
    aafdfe.train(pn,t0_aafdfe,M1_aafdfe,M2_aafdfe,D_aafdfe,'aaf','1');
    
    aafdfe.plotC(savePlots);
    aafdfe.plotB(savePlots);
    aafdfe.plotPSI(savePlots);
    
    %Pe = estimateErrorRate(aafdfe,minIter,maxIter,minErr,showConstellation);
end

% VA
if (run_va)
    t0_va = t0_mfdfe;
    M1_va = M1_mfdfe;
    D_va = D_mfdfe;
    M2_va = M2_mfdfe;
    Kd_va = 8;
    
    va = VA(BMAP,ch);
    va.train(pn,t0_va,M1_va,M2_va,D_va,Kd_va);
    
    mystem(va.t_psi,abs(va.psi),'$|\psi_n|$','$nT$','$|\cdot|$')
    %Pe = estimateErrorRate(va,minIter,maxIter,minErr);
end

% FBA (MAX-LOG-MAP)
if (run_fba)
    t0_fba = t0_mfdfe;
    M1_fba = M1_mfdfe;
    D_fba = D_mfdfe;
    M2_fba = M2_mfdfe;
    
    fba = FBA_MaxLogMap(BMAP,ch);
    Jmin = fba.train(pn,t0_fba,M1_fba,M2_fba,D_fba);
    
    mystem(fba.t_psi,abs(fba.psi),'$|\psi_n|$','$nT$','$|\cdot|$')
    %Pe = estimateErrorRate(fba,minIter,maxIter,minErr);
end

%% RC performance
showConstellation = false;
Gamma_db = linspace(Gamma_min,Gamma_max,Npts);
Gamma = 10.^(Gamma_db/10);

figure
myred = [237 28 36]/255;
mygreen = [0 175 0]/255;
myblue = [0 128 192]/255;

% MF+LE@T
Pe_mfle = zeros(Npts,1);

disp 'MF+LE@T'
if (run_mfle)
    for i = 1:Npts
        mfle = MF_LE(BMAP,ch);
        mfle.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        mfle.reset();
        mfle.train(pn,t0_mfle,M1_mfle,D_mfle);
        
        tic
        [Pe_mfle(i),Niter,errors] = estimateErrorRate(mfle,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
        if (Pe_mfle(i)<=Pe_min) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pe_mfle','Pe_mfle')
    end
else
    load('Pe_mfle_test','Pe_mfle')
end

semilogy(Gamma_db, Pe_mfle,'--','Color',myblue,'LineWidth',1); hold on
% MF+DFE@T
Pe_mfdfe = zeros(Npts,1);

disp 'MF+DFE@T'
if (run_mfdfe)
    for i = 1:Npts
        mfdfe = GeneralDFE(BMAP,ch);
        mfdfe.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        mfdfe.reset();
        mfdfe.train(pn,t0_mfdfe,M1_mfdfe,M2_mfdfe,D_mfdfe);
        
        tic
        [Pe_mfdfe(i),Niter,errors] = estimateErrorRate(mfdfe,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
        if (Pe_mfdfe(i)<=Pe_min) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pe_mfdfe','Pe_mfdfe')
    end
else
    load('Pe_mfdfe_test','Pe_mfdfe')
end

semilogy(Gamma_db, Pe_mfdfe,'Color',myblue','LineWidth',1)
% AAF+DFE@T/2
Pe_aafdfe2 = zeros(Npts,1);

disp 'AAF+DFE@T/2'
if (run_aafdfe2)
    for i = 1:Npts
        aafdfe2 = GeneralDFE(BMAP,ch);
        aafdfe2.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        aafdfe2.reset();
        aafdfe2.train(pn,t0_aafdfe2,M1_aafdfe2,M2_aafdfe2,D_aafdfe2,'aaf','1',2);
        
        tic
        [Pe_aafdfe2(i),Niter,errors] = estimateErrorRate(aafdfe2,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
        if (Pe_aafdfe2(i)<=Pe_min) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pe_aafdfe2','Pe_aafdfe2')
    end
else
    load('Pe_aafdfe2_test','Pe_aafdfe2')
end

semilogy(Gamma_db, Pe_aafdfe2,'k--','LineWidth',1)
% AAF+MF+DFE@T/2
Pe_aafmfdfe2 = zeros(Npts,1);

disp 'AAF+MF+DFE@T/2'
if (run_aafmfdfe2)
    for i = 1:Npts
        aafmfdfe2 = GeneralDFE(BMAP,ch);
        aafmfdfe2.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        aafmfdfe2.reset();
        aafmfdfe2.train(pn,t0_aafmfdfe2,M1_aafmfdfe2,M2_aafmfdfe2,D_aafmfdfe2,'aaf','mf',2);
        
        tic
        [Pe_aafmfdfe2(i),Niter,errors] = estimateErrorRate(aafmfdfe2,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
        if (Pe_aafmfdfe2(i)<=Pe_min) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pe_aafmfdfe2','Pe_aafmfdfe2')
    end
else
    load('Pe_aafmfdfe2_test','Pe_aafmfdfe2')
end

semilogy(Gamma_db, Pe_aafmfdfe2,'k:','LineWidth',1)
% AAF+DFE@T
Pe_aafdfe = zeros(Npts,1);

disp 'AAF+DFE@T'
if (run_aafdfe)
    for i = 1:Npts
        aafdfe = GeneralDFE(BMAP,ch);
        aafdfe.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        aafdfe.reset();
        aafdfe.train(pn,t0_aafdfe,M1_aafdfe,M2_aafdfe,D_aafdfe,'aaf');
        
        tic
        [Pe_aafdfe(i),Niter,errors] = estimateErrorRate(aafdfe,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
        if (Pe_aafdfe(i)<=Pe_min) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pe_aafdfe','Pe_aafdfe')
    end
else
    load('Pe_aafdfe_test','Pe_aafdfe')
end

semilogy(Gamma_db, Pe_aafdfe,'k','LineWidth',1)
% VA
Pe_va = zeros(Npts,1);

disp 'VA'
if (run_va)
    parfor i = 1:Npts
        va = VA(BMAP,ch);
        va.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        va.reset();
        va.train(pn,t0_va,M1_va,M2_va,D_va,Kd_va);
        
        tic
        [Pe_va(i),Niter,errors] = estimateErrorRate(va,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
%         if (Pe_va(i)<=Pe_min) % if too small, avoid doing the following ones
%             disp '>> break'
%             break;
%         end
    end
    
    if saveMat
        save('Pe_va','Pe_va')
    end
else
    load('Pe_va_test','Pe_va')
end

semilogy(Gamma_db, Pe_va,'--','Color',myred,'LineWidth',1)
% FBA (MAX-LOG-MAP)
Pe_fba = zeros(Npts,1);

disp 'FBA'
if (run_fba)
    parfor i = 1:Npts
        fba = FBA_MaxLogMap(BMAP,ch);
        fba.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        fba.reset();
        fba.train(pn,t0_fba,M1_fba,M2_fba,D_fba);
        
        tic
        [Pe_fba(i),Niter,errors] = estimateErrorRate(fba,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
        
%         if (Pe_fba(i)<=Pe_min) % if too small, avoid doing the following ones
%             disp '>> break'
%             break;
%         end
    end
    
    if saveMat
        save('Pe_fba','Pe_fba')
    end
else
    load('Pe_fba_test','Pe_fba')
end

semilogy(Gamma_db, Pe_fba,'Color',myred,'LineWidth',1)
% MF bound - Simulation
Pe_mf = zeros(Npts,1);

disp 'MF bound - Simulation'
if (run_bound_sim)
    for i = 1:Npts
        awgn = AWGN(BMAP,T);
        awgn.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP);
        awgn.ch.reset();
        
        tic
        [Pe_mf(i),Niter,errors] = estimateErrorRate(awgn,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, Niter=%8d, errors=%7d\n',i,Npts,t,Niter,errors);
    end
    
    if saveMat
        save('Pe_mf','Pe_mf')
    end
else
    load('Pe_mf_test','Pe_mf')
end

semilogy(Gamma_db,Pe_mf,'--','Color',mygreen,'LineWidth',1)
% MF bound - Theory
gamma = Gamma/(0.5*Ma);
Pe = 4*( 1-1/sqrt(M) )*qfunc( sqrt(gamma) );

semilogy(Gamma_db, Pe,'Color',mygreen,'LineWidth',1)

% improving plot
grid on
title('Results')
xlabel('$\Gamma_{dB}$','interpreter','latex')
ylabel('$P_e$','interpreter','latex')
xlim([Gamma_min Gamma_max])
ylim([Pe_min Pe_max])
legend('MF+LE@T',...
    'MF+DFE@T',...
    'AAF+DFE@T/2',...
    'AAF+MF+DFE@T/2',...
    'AAF+DFE@T',...
    'VA',...
    'FBA (Max-Log-MAP)',...
    'MF bound - Simulation',...
    'MF bound - Theory')

if (savePlots)
    matlab2tikz('../Latex/img/results.tex',...
        'extraaxisoptions',['title style={font=\Large\bfseries},'...
        'xlabel style={font=\large},'...
        'ylabel style={font=\large}']);
end