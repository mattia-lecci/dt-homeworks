function type = getMinType(N)
%GETMAXTYPE Returns the type string of the min unsigned integer type needed
% to represent all elements in N

if ( all(N<2^8) )
    type = 'uint8';
elseif ( all(N<2^16) )
    type = 'uint16';
elseif ( all(N<2^32) )
    type = 'uint32';
else
    type = 'uint64';
end

end