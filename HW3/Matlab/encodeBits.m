function [ak] = encodeBits(bl, map)
%ENCODEBITS Encodes the given bit sequence with the given map.
%
% INPUTS:
% * bl: the bit sequence. Must be an integer multiple of bpS (bits per
%       symbol, log2( length(map))
% * map: a valid bitmap. See createBMAP.m for more info
%
% OUTPUTS:
% * ak: the corresponding symbol sequence. The bit sequence is separated in
%       chunks bpS bits long. Every chunck if converted to decimal (MSB to
%       the right) and the corresponding symbol is chosen as map(1+this),
%       since indexes in matlab start from 1 while bit sequences start from
%       0. Hence, ak(1)=map( 1+bi2de(bl(1:bpS)) ) and so on.

% arg check
narginchk(2,2);

M = length(map); % alphabet cardinality
bpS = log2(M); % bit per Symbol

if ( ~isvector(map) || ~isvector(bl)  )
    error('map and bl must be vectors')
end
if ( ~all( bl==0 | bl==1 ) )
    error('init must be a 0-1 vector')
end
if ( M<=1 || mod( bpS,1 )>1e-10 )
    error('Map invalid! M=length(map) must be an integer power of 2 >=2')
end
if ( mod(length(bl), bpS)~=0 )
    error('length(bl) must be a multiple of log2(M)')
end

% bi2de needs bl to be row vector
if ( ~isrow(bl) )
    bl = bl.';
end

% init
index = 1:bpS;
N = length(bl)/bpS; % numberof blocks (assuming no remainder)

ak = zeros(N,1);

for i = 1:N
    
    % extract from bl and encode
    ind = 1 + bi2de( bl(index));
    ak(i) = map(ind);
    
    index = index+bpS;
end

end