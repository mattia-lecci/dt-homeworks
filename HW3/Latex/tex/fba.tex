\section{Scheme (g): Forward-Backward Algorithm (FBA)}

\begin{figure}[h]
	\centering
	\input{img/diag_g.tex}
\end{figure}

Since nowadays many different powerful decoders require more information in order to properly work (`soft information' instead of `hard detection' is needed), we would like an algorithm that returns this type of richer information.

Using the vector notation $\zbold_\ell^m$ which indicates the components from index $\ell$ to index $m$ of vector $\zbold$, we define the likelihood function as

\begin{equation}
L_k(\beta) = P[a_{k+L_1} = \beta | \zbold_0^{K-1} = \rh_0^{K-1}] \qquad \beta \in \mathcal{A}
\end{equation}

%We take a few step further in the statistical description of the sent sequence $\abold_k$ and the received sequence $\rh_k$. We define the transition probabilities as
%
%\begin{equation}
%\Pi(j|i) \triangleq P[\s_k = \sigma_j | \s_{k-1}= \sigma_i] = \begin{cases}
%	P[a_{k+L_1} = \beta]	& \sigma_i \rightarrow \sigma_j, \, (\sigma_j)_1 = \beta\\
%	0						& \sigma_i \nrightarrow \sigma_j
%\end{cases}
%\end{equation}
%
%Similarly as before, we define $p_{z_k}(\rho_k| j,i)$ as the single $k-th$ term of Eq.~\ref{eq:prob_za}.
%We also define the variable
%
%\begin{equation}
%C_k(j|i) = P[z_k=\rho_k, \s_k = \sigma_j | \s_{k-1} = \sigma_i] = ... = p_{z_k}(\rho_k| j,i) \, \Pi(j|i)
%\end{equation}
%
%We'll now briefly describe the FBA algorithm both in its original form and in its simplified version Max-Log-MAP.
%
%Starting from the original FBA algorithm, we introduce four metrics:
%
%\begin{enumerate}
%	\item Forward metric
%	\begin{equation}
%	F_k(j) = P[\zbold_0^k = \rho_0^k,\s_k = \sigma_j]
%	\end{equation}
%	
%	\item Backward metric
%	\begin{equation}
%	B_k(i) = P[\zbold_{k+1}^{K-1} = \rh_{k+1}^{K-1} | \s_k = \sigma_i]
%	\end{equation}
%	
%	\item State metric
%	\begin{equation}
%	V_k(i) = P[\s_k | \zbold_0^{K-1} = \rh_0^{K-1}]
%	\end{equation}
%	
%	\item Likelihood function of the generic symbol
%	\begin{equation}
%	L_k(\beta) = \sum_{i=1}^{N_s} P[a_{k+L_1} = \beta, \s_k = \sigma_i | \zbold_0^{K-1} = \rh_0^{K-1}]
%	\end{equation}
%	
%\end{enumerate}
%
%It can be shown that the following relations hold:
%
%\begin{enumerate}
%	\item Updating for $k=0,...,K-1$
%	\begin{equation}
%	F_k(j) = \sum_{\ell = 1}^{N_s} C_k(j|\ell) F_{k-1}(\ell) \qquad j = 1,...,N_s
%	\end{equation}
%	Initial conditions $F_{-1}(j)$ can be set as constant $1/N_s$ or $1-0$ if the initial state is known.
%	
%	\item Updating $k = K-1,...,0$ 
%	\begin{equation}
%	B_k(i) = \sum_{m=1}^{N_s} B_{k+1}(m) C_{k+1}(m|i) \qquad i = 1,...,N_s
%	\end{equation}
%	Final conditions $B_K(i)$ can be set as constant $1/N_s$ or $1-0$ if the final state is known.
%	
%	\item $\forall \, k=0,...,K-1$
%	\begin{equation}
%	V_k(i) = \frac{F_k(i)B_k(i)}{\sum_{n=1}^{N_s} F_k(n)B_k(n)} \qquad i = 1,...,N_s
%	\end{equation}
%	
%	\item $\forall \, k=0,...,K-1$, a general result
%	\begin{equation}
%	L_k(\beta) = \sum_{ \{i: (\sigma_i)_m = \beta \} } V_{k+(m-1)}(i) \qquad \beta \in \mathcal{A}
%	\end{equation}
%	
%\end{enumerate}

Note that the likelihood function gives us an estimate of the probability that the $k-th$ symbol is $\beta$ for each possible value of $\beta \in \mathcal{A}$. If we need a decision right away we could then use the MAP criterion to decide which symbol has the highest probability of being sent given the observation, namely

\begin{equation}
\hat{a}_k = \arg \max_{\beta \in \mathcal{A}} L_k(\beta) \qquad \forall \, k = 0,...,K-1
\end{equation}

We want to recall that if the sequence of symbols is iid (or we consider it as iid, maybe because we don't have more information about it), then the MAP criterion coincides with the ML criterion, hence the FBA algorithm should yield the same results as the Viterbi algorithm.

%Instead of using this basic version of the algorithm, we develop a little further some calculations that will give us the possibility of obtaining a very good approximation but with much less computational effort (in particular in the number of multiplications). For this purpose we will use lower case letters to denote the natural logarithm of the quantity, and a \emph{tilde} for the approximated quantity, e.g. $L_k(\beta) \rightarrow \tilde{\ell}_k(\beta)$.
%
%Hence, we can rewrite
%\begin{equation}
%	\ell_k(\beta) = \ln L_k(\beta) = \ln \sum_{ \{i: (\sigma_i)_1 = \beta \} } e^{v_k(i)}
%\end{equation}
%
%We can introduce an approximation by noting the fact that the exponential term tends to greatly amplify quantities $>1$ and attenuate those $<1$. In practice, it makes possible (usually) for one term to dominate the sum, hence we could write
%\begin{equation}
%	\tilde{\ell}_k(\beta) \approx \max_{ \{i: (\sigma_i)_m = \beta \} } v_k(i)
%\end{equation}
%
%Thus, after noticing that excluding constants (which do not play a role in the final results) for \emph{iid symbols} $C_k(j|i) \rightarrow c_k(j|i) = -|\rh_k - f(\s_k,\s_{k-1})|^2$, we can introduce this new approximation to al the previous metrics as follows:
%
%\begin{enumerate}
%	\item Updating $k = 0,...,K-1$
%	\begin{equation}
%	\tilde{f}_k(j) = \max_{ \ell \in \{1,...,N_s\} } \left( \tilde{f}_{k-1}(\ell) + c_k(j|\ell) \right)  \qquad j=1,...,N_s
%	\end{equation}
%	Using where the initial conditions $\tilde{f}_{-1}(j)$ are set to 0 if the initial state in unknown, or to $0/-\infty$ otherwise.
%	
%	\item Updating $k=K-1,...,0$
%	\begin{equation}
%	\tilde{b}_k(i) = \max_{ m \in \{1,...,N_s\} } \left( \tilde{g}_{k+1}(m) + c_{k+1}(m|i) \right)  \qquad i=1,...,N_s
%	\end{equation}
%	Similar considerations are done for the final conditions $\tilde{b}_K(i)$.
%	
%	\item $\forall \, k=0,...,K-1$
%	\begin{equation}
%	\tilde{v}_k(i) = \tilde{f}_k(i) + \tilde{b}_k(i) \qquad i=1,...,N_s
%	\end{equation}
%	
%	\item As already stated, the more general formula becomes
%	\begin{equation}
%	\tilde{\ell}_k(\beta) \approx \max_{ \{i: (\sigma_i)_m = \beta \} } \tilde{v}_{k+(m-1)}(i)
%	\end{equation}
%\end{enumerate}
%
%The decision rule (if needed) then becomes
%\begin{equation}
%\hat{a}_k = \arg\max_{ \beta \in \mathcal{A} } \; \tilde{\ell}_k(\beta) \qquad \forall k=0,...,K-1
%\end{equation}
%
%Note the complete absence of products in these formulas and also sums are replaced by maximum values.

In order to perform more rapid computations (basically avoiding the many multiplications usually required by the standard version), the algorithm can be tweaked, considering the natural logarithm of the metrics and only maximum values instead of sums, for further optimization. This version is called Max-Log FBA. We're not going to report all the theory and formulas here since it has been extensively discussed in class. 

To implement the FBA algorithm efficiently we can do once the backward propagation part at the beginning $\forall \, k$ and keep it in memory. Then, the forward propagation can be done only one index at a time, calculating directly the state metric and then the likelihood function, thus needing only the forward metric of the previous states. Again, similarly to what we said fo the Viterbi algorithm, we could keep in memory a few look-up tables to speed things up. Since we are using quite powerful computers with a lot of memory, we didn't divide the sequence in smaller blocks as suggested since it would only have slowed down the simulation.