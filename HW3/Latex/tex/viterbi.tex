\newcommand{\s}{\mathbf{s}}
\newcommand{\abold}{\mathbf{a}}
\newcommand{\zbold}{\mathbf{z}}
\newcommand{\ahat}{\mathbf{\hat{a}}}
\newcommand{\al}{\mathbf{\alpha}}
\newcommand{\rh}{\mathbf{\rho}}
\newcommand{\Ralpha}{\mathcal{R}_\al}
\newcommand{\probza}{p_{\zbold|\abold}(\rh|\al)}
\newcommand{\probaz}{p_{\abold|\zbold}(\al|\rh)}

\section{Scheme (f): Viterbi Algorithm}

\begin{figure}[h!]
	\centering
	\input{img/diag_f.tex}
\end{figure}

The Viterbi algorithm is an efficient way of implementing the ML (Maximum Likelihood) criterion exploiting the high correlation between two successive states by noting a simple fact: given that $\s_{k-1}=\sigma_i$, $\s_k=\sigma_j$ cannot be any of the $N_s$ states, but only $M$ of them. In fact, the degree of freedom is only given by one new symbol. This allows us to only check way less possibilities and significantly speeds up the search.

Without reporting all the theory and notation, we are just going to draw some quick considerations using the same notation of the book.

%Defining the metric
%
%\begin{equation}
%\Gamma_k = \sum_{i=0}^k |\rho_i - u_i|^2
%\end{equation}
%
%the following equations trivially hold:
%
%\begin{gather}
%\Gamma_k = \Gamma_{k-1} + |\rho_k - f(\s_k,\s_{k-1})|^2\\
%\Gamma(K-1) = \Gamma_{K-1}
%\end{gather}
%
%Then the ML criterion translates to
%
%\begin{equation}
%\ahat = \arg\min_{\al \in \mathcal{A}} \; \Gamma_{K-1}
%\end{equation}
%
%Now, defining
%
%\begin{enumerate}
%	\item A \emph{path metric} as
%	\begin{equation}
%	\Gamma(\s_k=\sigma_j) = \min_{\s_0,\s_1,...,\s_k=s\sigma_j} \Gamma_k
%	\end{equation}
%
%	\item A \emph{survivor sequence} (described equivalently as a sequence of states or a sequence of symbols) as
%	\begin{equation}
%	\mathcal{L}(\s_k=\sigma_j) = (\s_0,\s_1,...,\s_k=s\sigma_j)
%	\end{equation}
%	
%\end{enumerate}
%
%we note that the optimal sequence can be found recursively as follows:
%
%\begin{gather}
%\sigma_{i,opt} = \arg\min_{\sigma_i \rightarrow \sigma_j} \left( \Gamma(\s_{k-1}=\sigma_i) + |\rho_k - f(\sigma_j,\sigma_i)|^2 \right)\\
%%
%\Gamma(\s_k=\sigma_j) = \min_{\sigma_i \rightarrow \sigma_j} \left( \Gamma(\s_{k-1}=\sigma_i) + |\rho_k - f(\sigma_j,\sigma_i)|^2 \right)\\
%%
%\mathcal{L}(\s_k = \sigma_j) = ( \mathcal{L}(\s_{k-1} = \sigma_{i,opt}),\sigma_j )
%\end{gather}
%
%If we happen to know the initial state $\sigma_{i_0}$ of the system (previous to the first transmitted symbol, usually given by the training sequence) we could se the initial condition of the path metric
%
%\begin{equation}
%\Gamma(\s_{-1}=\sigma_{i_0}) = \left\{
%	\begin{array}{ll}
%	0		& \text{for } \sigma_i=\sigma_{i_0}\\
%	\infty	& \text{otherwise}
%	\end{array}
%\right.
%\end{equation}
%
%Otherwise, if the state is not known we could just set it all to $0$. Something analogous holds for the final state: we could consider only those sequences which end up in a final known state, ignoring all the others.

In order to implement it we only need two vectors of dimension $N_s$ to store the minimum cost metric for the previous and current states ($\Gamma_k$ and $\Gamma_{k-1}$), and a matrix ($\mathcal{L}$) to store the survivor sequences. It can be shown that that in practice, after a so called `\textit{trellis depth}' $K_d$, the oldest estimated symbols are exactly the same amongst all currently optimal paths. Hence, instead of storing a matrix as long as the received sequence, we only need a $N_s \times K_d$ matrix. For the given channel and the parameters $M_1,\,D$ from receiver (b) we observed that $K_d=5$ was enough for the trellis to converge. Just to be sure we used a slightly bigger value for it since the numbers were relatively small (e.g. $N_s = M^{L_1+L_2} = 4^{0+2} = 16$).

In order to speed things up we used a few look-up tables such as a matrix containing $f(\sigma_j,\sigma_i) \; \forall j,i$, a transition matrix $T$ containing only the possible transitions $\sigma_i \rightarrow \sigma_j \, \forall j$ and a list containing all possible $\sigma_i$ ordered by index that greatly simplifies the translation from index to state and vice versa.

Since we are using Scheme (b) for the first part of the receiver, we are not using directly the convention of the book. Instead of the equivalent response $\eta_n$, which considers $a_k$ as the `principal symbol' and precursors/postursors being the anti-causal and causal parts of this filter, we are considering $\psi_n$, which considers $a_{k-D}$ to be the `principal symbol' and $L_1, \; L_2$ the number of impulses respectively before and after the delay $D$. Note that we assumed $L_1=0$ since the equalizer should almost completely cancel the precursors, leaving us with $L_2=2$ as it can be seen from Fig.~\ref{fig:PSImfdfe}, p.\pageref{fig:PSImfdfe}. Hence

\begin{equation}
u_k = \tilde{f}(a_{k-D}, a_{k-D-1}, a_{k-D-2}) = f(\s_k,\s_{k-1})
\end{equation}

and the state definition then follows as 
\begin{equation}
\s_k = (a_{k-D}, a_{k-D-1})
\end{equation}