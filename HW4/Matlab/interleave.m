function cp = interleave(bl,Nrows,Mcols)
% Write by colummn, read by row
%
% INPUTS
% * bl: sequence. Must have length multiple of N*M
% * Nrows: #rows
% * Mcols: #cols
%
% OUTPUTS
% * cp: interlaced sequence. Same dimensions as bl

% arg check
narginchk(3,3)

if ( ~isvector(bl) )
    error('bl not a vector')
end
if ( mod( length(bl), Nrows*Mcols )~=0 )
    error('Length must be multiple of N*M')
end

tot = Nrows*Mcols;
nBlocks = length(bl)/tot;

cp = zeros(size(bl));
cp_index = 1:Mcols;

for b = 0:nBlocks-1
    bl_index = b*tot:Nrows:(b+1)*tot-1;
    bl_index = bl_index+1; % matlab starts from 1
    for n = 0:Nrows-1
        cp(cp_index) = bl(bl_index);
        
        bl_index = bl_index+1; % next row
        cp_index = cp_index+Mcols;
    end
end
        
end