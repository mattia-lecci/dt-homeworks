function sigmaw2 = computeOFDMsigmaw2(ofdm,Gamma_db)

Gamma = 10^(Gamma_db/10);

% fiter energy
gtx = ofdm.ch.gtx.Numerator;
totFilter = shiftdim( conv(gtx,ofdm.ch.qc) );
filtEn = totFilter'*totFilter;

Ma = ofdm.BMAP'*ofdm.BMAP/length(ofdm.BMAP);
sigmas2 = Ma/ofdm.M;

sigmaw2 = sigmas2*filtEn/Gamma;

end