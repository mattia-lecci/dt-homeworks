function bl = deinterleave(lp,Nrows,Mcols)
% Write by row, read by column
%
% INPUTS
% * cp: sequence. Must have length multiple of N*M
% * Nrows: #rows
% * Mcols: #cols
%
% OUTPUTS
% * bl: deinterlaced sequence. Same dimensions as cp

% arg check
narginchk(3,3)

if ( ~isvector(lp) )
    error('bl not a vector')
end
if ( mod( length(lp), Nrows*Mcols )~=0 )
    error('Length must be multiple of N*M')
end

tot = Nrows*Mcols;
nBlocks = length(lp)/tot;

bl = zeros(size(lp));
bl_index = 1:Nrows;

for b = 0:nBlocks-1
    cp_index = b*tot:Mcols:(b+1)*tot-1;
    cp_index = cp_index+1; % matlab starts from 1
    for n = 0:Mcols-1
        bl(bl_index) = lp(cp_index);
        
        cp_index = cp_index+1; % next col
        bl_index = bl_index+Nrows;
    end
end

end