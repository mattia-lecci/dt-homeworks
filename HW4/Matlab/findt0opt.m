clear varables
close all
clc


addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

SNR = 10;

%% OFDM

disp 'Computing OFDM stats...'

Q0 = 2;
gtx=rcosdesign(0.0625, 11, Q0);
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
Eqc = qc'*qc;
grc = gtx;

sigmaw2 = 0;
Tofdm = 1;
M = 512;
Nvir = 10;%%%%%%%%%%
Neff = M - (2*Nvir+1);
ch = OFDMChannel(gtx,qc,grc,Q0,sigmaw2,Tofdm,t0);
BMAP = QPSKbmap();

% send ak
Nsymb = Neff*1e4;
ak = createRandomSymbols(BMAP,Nsymb);


%% tests
% t0 = 5
ch.t0 = 5;
ofdm = OFDM(ch,BMAP,M,Nvir);
sigmaw2 = computeOFDMsigmaw2(ofdm,SNR);
ofdm.setSigmaw2(sigmaw2);

sigmaI2_5 = ofdm.sigmaI2;

ofdm.ch.ploth();

% t0 = 6
ch.t0 = 6;
ofdm = OFDM(ch,BMAP,M,Nvir);
sigmaw2 = computeOFDMsigmaw2(ofdm,SNR);
ofdm.setSigmaw2(sigmaw2);

sigmaI2_6 = ofdm.sigmaI2;

ofdm.ch.ploth();


figure
plot(10*log10(sigmaI2_5));hold on
plot(10*log10(sigmaI2_6));
title('(\sigma_I^2)_{dB}')
legend('t0=5','t0=6')