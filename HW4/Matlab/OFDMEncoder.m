function sk = OFDMEncoder(ak,M,Npx,Nvir)
% OFDM encoder with classic DMT methodology, i.e. only IFFT is performed
%
% INPUTS:
% * ak: input sequence
% * M: block size
% * Npx: length of cyclic prefix
% * Nvir: number of virtual carriers per side (total: 2Nvir+1). They are
%       set to 0 and not considered in decoding phase
%
% OUTPUTS:
% * sk: output sequence

totVir = 2*Nvir+1;
inBlockLen = M-totVir;
if ( mod(length(ak),inBlockLen)~=0 )
    error('length(ak) must be a multiple of M-(2Nvir+1)');
end

ak = shiftdim(ak);

% init
akLen = length(ak);
outblockLen = M+Npx;
nBlocks = akLen/inBlockLen;
skLen = nBlocks*outblockLen;
sk = zeros(skLen,1);

ind_ak_first = 1:(inBlockLen+1)/2;
ind_ak_last = (inBlockLen+1)/2+1:inBlockLen;
ind_sk = 1:outblockLen;

for i = 1:nBlocks
    % creating input block considering Nvir
    inputBlock = [ak(ind_ak_first);...
                zeros(totVir,1);...
                ak(ind_ak_last)];
    
    Ak = ifft( inputBlock, M);
    cycPrefix = Ak(end-Npx+1:end); % last Npx terms
    
    % for every block start with the prefix
    sk( ind_sk(1:Npx) ) = cycPrefix;
    sk( ind_sk(Npx+1:end) ) = Ak;
    
    % update indexes
    ind_ak_first = ind_ak_first+inBlockLen;
    ind_ak_last= ind_ak_last+inBlockLen;
    ind_sk = ind_sk+outblockLen;
end

end