clear variables
close all
clc

addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

%% PARAMETERS

% interleaver
Nrows = 41;
Mcols = 45;

% QPSK modulator
BMAP = QPSKbmap();
Ma = BMAP'*BMAP/length(BMAP);

% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
Eqc = qc'*qc;

% ofdm
Tofdm = 1;
Q0 = 2;
rho = 0.0625;
gtx = rcosdesign(rho, 11, Q0);
% qc: same as SC
grc = gtx; % same as tx
t0_ofdm = 6;
M = 512;

minIter = 1e4;
maxIter = 2e7;
minErr = 1e4;
SNR = 10;
encode = true;

savePlot = false;
run = false;
saveMat = false;

% creating channel
ofdm_ch = OFDMChannel(gtx,qc,grc,Q0,0,Tofdm,t0_ofdm);
ofdm = OFDM(ofdm_ch,BMAP,M,0); % dummy object to compute sigmaw2
sigmaw2 = computeOFDMsigmaw2(ofdm,SNR);
ofdm.ch.sigmaw2 = sigmaw2;

% to plot later
sigmaI2 = ofdm.computeSigmaI2();

%% choosing Nvir

NvirMin = 0;
NvirMax = 50;

Nvir_ = (NvirMin:NvirMax)';
efficiency = (M - 2*Nvir_-1)/M;

newBER = zeros(size(Nvir_));

if (run)
    for i = 1:length(Nvir_)
        % creating OFDM with new Nvir
        ofdm_ch.reset();
        ofdm = OFDM(ofdm_ch,BMAP,M,Nvir_(i));
        
        tic
        [newBER(i),totBits,errors] = estimateBER(ofdm,encode,minIter,maxIter,minErr,Nrows,Mcols);t=toc;
        
        fprintf('%3d/%3d: time=%5.1fs OFDM BER: %6d/%8d=%.2e\n',i,length(Nvir_),t,errors,totBits,newBER(i));
        
    end
    
    if (saveMat)
        save('Nvir','BER')
    end
else
    if (t0_ofdm == 6)
        load('Nvir_t0_6','BER')
    else
        load('Nvir_t0_5','BER')
    end
end

%% plot
figure
subplot(3,1,1)
semilogy(Nvir_,BER,'k','LineWidth',1); grid on
xlim([NvirMin NvirMax])
ylabel('BER')
%title('Choosing N_{vir}')

subplot(3,1,2)
n = 0:length(sigmaI2)-1;
n = n - ( M/2+1 );
plot(n,10*log10(sigmaI2),'k','LineWidth',1); grid on
xlim([-inf inf])
ylim([-15 5])
ylabel('(\sigma_I^2)_{dB}')

subplot(3,1,3)
plot(0:length(efficiency)-1,efficiency,'k','LineWidth',1); grid on
xlim([NvirMin NvirMax])
ylabel('efficiency')
xlabel('N_{vir}')

if (savePlot)
    matlab2tikz('../Latex/img/Nvir_t0_6.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%% both plots
% uncoded
figure

load('Nvir_t0_5')
semilogy(Nvir_,BER,'k','LineWidth',1); grid on;hold on
load('Nvir_t0_6')
semilogy(Nvir_,BER,'k:','LineWidth',1); grid on;hold on

xlim([NvirMin NvirMax])
ylabel('BER')
xlabel('N_{vir}')
%title('Choosing N_{vir}')
legend('t_0 = 5','t_0 = 6')

if (savePlot)
    matlab2tikz('../Latex/img/Nvir_uncoded_5vs6.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

% coded
figure

load('Nvir_t0_5_coded')
semilogy(Nvir_,BER,'k','LineWidth',1); grid on;hold on
load('Nvir_t0_6_coded')
semilogy(Nvir_,BER,'k:','LineWidth',1); grid on;hold on

xlim([NvirMin NvirMax])
ylabel('BER')
xlabel('N_{vir}')
%title('Choosing N_{vir}')
legend('t_0 = 5','t_0 = 6')

if (savePlot)
    matlab2tikz('../Latex/img/Nvir_coded_5vs6.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end

%% effective rate

rate = (M - (2*Nvir_+1))*log2(length(BMAP)); % /Tblock but it's const wrt Nvir

% uncoded
% importing BERs with t0=5,6
load('Nvir_t0_5')
effRate5 = rate.*(1-BER);
load('Nvir_t0_6')
effRate6 = rate.*(1-BER);

figure
plot(Nvir_,effRate5,'k','LineWidth',1); grid on; hold on
plot(Nvir_,effRate6,'k:','LineWidth',1)
title('Effective Rate','interpreter','latex')
xlabel('Nvir','interpreter','latex')
ylabel('$R_{eff}$','interpreter','latex')
set(gca,'Yscale','log')
legend('t_0=11','t_0=12')
ylim([980 1016])
xlim([0 10])

% coded
% importing BERs with t0=5,6
load('Nvir_t0_5_coded')
effRate5 = rate.*(1-BER);
load('Nvir_t0_6_coded')
effRate6 = rate.*(1-BER);

figure
plot(Nvir_,effRate5,'k','LineWidth',1); grid on; hold on
plot(Nvir_,effRate6,'k:','LineWidth',1)
title('Effective Rate','interpreter','latex')
xlabel('Nvir','interpreter','latex')
ylabel('$R_{eff}$','interpreter','latex')
set(gca,'Yscale','log')
legend('t_0=11','t_0=12')
ylim([940 1015])
xlim([0 20])