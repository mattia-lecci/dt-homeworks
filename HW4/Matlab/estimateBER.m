function [BER,totBits,errors] = estimateBER(obj,codedFlag,minBit,maxBit,minErr,nRows,nCols)
narginchk(5,7)
if (nargin<6)
    nRows = 41;
end
if (nargin<7)
    nCols = 45;
end

if (minBit>maxBit)
    error('minIter>maxIter')
end

% Assuming already trained
% init
switch class(obj) % decide delay and sigmaI2 based on class of obj
    case 'AWGN'
        % get sigma_I^2
        sigmaI2 = obj.ch.sigmaw2/2;
    case 'OFDM'
        sigmaI2 = obj.sigmaI2;
    case 'GeneralDFE'
        Dtot = obj.D+obj.Ds;
        
        % compute sigma_I^2
        psiD = obj.psi( obj.t_psi==obj.D );
        sigmaa2 = obj.BMAP'*obj.BMAP/length(obj.BMAP);
        sigmaI2 = ( obj.Jmin-abs(1-psiD)^2*sigmaa2 )/( 2*abs(psiD)^2 );
end
switch codedFlag % decide block size requested from Deinterleaver/LDPC decoder (if present)
    case true
        [enc,dec] = getLDPC();
        
        N = size(enc.ParityCheckMatrix,2);
        K = N-size(enc.ParityCheckMatrix,1);
        codeRate = K/N;
        
        blockSizeToEnc = lcm(nRows*nCols, K);
        blockSizeToDec = blockSizeToEnc/codeRate;
    case false
        blockSizeToEnc = 1;
        blockSizeToDec = 1; % no size limitations
end
totBits = 0;
errors = 0;
mem_yk = [];
mem_bl = [];
mem_LLR = [];

%% start simulation
while ( totBits<=maxBit )
    
    % create bits -> symbols
    Nbits = getNbits(codedFlag,blockSizeToEnc);
    bl = randi([0 1],Nbits,1);
    if (codedFlag)
        cp = encode(bl,nRows,nCols,enc);
    else
        cp = bl;
    end
    ak = QPSKEncoder(cp);
    
    % send symbols
    switch class(obj)
        case 'AWGN'
            yk = obj.ch.send(ak); % receiver does nothing
        case 'OFDM'
            yk = obj.send(ak);
        case 'GeneralDFE'
            [~,yk] = obj.send(ak);
            
            % symbols considering delay
            if (totBits==0 && isempty(mem_LLR))
                yk = yk(Dtot+1:end); % ignore fist symbols from the transient
            end
    end
    
    % get LLR and extract useful info
    [useful_yk,mem_yk] = updateMem_yk(yk,mem_yk,length(sigmaI2));
    LLR = QPSKSoftDecoder(useful_yk,sigmaI2);
    [useful_bl,mem_bl,LLR,mem_LLR] = updateMem_bl_LLR(bl,mem_bl,LLR,mem_LLR,blockSizeToDec,blockSizeToEnc);
    
    if (codedFlag)
        bl_hat = decode(-LLR,nRows,nCols,dec); % Matlab's LDCP considers 0 if LLR is >0
    else
        bl_hat = sign(LLR); % -1,+1
        bl_hat( bl_hat<=0 ) = 0; % 0,1
    end
    
    % calculate errors, update counter
    errors = computeErrors(errors,useful_bl,bl_hat);
    totBits = totBits + length(useful_bl);

    if ( errors>=minErr || totBits>=maxBit )
        break;
    end
end

BER = errors/totBits;

end

%% Utilities

function Nbits = getNbits(codedFlag,encSize)

if (codedFlag)
    Nbits = encSize;
else
    Nbits = 1024*1e3; % ~1M blocks per loop
end

end

function [useful_yk,mem_yk] = updateMem_yk(yk,mem_yk,len)

% init
totLen = length(yk)+length(mem_yk);
nBlocks = floor(totLen/len);

[useful_yk,mem_yk] = extract(yk,mem_yk,nBlocks*len);

end

function [useful_bl,bl_mem,useful_LLR,LLR_mem] = updateMem_bl_LLR(bl,bl_mem,LLR,LLR_mem,blockSizeLLR,blockSizeBl)
% Usually: vec1 = bl, vec2 = LLR

% init: extraction driven by LLR length
totLLR = length(LLR)+length(LLR_mem);
nBlocks = floor(totLLR/blockSizeLLR);

[useful_bl,bl_mem] = extract(bl,bl_mem,nBlocks*blockSizeBl);
[useful_LLR,LLR_mem] = extract(LLR,LLR_mem,nBlocks*blockSizeLLR);

end

function [useful,mem] = extract(new,old,N)

nNew = length(new);
nOld = length(old);

if (nNew+nOld<N)
    error('nNew+nOld<N')
end

if (nOld>=N)
    useful = old(1:N);
    mem = [old(N+1:end);new];
else
    useful = [old; new(1:N-nOld)];
    mem = new(N-nOld+1:end);
end

end


function cp = encode(bl,nRows,nCols,encoder)

% init
LDPCblockLenIn = size(encoder.ParityCheckMatrix,1);
LDPCblockLenOut = size(encoder.ParityCheckMatrix,2);
nBlocks = length(bl)/LDPCblockLenIn;
if ( mod(nBlocks,1)~=0 )
    error('length(bl) is not a multiple of LDPC encoder input')
end

cm = zeros(nBlocks*LDPCblockLenOut,1);

% encode block by block
for i = 0:nBlocks-1
    cm(LDPCblockLenOut*i+1:LDPCblockLenOut*(i+1)) =...
        encoder.step(bl(LDPCblockLenIn*i+1:LDPCblockLenIn*(i+1)));
end

% interleave
cp = interleave(cm,nRows,nCols);

end

function bl_hat = decode(LLR,nRows,nCols,decoder)

% init
LDPCblockLenIn = size(decoder.ParityCheckMatrix,2);
LDPCblockLenOut = size(decoder.ParityCheckMatrix,1);
nBlocks = length(LLR)/LDPCblockLenIn;
if ( mod(nBlocks,1)~=0 )
    error('length(LLR) is not a multiple of LDPC decoder input')
end

% deinterleave
LLR = deinterleave(LLR,nRows,nCols);
bl_hat = zeros(nBlocks*LDPCblockLenOut,1);

% decode block by block
for i = 0:nBlocks-1
    bl_hat(LDPCblockLenOut*i+1:LDPCblockLenOut*(i+1)) =...
        decoder.step(LLR(LDPCblockLenIn*i+1:LDPCblockLenIn*(i+1)));
end

end

function errors = computeErrors(err,bl,hat)

if ( isempty(bl) && isempty(hat) )
    errors = err;
elseif ( ~isempty(bl) && ~isempty(hat) )
    errors = err + sum(bl~=hat);
else
    error('Unexpected state')
end

end

function [encoder,decoder] = getLDPC()

encoder = comm.LDPCEncoder;

% if supported use gpu version
try
    decoder = comm.gpu.LDPCDecoder;
catch
    decoder = comm.LDPCDecoder;
end

end