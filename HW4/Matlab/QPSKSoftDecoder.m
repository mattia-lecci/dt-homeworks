function LLR = QPSKSoftDecoder(yk,sigmaI2)
% Soft decoder for QPSK modulation (convention as in QPSKbmap)
%
% INPUTS:
% * yk: sequence before decision block. It should be normalized with psiD
% * sigmaI2: noise variance per component for yk. If sigmaI2 is scalar,
%       then it is considered constant and equal for each input symbol.
%       Otherwise yk is divided into blocks of length(sigmaI2)
%
% OUTPUTS:
% * LLR: log-likelihood ratio for corresponding bit sequence. Positive if
%       more likely to be 1 (Matlab does the opposite!!)

% init
nBit = 2*length(yk);
LLR = zeros(nBit,1);

if ( isscalar(sigmaI2) ) % same for every symbol
    LLR(1:2:end) = 2*real(yk)/sigmaI2; % even
    LLR(2:2:end) = 2*imag(yk)/sigmaI2; % odd
else % OFDM! different for each subcarrier
    sigmaLen = length(sigmaI2);
    ykLen = length(yk);
    
    if ( mod(ykLen,sigmaLen)~=0 )
        error('length(yk) must be multiple of length(sigmaI2)')
    end
    
    nBlocks = ykLen/sigmaLen;
    ind_yk = 1:sigmaLen;
    ind_LLR = 1:2*sigmaLen;
    
    for i = 0:nBlocks-1
        
        LLR( ind_LLR(1:2:end) ) = 2*real( yk(ind_yk) )./sigmaI2;
        LLR( ind_LLR(2:2:end) ) = 2*imag( yk(ind_yk) )./sigmaI2;
        
        ind_yk = ind_yk+sigmaLen;
        ind_LLR = ind_LLR+2*sigmaLen;
        
    end
    
end