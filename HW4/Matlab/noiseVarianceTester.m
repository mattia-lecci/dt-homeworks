clear varables
close all
clc


addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

SNR = 10;

%% OFDM

savePlots = false;

disp 'Computing OFDM stats...'

Q0 = 2;
gtx=rcosdesign(0.0625, 11, Q0);
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
Eqc = qc'*qc;
grc = gtx;

t0 = 5;
sigmaw2 = 0;
Tofdm = 1;
M = 512;
Nvir = 0;
Neff = M - (2*Nvir+1);
ch = OFDMChannel(gtx,qc,grc,Q0,sigmaw2,Tofdm,t0);
BMAP = QPSKbmap();

ofdm = OFDM(ch,BMAP,M,Nvir);
sigmaw2 = computeOFDMsigmaw2(ofdm,SNR);
ofdm.setSigmaw2(sigmaw2);

% send ak
Nsymb = Neff*1e4;
ak = createRandomSymbols(BMAP,Nsymb);
yk = ofdm.send(ak);

estimVar = analyzeVar(ak,yk,length(ofdm.sigmaI2))/2; % /2 => per component

figure
plot(10*log10(estimVar), 'k.', 'LineWidth', 1.5);hold on;grid on
plot(10*log10(ofdm.sigmaI2), 'k-.', 'LineWidth', 1.5);
title('(\sigma_I^2)_{dB}')


%% dfe

disp 'Computing DFE stats...'

% channel params
Ta = 1;
Q0_sc = 2;
t0_sc = 5; Ds = floor(t0_sc/Q0_sc);
M1_sc = 5;
D_sc = 6; Dtot = D_sc+Ds;
M2_sc = 4+M1_sc-1-D_sc; % to nullify all elements after D, considering t0=5

pn = pnseq(ones(9,1),'+-1')*BMAP(1);
dfe_ch = Channel(qc,Q0_sc,0,Ta);
dfe = GeneralDFE(BMAP,dfe_ch);
dfe.ch.sigmaw2 = computeSigmaw2(SNR,BMAP,Eqc);
dfe.train(pn,t0_sc,M1_sc,M2_sc,D_sc);

% send ak
Nsymb = 1e5;
ak = createRandomSymbols(BMAP,Nsymb);
[~,yk] = dfe.send(ak);

estimVar = analyzeVar(ak(1:end-Dtot),yk(Dtot+1:end),1)/2; % /2 => per component

% analitically (Jmin)
psiD = dfe.psi( dfe.t_psi==dfe.D );
sigmaa2 = dfe.BMAP'*dfe.BMAP/length(dfe.BMAP);
sigmaI2_Jmin = ( dfe.Jmin-abs(1-psiD)^2*sigmaa2 )/( 2*abs(psiD)^2 );

% analitically (sigmaw2)
grc = dfe.g1;
t0mod = mod(t0_sc,Q0);
grc_sampled = grc(1+t0mod:Q0_sc:end);
errorChannel = conv(grc_sampled,dfe.c.Numerator);
errChEn = errorChannel'*errorChannel;
sigmaI2_sigmaw2 = dfe.ch.sigmaw2*errChEn/2;

fprintf(['estimVar=%f\n',...
    'sigmaI2_Jmin=%f\n',...
    'sigmaI2_sigmaw2=%f\n'],...
    estimVar,sigmaI2_Jmin,sigmaI2_sigmaw2);

plot([0 length(ofdm.sigmaI2)-1], 10*log10(sigmaI2_Jmin)*[1 1],'k', 'LineWidth', 1)
xlim([0 inf])
legend('OFDM - estimated','OFDM - computed', 'DFE')
xlabel('m [subch.]')

if (savePlots)
    matlab2tikz('../Latex/img/sigmaI.tex',...
        'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
        'xlabel style={font=\Large},'...
        'ylabel style={font=\Large}']);
end