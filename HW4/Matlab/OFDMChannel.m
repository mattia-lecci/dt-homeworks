classdef OFDMChannel < handle
    properties
        gtx; % FIRInterpolator
        qc; % channel response
        grc; % receiver filter
        
        sigmaw2; % (complex) noise power
        Tc; % channel sampling period = Tofdm/Q0
        Tofdm; % OFDM sampling period
        t0; % sampling timing phase
        Ds; % delay given by sampling (t0 and Q0)
        
        txMemory; % memory for gtx
        chMemory; % memory for qc
        rcMemory; % memory for grc
    end
    
    methods
        function obj = OFDMChannel(gtx,qc,grc,Q0,sigmaw2,Tofdm,t0)
            obj.gtx = dsp.FIRInterpolator(Q0,gtx);
            obj.qc = qc;
            obj.grc = grc;
            
            obj.sigmaw2 = sigmaw2;
            obj.Tc = Tofdm/Q0;
            obj.Tofdm = Tofdm;
            obj.t0 = t0;
            obj.Ds = floor(obj.t0/Q0);
            
            obj.txMemory = zeros( length(gtx)-1,1);
            obj.chMemory = zeros( length(qc)-1,1);
            obj.rcMemory = zeros( length(grc)-1,1);
        end
        
        function rk = send(obj,sk)
            
            % init
            Q0 = obj.gtx.InterpolationFactor;
            isMemoryEmpty = obj.isMemoryEmpty();
            
            % tx
            [stx,obj.txMemory] = filterInterp(obj.gtx,sk,obj.txMemory);
            [sc,obj.chMemory] = filter(obj.qc,1,stx,obj.chMemory);
            
            wc = crandn(size(sc,1),size(sc,2),0,obj.sigmaw2);
            
            rc = sc+wc;
            [rr,obj.rcMemory] = filter(obj.grc,1,rc,obj.rcMemory);
            
            if( isMemoryEmpty ) % first time -> full t0
                t0_ = obj.t0;
            else % steady state! take only t0mod
                t0_ = mod(obj.t0,Q0);
            end
            
            rk = rr(t0_+1:Q0:end);
        end
        
        function reset(obj)
            %RESET Reset channel memory to zero
            obj.txMemory = zeros(size(obj.txMemory));
            obj.chMemory = zeros(size(obj.chMemory));
            obj.rcMemory = zeros(size(obj.rcMemory));
        end
        
        function b = isMemoryEmpty(obj)
            isTxEmpty = all( obj.txMemory==0 );
            isChEmpty = all( obj.chMemory==0 );
            isRcEmpty = all( obj.rcMemory==0 );
            
            b = isTxEmpty && isChEmpty && isRcEmpty;
        end
        
        %% getters
        function [qr,n_qr] = getqr(obj)
            num_gtx = obj.gtx.Numerator;
            qr = conv( conv(num_gtx,obj.qc), obj.grc);
            n_qr = 0:length(qr)-1;
        end
        
        function [h,n_h] = geth(obj)
            Q0 = obj.gtx.InterpolationFactor;
            [qr,n_qr] = obj.getqr();
            t0mod = mod(obj.t0,Q0);
            
            % sample qr
            h = qr(t0mod+1:Q0:end);
            n_h = n_qr(t0mod+1:Q0:end); % only sampled
            n_h = (n_h-obj.t0)/Q0; % centered and renormalized
            
            % trim zeros
            if (n_h(1)<0) % keep zeros if filter is causal
                indFirst = find(h,1,'first');
                ind0 = find(n_h==0);
                indLast = find(h,1,'last');
                
                minInd = min(ind0,indFirst); % trim only negative zeros

                h = h(minInd:indLast);
                n_h = n_h(minInd:indLast);
            end
        end
        
        %% plots
        function plotGtx(obj,save)
            
            if (nargin<2)
                save = false;
            end
            
            num = obj.gtx.Numerator;
            n = (0:length(num)-1)';
            Q0 = obj.gtx.InterpolationFactor;
            
            mystem(n*obj.Tc,num,sprintf('$|g_{tx}|@ \\frac{T_{OFDM}}{%d}$',Q0),...
                't [s]','','NoHighlight')
            if (save)
                matlab2tikz('../Latex/img/gtx.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function plotQc(obj,save)
            
            if (nargin<2)
                save = false;
            end
            
            n = (0:length(obj.qc)-1)';
            Q0 = obj.gtx.InterpolationFactor;
            
            mystem(n*obj.Tc,abs(obj.qc),sprintf('$q_c @ \\frac{T_{OFDM}}{%d}$',Q0),...
                't [s]','$|\cdot|$','NoHighlight')
            if (save)
                matlab2tikz('../Latex/img/qc.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function plotGrc(obj,save)
            
            if (nargin<2)
                save = false;
            end
            
            n = (0:length(obj.grc)-1)';
            Q0 = obj.gtx.InterpolationFactor;
            
            mystem(n*obj.Tc,obj.grc,sprintf('$|g_{rc}|@ \\frac{T_{OFDM}}{%d}$',Q0),...
                't [s]','','NoHighlight')
            xlim([0 inf]);
            if (save)
                matlab2tikz('../Latex/img/grc.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    getXtick(n),','...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function plotQr(obj,save)
            
            if (nargin<2)
                save = false;
            end
            
            [qr,n_qr] = obj.getqr();
            Q0 = obj.gtx.InterpolationFactor;
            
            mystem(n_qr*obj.Tc,abs(qr),sprintf('$q_R \\frac{T_{OFDM}}{%d}$',Q0),...
                't [s]','$|\cdot|$','NoHighlight')
            xlim([0 inf]);
            if (save)
                matlab2tikz('../Latex/img/qr.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function ploth(obj,save)
            
            if (nargin<2)
                save = false;
            end
            
            [h,n_h] = obj.geth();
            
            mystem(n_h*obj.Tofdm,abs(h),'$|h_m| @T_{OFDM}$',...
                't [s]','$|\cdot|$','NoHighlight')
            if (save)
                matlab2tikz('../Latex/img/h.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function [QR,f] = plotQrFreq(obj,NFFT,save)
            
            if (nargin<3)
                save = false;
            end
            
            qr = obj.getqr();
            [QR,f] = freqz(qr,1,NFFT,1/obj.Tc);
            
            figure
            plot(f,20*log10(abs(QR)),'k','LineWidth',1);
            grid on;
            xlim([0 1/(2*obj.Tc)]);
            title('$|Q_R(f)|$','interpreter','latex');
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            xlabel('f [Hz]','interpreter','latex')
            ylim([-40 inf])
            if (save)
                matlab2tikz('../Latex/img/qrFreq.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function [H,f] = plotHFreq(obj,NFFT,save)
            
            if (nargin<3)
                save = false;
            end
            
            h = obj.geth();
            H = freqz(h,1,NFFT,'whole',1/obj.Tofdm);
            nH = 0:length(H)-1;
            
            figure
            plot(nH,20*log10(abs(H)),'k.');
            grid on;
            xlim([min(nH) max(nH)]);
            title('$|\mathcal{H}(e^{j2\pi \frac{m}{M}})|$','interpreter','latex');
            ylabel('$|\mathcal{H}_m|_{dB}$','interpreter','latex')
            xlabel('m [subch.]','interpreter','latex')
            ylim([0 10]);
            if (save)
                matlab2tikz('../Latex/img/hFreq.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function [GTX,f] = plotGtxFreq(obj,NFFT,save)
            
            if (nargin<3)
                save = false;
            end
            
            num_gtx = obj.gtx.Numerator;
            [GTX,f] = freqz(num_gtx,1,NFFT,1/obj.Tc);
            
            figure
            plot(f,20*log10(abs(GTX)),'k','LineWidth',1);
            grid on;
            xlim([0 1/(2*obj.Tc)]);
            title('$|G_{tx}(f)|$','interpreter','latex');
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            xlabel('f [Hz]','interpreter','latex')
            ylim([-40 inf]);
            if (save)
                matlab2tikz('../Latex/img/gtxFreq.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function [QC,f] = plotQcFreq(obj,NFFT,save)
            
            if (nargin<3)
                save = false;
            end
            
            [QC,f] = freqz(obj.qc,1,NFFT,1/obj.Tc);
            
            figure
            plot(f,20*log10(abs(QC)),'k','LineWidth',1);
            grid on;
            xlim([0 1/(2*obj.Tc)]);
            title('$|Q_c(f)|$','interpreter','latex');
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            xlabel('f [Hz]','interpreter','latex')
            ylim([-40 inf]);
            if (save)
                matlab2tikz('../Latex/img/qcFreq.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        function [GRC,f] = plotGrcFreq(obj,NFFT,save)
            
            if (nargin<3)
                save = false;
            end
            
            [GRC,f] = freqz(obj.grc,1,NFFT,1/obj.Tc);
            
            figure
            plot(f,20*log10(abs(GRC)),'k','LineWidth',1);
            grid on;
            xlim([0 1/(2*obj.Tc)]);
            title('$|G_{rc}(f)|$','interpreter','latex');
            ylabel('$|\cdot|_{dB}$','interpreter','latex')
            xlabel('f [Hz]','interpreter','latex')
            ylim([-40 inf]);
            if (save)
                matlab2tikz('../Latex/img/grcFreq.tex',...
                    'extraaxisoptions',['title style={font=\LARGE\bfseries},'...
                    'xlabel style={font=\Large},'...
                    'ylabel style={font=\Large}']);
            end
            
        end
        
        %% check attributes
        function set.qc(obj,newqc)
            % to avoid problems with channel memory
            if ( isempty(obj.qc) )
                obj.qc = newqc;
            else
                error('Cannot modify channel response')
            end
        end
        
        function set.gtx(obj,newgtx)
            % to avoid problems with channel memory
            if ( isempty(obj.gtx) )
                obj.gtx = newgtx;
            else
                error('Cannot modify tx filter')
            end
        end
        
        function set.grc(obj,newgrc)
            % to avoid problems with channel memory
            if ( isempty(obj.grc) )
                obj.grc = newgrc;
            else
                error('Cannot modify rc filter')
            end
        end
        
    end
    
end