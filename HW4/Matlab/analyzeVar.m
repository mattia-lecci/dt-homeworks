function variance = analyzeVar(ak,yk,period)

% init
yLen = length(yk);
nBlocks = floor( yLen/period);
ind = 1:period:nBlocks*period;

tmp = zeros(nBlocks,period);

for i = 1:period
    tmp(:,i) = yk(ind)-ak(ind); % centered in 0
    ind = ind+1;
end

variance = var(tmp).';

end