classdef OFDM < handle
    properties
        ch;
        BMAP;
        M;
        Npx; % by default Nc-1
        Nvir; % number of virtual channel per side. Total: 2*Nvir+1
        K; % channel gains (Mx1 vector)
        sigmaI2; % noise variance per component (col)
        
        mem_ak;
        mem_rn;
    end
    
    methods
        function obj = OFDM(ch,BMAP,M,Nvir)
            
            % memorizing
            obj.ch = ch;
            obj.BMAP = BMAP;
            obj.M = M;
            obj.Nvir = Nvir;
            
            % computing min Npx
            obj.Npx = length(obj.ch.geth())-1;
            
            
            % computing channel gain
            [h,n_h] = obj.ch.geth();
            H = fft(h,obj.M);
            
            n0 = n_h(1);
            i = 0:obj.M-1; % frequency vector not normalized
            expDelay = exp(-1j*2*pi*i*n0/obj.M);
            obj.K = shiftdim( H.*expDelay ); % adding delay term to H
            
            obj.setSigmaI2();
            
        end
        
        function yk = send(obj,ak)
            
            ak = shiftdim(ak);
            akBlock = obj.updateAkMem(ak);
            
            % sending
            sk = OFDMEncoder(akBlock,obj.M,obj.Npx,obj.Nvir);
            rn = obj.ch.send(sk);
            
            rnBlock = obj.updateRnMem(rn);
            yk = OFDMDecoder(rnBlock,obj.M,obj.Npx,obj.Nvir,obj.K);
        end
        
        function setSigmaw2(obj,sigmaw2)
           
            obj.ch.sigmaw2 = sigmaw2;
            obj.setSigmaI2();
            
        end
        
        function setSigmaI2(obj)
            
            % calculating sigmaI2
            sigmaI2_ = obj.computeSigmaI2();
            
            % extracting only useful coeffs (no virtual subcarriers)
            blockLen = obj.M - (2*obj.Nvir+1);
            firstLen = (blockLen+1)/2;
            lastLen = blockLen-firstLen;
            ind = [1:firstLen, obj.M-lastLen+1:obj.M];
            obj.sigmaI2 = sigmaI2_(ind);
            
        end
        
        function sigmaI2 = computeSigmaI2(obj)
            % compute sigmaI2
            grc = shiftdim(obj.ch.grc);
            Egrc = grc'*grc; % rx filter energy
            sigmaw2 = obj.ch.sigmaw2*Egrc; % var of noise before FFT
            sigmaW2 = obj.M*sigmaw2./abs(obj.K).^2; % var of noise after FFT and EQ
            sigmaI2 = sigmaW2/2; % noise per component
        end
        
        function reset(obj)
            obj.ch.reset();
            obj.mem_ak = [];
            obj.mem_rn = [];
        end
    end
    
    methods(Access=private)
        
        function akBlock = updateAkMem(obj,ak)
            
            if (~isempty(obj.mem_ak) )
                ak = [obj.mem_ak; ak]; % prefix memory
            end
            
            % init
            akLen = length(ak);
            totVir = 2*obj.Nvir+1;
            blockLen = obj.M-totVir;
            nBlocks = floor( akLen/blockLen ); % blocks of M symbols
            
            % take only integer multiples of totVir, the rest in memory
            akBlock = ak(1:nBlocks*blockLen);
            obj.mem_ak = ak(nBlocks*blockLen+1:end);
            
        end
        
        function rnBlock = updateRnMem(obj,rn)
            
            if (~isempty(obj.mem_rn) )
                rn = [obj.mem_rn; rn]; % prefix memory
            end
            
            % init
            rnLen = length(rn);
            blockLen = obj.M+obj.Npx;
            nBlocks = floor( rnLen/blockLen );
            
            % extract only integer multiples of M+Npx, the rest in memory
            rnBlock = rn(1:nBlocks*blockLen);
            obj.mem_rn = rn(nBlocks*blockLen+1:end);
            
        end
        
    end
end