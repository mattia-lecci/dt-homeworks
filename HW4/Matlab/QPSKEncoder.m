function ak = QPSKEncoder(cp)
% INPUTS:
% * cp: bit sequence
%
% OUTPUTS:
% * ak: encoded sequence according to QPSKbmap rule

if ( mod( length(cp),2) ==1 )
    error('cp has odd length')
end

% init
cp( cp==0 ) = -1; % convert 0 to -1

ak = cp(1:2:end) + 1j*cp(2:2:end);

end