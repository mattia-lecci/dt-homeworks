clear variables
close all
clc

addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

%% PARAMETERS

% interleaver
Nrows = 41;
Mcols = 45;

% QPSK modulator
BMAP = QPSKbmap();
Ma = BMAP'*BMAP/length(BMAP);

% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
Eqc = qc'*qc;

% ofdm
Tofdm = 1;
Q0 = 2;
rho = 0.0625;
gtx = rcosdesign(rho, 11, Q0);
% qc: same as SC
grc = gtx; % same as tx
t0_ofdm=2;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Nvir=0;%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
M = 512;

% dfe
Ta = 1;
t0_sc = 5;
M1_sc = 5;
D_sc = 6;
M2_sc = 4+M1_sc-1-D_sc; % to nullify all elements after D, considering t0=5

minIter = 1e4;
maxIter = 1e6;
minErr = 1e2;
SNR = 10;
encode = false;

% creating channels
ch = Channel(qc,Q0,0,Ta);
dfe = GeneralDFE(BMAP,ch);

ofdm_ch = OFDMChannel(gtx,qc,grc,Q0,0,Tofdm,t0_ofdm);
ofdm = OFDM(ofdm_ch,BMAP,M,Nvir);

%% OFDM
ofdm.reset();
ofdm.ch.sigmaw2 = computeOFDMsigmaw2(ofdm,SNR);

[BER,totBits,errors] = estimateBER(ofdm,encode,minIter,maxIter,minErr,Nrows,Mcols);
fprintf('OFDM BER: %6d/%6d=%.2e\n',errors,totBits,BER);

%% DFE
pn = pnseq(ones(9,1),'+-1')*BMAP(1);
dfe.ch.sigmaw2 = computeSigmaw2(SNR,BMAP,Eqc);
dfe.train(pn,t0_sc,M1_sc,M2_sc,D_sc);

[BER,totBits,errors] = estimateBER(dfe,encode,minIter,maxIter,minErr,Nrows,Mcols);
fprintf('DFE BER: %6d/%6d=%.2e\n',errors,totBits,BER);

%% AWGN
awgn = AWGN(BMAP,Ta);
awgn.ch.sigmaw2 = computeSigmaw2(SNR,BMAP);
awgn.ch.reset();

[BER,totBits,errors] = estimateBER(awgn,encode,minIter,maxIter,minErr,Nrows,Mcols);
fprintf('AWGN BER: %6d/%6d=%.2e\n',errors,totBits,BER);