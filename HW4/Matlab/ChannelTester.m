clear variables
close all
clc

addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

Q0 = 2;
num = [0 1 2 3 4];
BMAP = QPSKbmap();
Nsymb = 100;

ak = createRandomSymbols(BMAP,Nsymb);

%% testing filterInterp.m
h = dsp.FIRInterpolator(Q0,num);

N1 = 10;

[out,mem] = filterInterp(h,ak);
[out1,mem1] = filterInterp(h,ak(1:N1));
[out2,mem2] = filterInterp(h,ak(N1+1:end),mem1);

figure
stem(abs(out));hold on
stem(abs( [out1;out2]))

%% testing OFDMChannel
clear variables
close all
clc

Q0 = 2;
gtx=rcosdesign(0.0625, 11, Q0);
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
grc = gtx;

t0 = 1;
sigmaw2 = 0;
Tofdm = 1;

NFFT = 2048;

ch = OFDMChannel(gtx,qc,grc,Q0,sigmaw2,Tofdm,t0);

plot test
ch.plotGtx();
ch.plotQc();
ch.plotGrc();
ch.plotQr();
ch.ploth();
ch.plotQrFreq(NFFT);
ch.plotGtxFreq(NFFT);
ch.plotQcFreq(NFFT);
ch.plotGrcFreq(NFFT);

BMAP = QPSKbmap();
Nsymb = 100;
N1 = 11;
ak = createRandomSymbols(BMAP,Nsymb);

% outputs
out = ch.send(ak);

ch.reset();

out1 = ch.send(ak(1:N1));
out2 = ch.send(ak(N1+1:end));
out_hat = [out1;out2];

figure
stem(abs(out), 'Color', 'r'); hold on
stem(abs(out_hat), 'Color', 'k');

isequal(out,out_hat)

%% testing OFDM tx/rx

clear variables
close all
clc

Q0 = 2;
gtx=rcosdesign(0.0625, 11, Q0);
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
grc = gtx;

t0 = 5; Ds = floor(t0/Q0);
sigmaw2 = 0;
Tofdm = 1;
M = 512;
Nvir = 10;%%%%%%%%%%
ch = OFDMChannel(gtx,qc,grc,Q0,sigmaw2,Tofdm,t0);
BMAP = QPSKbmap();

ofdm = OFDM(ch,BMAP,M,Nvir);
sigmaw2 = computeOFDMsigmaw2(ofdm,Inf);
ofdm.setSigmaw2(sigmaw2);

% send ak
Nsymb = ( M-2*Nvir-1 )*10;
ak = createRandomSymbols(BMAP,Nsymb);

yk = ofdm.send(ak);

% select useful symbols
useful_ak = ak( 1:length(yk) );

disp(['Errors: ', num2str(sum( abs(useful_ak-yk)>1 ))]);
plotConstellation(yk,BMAP);

%% testing QPSK

clear variables
close all
clc

BMAP = QPSKbmap();
bl = randi([0 1], 100,1);

ak = QPSKEncoder(bl);
LLR = QPSKSoftDecoder(ak,0);

bl_hat = sign(LLR);
bl_hat( bl_hat<0 ) = 0;

isequal(bl,bl_hat)