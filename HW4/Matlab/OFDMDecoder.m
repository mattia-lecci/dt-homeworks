function yk = OFDMDecoder(rn,M,Npx,Nvir,K)
% Decoder for OFDM system
%
% INPUTS:
% * rn: input sequence
% * M: block length
% * Npx: cyclic prefix length
% * Nvir: number of virtual carriers per side to be ignored. Total: 2*Nvir+1
% * K: channel gain (DFT) per subcarrier
%
% OUTPUTS:
% * yk: output sequence after normalization

if ( mod(length(rn),M+Npx)~=0 )
    error('length(rn) must be a multiple of M+Npx');
end

rn = shiftdim(rn);
K = shiftdim(K);

% init
rnLen = length(rn);
totVir = 2*Nvir+1;
inBlockLen = M+Npx;
outBlockLen = M-totVir;
nBlocks = rnLen/inBlockLen;
ykLen = nBlocks*outBlockLen;
yk = zeros(ykLen,1);

ind_rn = Npx+1:inBlockLen; % only extracts useful part
ind_xk_first = 1:(outBlockLen+1)/2;
ind_xk_last = ( (outBlockLen+1)/2+1:outBlockLen )+totVir;
ind_xk = [ind_xk_first, ind_xk_last];
ind_yk = 1:outBlockLen;
Keff = K(ind_xk); % effective channel ignoring virtual subcarriers

for i = 1:nBlocks
    xk = fft( rn(ind_rn), M);
    yk(ind_yk) = xk(ind_xk)./Keff;
    
    % update indexes
    ind_rn = ind_rn+inBlockLen;
    ind_yk = ind_yk+outBlockLen;
end

end