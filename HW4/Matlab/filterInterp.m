function [out,memout] = filterInterp(filt,in,memin)
% Function that helps filtering with interpolators keeping memory for
% continuous filtering.

% INPUTS:
% * filt: object dsp.FIRInterpolator
% * in: input sequence
% * memin: input memory.
%
% OUTPUTS:
% * out: output sequence without the transient given by the memory
% * memout: to be used as next memin for continuity


if (nargin<3)
    memLen = length(filt.Numerator)-1;
    memin = zeros(memLen,1);
end

% adjust
memin = shiftdim(memin);
in = shiftdim(in);

% init
Q0 = filt.InterpolationFactor;
memLen = length(memin);
inLen = length(in);

% filtering
full_in = [memin;in];
out = filt.step(full_in);
out(1:memLen*Q0) = []; % cancel transient

% update memory
n = min(inLen,memLen);
memout = circshift(memin,-n); % shift memory towards past
memout(end-n+1:end) = in(end-n+1:end); % add new symbols

end