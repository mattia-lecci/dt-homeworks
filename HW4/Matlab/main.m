clear variables
close all
clc

addpath('../../HW1/Matlab')
addpath('../../HW2/Matlab')
addpath('../../HW3/Matlab')

%% PARAMETERS
% interleaver
Nrows = 41;
Mcols = 45;

% QPSK modulator
BMAP = QPSKbmap();
Ma = BMAP'*BMAP/length(BMAP);

% channels
Ta = 1;
Q0_sc = 2;
t0_sc = 5;
M1_sc = 5;
D_sc = 6;
M2_sc = 4+M1_sc-1-D_sc; % to nullify all elements after D, considering t0=5
% creation of q_c
qc = zeros(10,1);
qc(5+1) = 1*exp(1j*pi/6);
qc(6+1) = 0.9*exp(1j*pi/4);
qc(9+1) = 0.6*exp(1j*pi/3);
Eqc = qc'*qc;

Tofdm = 1;
Q0_ofdm = 2;
rho = 0.0625;
gtx = rcosdesign(rho, 11, Q0_ofdm);
% qc: same as SC
grc = gtx; % same as tx

t0_ofdm = 11;
%Npx=0; computed automatically by OFDM as Nc-1
Nvir = 17;
M = 512;

% other
savePlots = false;
saveMat = false;
NFFT = 2048;

Pmin = 1e-5;
Pmax = 1e-1;
minIter = 1e4;
maxIter = 1e7;
minErr = 1e3;

% run
run_dfe_uncoded = false;
run_ofdm_uncoded = false;
run_awgn_uncoded = false;
run_dfe_coded = false;
run_ofdm_coded = false;
run_awgn_coded = false;

%% PREPARATION
dfe_ch = Channel(qc,Q0_sc,0,Ta);

ofdm_ch = OFDMChannel(gtx,qc,grc,Q0_ofdm,0,Tofdm,t0_ofdm);
ofdm = OFDM(ofdm_ch,BMAP,M,Nvir);

pn = pnseq(ones(9,1),'+-1')*BMAP(1);

%% PLOTS
ofdm_ch.plotGrc(savePlots); title('$g_{\sqrt{rcos}} @ \frac{T_{OFDM}}{2}$','interpreter','latex')
ofdm_ch.plotQc(savePlots);
ofdm_ch.plotQr(savePlots); 
ofdm_ch.ploth(savePlots);
ofdm_ch.plotQrFreq(NFFT,savePlots); 
ofdm_ch.plotGrcFreq(NFFT,savePlots); title('$G_{\sqrt{rcos}}(f)$','interpreter','latex')
ofdm_ch.plotHFreq(M,savePlots);

mygreen = [0 175 0]/255;
myblue = [0 128 192]/255;

%% UNCODED

% parameters
Gamma_min = 0;
Gamma_max = 15;
Npts = 20;
Gamma_db = linspace(Gamma_min,Gamma_max,Npts);
Gamma = 10.^(Gamma_db/10);

close all
figure; hold on

% DFE
Pbit_dfe_uncoded = zeros(Npts,1);

disp 'DFE uncoded'
if (run_dfe_uncoded)
    for i = 1:Npts
        dfe = GeneralDFE(BMAP,dfe_ch);
        dfe.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        dfe.ch.reset();
        dfe.train(pn,t0_sc,M1_sc,M2_sc,D_sc);
        
        tic
        [Pbit_dfe_uncoded(i),Niter,errors] = estimateBER(dfe,false,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_dfe_uncoded(i));
        
        if (Pbit_dfe_uncoded(i)<=Pmin) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pbit_dfe_uncoded','Pbit_dfe_uncoded')
    end
else
    load('Pbit_dfe_uncoded','Pbit_dfe_uncoded')
end

semilogy(Gamma_db, Pbit_dfe_uncoded,'Color',mygreen,'LineWidth',1)

% OFDM
Pbit_ofdm_uncoded = zeros(Npts,1);

disp 'OFDM uncoded'
if (run_ofdm_uncoded)
    for i = 1:length(Gamma)
        sigmaw2 = computeOFDMsigmaw2(ofdm,Gamma_db(i));
        ofdm.setSigmaw2(sigmaw2);
        ofdm.reset();
        
        tic
        [Pbit_ofdm_uncoded(i),Niter,errors] = estimateBER(ofdm,false,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_ofdm_uncoded(i));
        
        if (Pbit_ofdm_uncoded(i)<=Pmin) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
        
    end
    if saveMat
        save('Pbit_ofdm_uncoded','Pbit_ofdm_uncoded')
    end
else
    load('Pbit_ofdm_uncoded','Pbit_ofdm_uncoded')
end

semilogy(Gamma_db, Pbit_ofdm_uncoded,'Color',myblue,'LineWidth',1)

% AWGN
Pbit_awgn_uncoded = zeros(Npts,1);

disp 'AWGN uncoded'
if (run_awgn_uncoded)
    for i = 1:Npts
        awgn = AWGN(BMAP,Ta);
        awgn.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP);
        awgn.ch.reset();
        
        tic
        [Pbit_awgn_uncoded(i),Niter,errors] = estimateBER(awgn,false,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_awgn_uncoded(i));
        
        if (Pbit_dfe_uncoded(i)<=Pmin) % if too small, avoid doing the following ones
            disp '>> break'
            break;
        end
    end
    
    save('Pbit_awgn_uncoded','Pbit_awgn_uncoded');
else
    load('Pbit_awgn_uncoded','Pbit_awgn_uncoded');
end

semilogy(Gamma_db, Pbit_awgn_uncoded,'k','LineWidth',1)

% managing plot
title('Results - Uncoded','interpreter','latex')
xlabel('$\Gamma_{dB}$','interpreter','latex')
ylabel('$P_{bit}$','interpreter','latex')
xlim([Gamma_min, Gamma_max])
ylim([Pmin, Pmax])
set(gca,'Yscale','log')
grid on
legend('DFE','OFDM','AWGN (MF bound)')
if savePlots
    matlab2tikz('../Latex/img/results_uncoded.tex',...
        'extraaxisoptions',['title style={font=\Large\bfseries},'...
        'xlabel style={font=\large},'...
        'ylabel style={font=\large}']);
end

%% CODED

% parameters
Gamma_min = 0.5;
Gamma_max = 2;
Npts = 20;
Gamma_db = linspace(Gamma_min,Gamma_max,Npts);
Gamma = 10.^(Gamma_db/10);

figure; hold on

% DFE
Pbit_dfe_coded = zeros(Npts,1);

disp 'DFE coded'
if (run_dfe_coded)
    for i = 1:Npts
        dfe = GeneralDFE(BMAP,dfe_ch);
        dfe.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP,Eqc);
        dfe.ch.reset();
        dfe.train(pn,t0_sc,M1_sc,M2_sc,D_sc);
        
        tic
        [Pbit_dfe_coded(i),Niter,errors] = estimateBER(dfe,true,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_dfe_coded(i));
        
        if (Pbit_dfe_coded(i)<=Pmin) % if too small, avoid doing the following ones
            if (Pbit_dfe_coded(i)==0)
                Pbit_dfe_coded(i) = 1/maxIter; % avoids to truncate the line
            end
            
            disp '>> break'
            break;
        end
    end
    
    if saveMat
        save('Pbit_dfe_coded','Pbit_dfe_coded')
    end
else
    load('Pbit_dfe_coded','Pbit_dfe_coded')
end

semilogy(Gamma_db, Pbit_dfe_coded,'Color',mygreen,'LineWidth',1)

% OFDM
Pbit_ofdm_coded = zeros(Npts,1);

disp 'OFDM coded'
if (run_ofdm_coded)
    for i = 1:length(Gamma)
        sigmaw2 = computeOFDMsigmaw2(ofdm,Gamma_db(i));
        ofdm.setSigmaw2(sigmaw2);
        ofdm.reset();
        
        tic
        [Pbit_ofdm_coded(i),Niter,errors] = estimateBER(ofdm,true,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_ofdm_coded(i));
        
        if (Pbit_ofdm_coded(i)<=Pmin) % if too small, avoid doing the following ones
            if (Pbit_ofdm_coded(i)==0)
                Pbit_ofdm_coded(i) = 1/maxIter; % avoids to truncate the line
            end
            
            disp '>> break'
            break;
        end
        
    end
    if saveMat
        save('Pbit_ofdm_coded','Pbit_ofdm_coded')
    end
else
    load('Pbit_ofdm_coded','Pbit_ofdm_coded')
end

semilogy(Gamma_db, Pbit_ofdm_coded,'Color',myblue,'LineWidth',1)

% AWGN
Pbit_awgn_coded = zeros(Npts,1);

disp 'AWGN coded'
if run_awgn_coded
    for i = 1:Npts
        awgn = AWGN(BMAP,Ta);
        awgn.ch.sigmaw2 = computeSigmaw2(Gamma_db(i),BMAP);
        awgn.ch.reset();
        
        tic
        [Pbit_awgn_coded(i),Niter,errors] = estimateBER(awgn,true,minIter,maxIter,minErr); t = toc;
        
        fprintf('%2d/%2d. Time=%7.3fs, SNR=%5.2f, Niter=%8d, errors=%7d, BER=%.2e\n',...
            i,Npts,t,Gamma_db(i),Niter,errors,Pbit_awgn_coded(i));
        
        if (Pbit_awgn_coded(i)<=Pmin) % if too small, avoid doing the following ones
            if (Pbit_awgn_coded(i)==0)
                Pbit_awgn_coded(i) = 1/maxIter; % avoids to truncate the line
            end
            
            disp '>> break'
            break;
        end
    end
    
    save('Pbit_awgn_coded','Pbit_awgn_coded');
else
    load('Pbit_awgn_coded','Pbit_awgn_coded');
end

semilogy(Gamma_db, Pbit_awgn_coded,'k','LineWidth',1)

% managing plot
title('Results - coded','interpreter','latex')
xlabel('$\Gamma_{dB}$','interpreter','latex')
ylabel('$P_{bit}$','interpreter','latex')
xlim([0.5, 2])
ylim([Pmin, Pmax])
set(gca,'Yscale','log')
grid on
legend('DFE','OFDM','AWGN (MF bound)')

if savePlots
    matlab2tikz('../Latex/img/results_coded.tex',...
        'extraaxisoptions',['title style={font=\Large\bfseries},'...
        'xlabel style={font=\large},'...
        'ylabel style={font=\large}']);
end
