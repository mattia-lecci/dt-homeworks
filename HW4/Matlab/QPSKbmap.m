function BMAP = QPSKbmap()
% First symbol -> Re[]
% Second symbol -> Im[]

BMAP = zeros(4,1);
BMAP(1) = 1+1j;
BMAP(2) = 1-1j;
BMAP(3) = -1+1j;
BMAP(4) = -1-1j;

end