\section{Choice of $t_0$ and $N_{vir}$} \label{sec:appendix}

\begin{figure}[b]
	\hfill
	\centering
	\subfigure[$\bar{t}_0 = 11$ OFDM without LDPC, $\Gamma_{dB} =  10 dB$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/Nvir_t0_5.tex}
		}\label{fig:Nvir5}}
	\hfill
	\subfigure[$\bar{t}_0 = 11$ vs $\bar{t}_0 = 12$ OFDM uncoded, $\Gamma_{dB} =  10 dB$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/Nvir_uncoded_5vs6.tex}
		}\label{fig:NvirUncoded}}
	\hfill
	\subfigure[$\bar{t}_0 = 11$ vs $\bar{t}_0 = 12$ OFDM coded, $\Gamma_{dB} =  1.3 dB$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/Nvir_coded_5vs6.tex}
		}\label{fig:NvirCoded}}
	\hfill
	\caption{Choice of the optimum $N_{vir}$ for two different values of $t_0$}
	\label{fig:tradeoff}
\end{figure}

In order to find the optimum number of virtual subchannels, we performed simulations for different values of $N_{vir}$, searching for the best trade-off between bit error probability and efficiency. Note that once $N_px$ is fixed, we can define efficiency as 
\begin{equation}
\eta = \frac{M - (2N_{vir}+1)}{M}
\end{equation}
where M is the total OFDM block size and, for symmetry, $2N_{vir}+1$ is the total number of virtual subchannels; in fact $N_{vir}$ takes into account just one side of the band of interest.

The search for the best $N_{vir}$ has been done jointly with the one for suitable value of $\bar{t}_0$. First of all, we studied when $|h|$ starts assuming non negligible values, which is around $\bar{t}_0 = 12 \implies t_0=6$, as discussed in section \textbf{OFDM} and can be seen in Fig.~\ref{fig:qr}, p.\pageref{fig:qr}. Of course, once decided where to start, what makes the difference is just if we take an even or odd starting point. This is due to the sampling factor 2 and that any shift in the time domain is traduced in a multiplication by a complex exponential in the frequency domain.

As a consequence, we tried different configurations of $N_{vir}$ just for $\bar{t}_0 = 11$ and $\bar{t}_0 = 12$. We tested our assumptions for both uncoded and coded configurations, using fixed values of SNR ($\Gamma_{dB} =  10\ dB$ and $1.3\ dB$ respectively for uncoded and coded OFDM versions).

Figure \ref{fig:tradeoff} shows the behaviour of BER and $\sigma_I^2$ for increasing values of $N_{vir}$. In Figure \ref{fig:NvirUncoded} we compare performance of BER for $\bar{t}_0 = 11$ and 12 for the uncoded version of OFDM and we can notice that $\bar{t}_0 = 11$ gives the best results even for small values of $N_{vir}$. The difference between the two timing phases and the different values of $N_{vir}$, though, is very little.

With the addition of the LDPC coding, again $\mathbf{\bar{t}_0 = 11}$ seems to be the best choice, as confirmed by Figure \ref{fig:NvirCoded} but with a significant difference this time. We then decided to use that as optimal parameter.

Given all this additional information, we are now ready to choose also $N_{vir}$. Watching just Figure \ref{fig:NvirUncoded} we could be tempted to choose $N_{vir} = 0$, seeing that $P_{bit}$ doesn't change too much for small values of the parameter; in fact we saw that different simulations with increasing value of $N_{vir}$ show that performances don't improve. However, if we look at Figure \ref{fig:NvirCoded}, the coded version of OFDM, we see that $P_{bit}$ expresses a local minimum around 20, which is a very high value (it would eliminate 41/512 subchannels). In order to get an in-depth overview, we performed some more tests: it can be seen from Figure \ref{fig:resultsNvir} that the gain in using $N_{vir} = 10$ or 17 instead of $N_{vir} = 0$ is negligible, approximately of the order of 0.1 dB; thus in that way we would be wasting channels whit no appreciable advantages. Given that, we decided to keep the efficiency (as we define it) at 100\% setting $N_{vir} = 0$.

Another observation in support of our trade-off approach, is that if we start increasing a lot $N_{vir}$ (for example 100 or bigger) $\sigma_I^2$ decreases (Figure \ref{fig:Nvir5}), bringing to a gain in terms of SNR and consequently to a better performance for what concern bit error probability. This is due to the fact that after a certain point we use just "good" channels, or channels that have a low $\sigma_I^2$, having deleted all the "bad" ones. Great, but what a huge waste of resources! Given this remarkable drop in efficiency, we can conclude that working with OFDM in such conditions (great value of $N_{vir}$) can't be an option.

Since this tradeoff appears over and over we also decided to define rate and effective rate as follows:
\begin{align}
R(N_{vir}) &= \frac{M- (2N_{vir}+1)}{T_{block}} \cdot 2 \quad bps\\
R_{eff}(N_{vir}) &= R(1-P_{bit}(N_{vir})) \quad bps
\end{align}
where the factor 2 in the Rate is given by the fact that QPSK sends 2 bits per symbol, and it's clear that both Rate and Effective Rate are functions of the number of virtual subchannels. The definition of $R_{eff}$ basically is the number of correctly received bits per second.

The results are shown in Fig.~\ref{fig:Reff}. It is clear that for the uncoded version the best choice would still be $V_{vir}=0$ while for the coded version the optimal value would seem to be $N_{vir}=2$. Since this was a last minute idea we didn't have time to perform more in-depth analysis we kept $N_{vir}=0$.


\begin{figure}[h]
	\hfill
	\centering
	\subfigure[$R_eff$ for uncoded OFDM]{
		\includegraphics[width=.4\linewidth]{img/Reff_uncoded.eps}
	\label{fig:Reff_uncoded}}
	\hfill
	\subfigure[$R_eff$ for coded OFDM]{
		\includegraphics[width=.4\linewidth]{img/Reff_coded.eps}
	\label{fig:Reff_coded}}
	\hfill
	\caption{Plots for $R_{eff}(N_{vir})$}
	\label{fig:Reff}
\end{figure}

\begin{figure}[h]
	\centering
	\resizebox{0.4\linewidth}{!}{
			\input{img/results_coded_Nvir.tex}
	}
	\caption{Comparison between different value of $N_{vir}$, having fixed $t_0 = 11$}
	\label{fig:resultsNvir}
\end{figure}