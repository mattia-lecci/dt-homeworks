\section{Orthogonal Frequency Division Multiplexing}

\textit{Let the OFDM block size M be 512.\\
	$T_{OFDM}=\frac{T_{block}}{M + N_{px}}$ where $T_{block}$ is the 'time length' of the data block and $N_{px}$ the prefix length.\\
	The receiver, operating at $T_C$, is a square root of a raised-cosine pulse and its output is sampled at $\frac{1}{T_{OFDM}}$ with a suitable timing phase $t_0$.\\
	Determine $t_0$, the instant the receiver starts collecting $M + N_{px}$ samples, i.e. $\bar{t}_0$, where $\bar{t}_0=\frac{t_0}{T_C}$.\\
	Determine values for $N_{px}$ and $N_{vir}$ (number of virtual subchannels).\\
	Report expression of LLR's.
}

\begin{figure}[h!]
	\centering
	\input{img/ofdm.tex}
	\caption{Channel model for OFDM}
\end{figure}

\begin{figure}[t]
	\subfigure[Study of $\sigma_I^2$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/sigmaI.tex}
		}\label{fig:sigmaI}}
	\hfill
	\subfigure[High SNR (subch. 0)]{
		\includegraphics[width=.3\linewidth]{img/const_ofdm_highSNR}
		\label{fig:highSNR}}
	\hfill
	\subfigure[Low SNR (subch. 254)]{
		\includegraphics[width=.3\linewidth]{img/const_ofdm_lowSNR}
		\label{fig:lowSNR}}
	\caption{OFDM constellation for different subchannels}
	\hfill
	\label{fig:ofdm_const}
\end{figure}

\begin{figure}[b]
	\centering
	\subfigure[Impulse response of $g_{\sqrt{rcos}}$ @$\frac{T_{OFDM}}{2}$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/grc.tex}
		}\label{fig:grc}}
	\hfill
	\subfigure[Impulse Response of $q_C$ @$\frac{T_{OFDM}}{2}$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/qc.tex}
		}\label{fig:qc}}
	\hfill
	\subfigure[Impulse Response of $q_R$ @$\frac{T_{OFDM}}{2}$]{
		\resizebox{0.3\linewidth}{!}{
			\input{img/qr.tex}
		}\label{fig:qr}}
	\caption{Filters impulse response $(T_{OFDM}=1s)$}
\end{figure}

In this case, both the transmitter and the receiver are implemented with a square-root raised cosine impulse response having roll-off factor $\rho=0.0625$. After some testing on the length of the filter we decided that 23 (meaning 5 precursors and 5 postcursors, plus the central symbol, with an upsampling factor $Q_0=2$) was enough since going beyond that didn't change much the results. The plot of $g_{\sqrt{rcos}}$ is shown in Figure \ref{fig:grc} while the module of its frequency response is shown in Figure \ref{fig:grcfreq}.

In this case the equivalent channel impulse response is $h(mT_{OFDM}) = q_R(t_0 + mT_{OFDM})$ where $q_R = (g_{\sqrt{rcos}} * q_C * g_{\sqrt{rcos}})$. As mentioned in the text, $T_{OFDM}$ depends also on $N_{px}$, the cyclic prefix length; as we know, the cyclic prefix is important since it allows to greatly simplify both transmitter and receiver by using a simple DFT/IDFT instead of filter banks in the DMT/OFDM we developed, at the cost, though, of some inefficiency. Figure \ref{fig:qr} shows the impulse response of $q_R$ while Figure \ref{fig:qrfreq} shows its frequency response.

To be consistent, $N_{px}$ needs to be greater than or equal to the length of channel impulse response minus 1. One thing that it is important to notice that the TX/RC filters are simple low pass filters. This means that, ideally, we should only care about $q_c$ for the choice of $N_{px}$. However, in practice we cannot use filters with infinite length and the windowing of the raised cosine will only give us an approximation of the ideal response. As a result, $q_R$ will be approximately zero towards the beginning and the end of the response (i.e. far away from where the translated channel lies) while near the center the effects of this windowing will become not negligible. Since we are using a relatively short filter, we are forced to consider almost all of the samples as channel. We then decided to only consider samples which are at least bigger than a factor $10^{-2}$ from the maximum. Hence our decision, looking at Fig.~\ref{fig:h}, is to choose $\mathbf{t_0=11}$ and $\mathbf{N_{px}=18}$. This yields results almost indistinguishable from those obtained considering the whole channel. Deeper considerations on the choice of $t_0$ will be discussed in Appendix.

We recall that in the OFDM $\sigma_I^2$ is computed in a slightly different way than in the previous case. First of all we consider the noise at the receiver as $\sigma_{w_R}^2 = \sigma_w^2 E_{g_{RC}}$ where $\sigma_w^2$ is the noise introduced by the channel and $E_{g_{RC}}$ is the energy of the receiving filter. The next step is the FFT block, from which we get as output $\sigma_{W_R}^2 = M\sigma_{w_R}^2$ which is just the noise weighted by the normalization factor M (needed due to how Matlab computes the DFT). After that we finally have have the normalization factor due to the transform of the channel $\mathcal{H}_m \triangleq \mathcal{H}(e^{j2\pi \frac{m}{M}})$, yielding $\sigma_{W,m}^2 = \frac{\sigma_{W_R}^2}{|\mathcal{H}_m|^2}$, thus $\sigma_{I,m}^2 = \frac{\sigma_{W,m}^2}{2}$, hence yielding a different noise variance for each subchannel $m$.

In this case the Log Likelihood Ratio for each subchannel $m$ is:
\begin{equation}
l'^{(m)}_{2k} = \frac{2\Re[\bar{y}_k]}{\sigma^2_{I,m}} \qquad 
l'^{(m)}_{2k+1} = \frac{2\Im[\bar{y}_k]}{\sigma^2_{I,m}}
\end{equation}

In fact, differently from the previous case, now $\sigma^2_{I,m}$ exhibits the dependency from the vector $\mathcal{H}_m$: in this way of course we don't obtain a scalar as in the previous case, but a vector. Looking finally at Figure \ref{fig:hfreq}, which depicts $|\mathcal{H}_m|$ over M samples, it's easy to see how its behaviour deeply influences different samples of $\sigma^2_{I,m}$.

Just to be sure of our estimate of $\sigma^2_{I,m}$ we compared an actual estimate (variance of the error of ($\bar{y}_k - a_k$)) with our calculation just described . In Figure \ref{fig:sigmaI} it's clear that the difference is negligible; in that figure we plot also the constant value of $\sigma^2_I$ for DFE. What we can notice from Figure \ref{fig:highSNR} and Figure \ref{fig:lowSNR} is that for low value of $\sigma^2_I$ (and so high SNR) the cloud of the received symbols is closely circumscribed, increasing the LLR value and so the reliability of the detection; for high $\sigma^2_I$ symbols are more dispersed, increasing the probability of wrong detection and lower the LLR value. What we wanted to observe is that while DFE may have an averagely good channel, OFDM has peaks and troughs, but they do not average out in the uncoded version of the system. When the noise is lower than a certain threshold, in fact, there isn't a big improvement since all the received symbols tend to not overflow to close regions. Whereas, as the variance increases there will be more and more errors. Hence, variance peaks are predominant in the BER with respect to troughs, thus yielding worse performance for the OFDM system. In the uncoded version, instead, LLR's are taken into account and this actually helps, greatly reducing the gap (or even surpassing it) from OFDM to DFE.

In our configuration we decided to set $N_{vir} = 0$. We dedicated an entire section in page \pageref{sec:appendix} to describe the procedure and tests that lead us to this decision.

\begin{figure}[t]
	\centering
	\subfigure[Frequency response of $g_{\sqrt{rcos}}$]{
		\resizebox{0.4\linewidth}{!}{
			\input{img/grcFreq.tex}
		}\label{fig:grcfreq}}
	\hfill
	\subfigure[Frequency Response of $q_R$]{
		\resizebox{0.4\linewidth}{!}{
			\input{img/qrFreq.tex}
		}\label{fig:qrfreq}}
	\caption{Filters impulse response}
\end{figure}
\begin{figure}[t]
	\centering
	\subfigure[Magnitude of the impulse response. Note that only the non-zero terms are plotted (the first 5 zeros from $q_c$ are ignored)]{
		\resizebox{0.35\linewidth}{!}{
			\input{img/h.tex}
		}\label{fig:h}}
	\hfill
	\subfigure[Magnitude of the frequency response]{
		\resizebox{0.35\linewidth}{!}{
			\input{img/hFreq.tex}
		}\label{fig:hfreq}}
	\caption{Equivalent channel response in OFDM}
\end{figure}