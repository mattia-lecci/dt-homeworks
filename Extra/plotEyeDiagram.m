function plotEyeDiagram(x,n1,samplesPerWindow,step)

% init
ind = n1:(n1+samplesPerWindow-1);
nBlocks = getNblocks(x,n1,samplesPerWindow,step);

signals = zeros(samplesPerWindow,nBlocks);
n = 0:samplesPerWindow-1;

% extract signal pieces
for i = 1:nBlocks
    
    signals(:,i) = x(ind);
    ind = ind+step;
    
end

figure
plot(n,signals,'k');
title('Eye Diagram')
xlabel('n (+n_1)')

end

% utility
function nBlocks = getNblocks(x,n1,samplesPerWindow,step)

effL = length(x)-n1+1; % effective length of x excluding first n1-1 samples
nBlocks = floor( (effL-samplesPerWindow)/step +1 );

end