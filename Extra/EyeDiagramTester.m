clear variables
close all
clc

addpath('../HW3/Matlab')

% rcos paramters
rho = 0;
symbolsPerSide = 5;
nSymb = 1+2*symbolsPerSide;
Q0 = 10;

num = rcosdesign(rho,nSymb,Q0,'normal'); num = num/max(num);
n_h = 0:length(num)-1;
h = dsp.FIRInterpolator(Q0,num);

peakInd = 1 + n_h(end)/2;

figure
stem(n_h,num)
title('h_n')
xlabel('n')

% signal generation
M = 4; % BMAP cardinality
BMAP = createBMAP('pam',M);
N = 1e3; % #symbols 

ak = randi([1, M],N,1);
ak = BMAP(ak);

x = h.step(ak);

samples = peakInd:Q0:length(x); % when to sample

figure
plot(x,'k'); hold on
plot(samples,x(samples),'ko')
title('x')

% eye diagram
n1 = peakInd;
samplesPerWindow = 2*Q0+1;
step = Q0;

plotEyeDiagram(x,n1,samplesPerWindow,step);
ylim([min(BMAP)-2 max(BMAP)+2])